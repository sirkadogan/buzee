const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

const PoaDB = admin.initializeApp({
    databaseURL: "https://buzee2-9c40b-d2c90.firebaseio.com/",
    appId: 'PoaDB'
}, 'PoaDB');

function deleteOffensivePost(snapshot, context) { //Deleta post que tenha recebido 5 downvotes
    const pushId = context.params.pushId;
    const root = snapshot.after.ref.root;
    const eventData = snapshot.after.val();
    if (eventData <= -5) {
        return root.child('posts/postFeed/' + pushId).once('value').then((snapshot) => { //Pega post e salva em um nó de posts deletados
            data = snapshot.val();
            return root.child('posts/postDeleted/').push(data).then(() => {
                return root.child('posts/postFeed/' + pushId).remove() //Remove post do feed
            });
        })
    } else {
        return null
    }
}

function deleteOffensiveComment(snapshot, context) { //Deleta comentario que tenha recebido 5 downvotes
    const root = snapshot.after.ref.root;
    const eventData = snapshot.after.val();
    let commentId = snapshot.after.ref.parent.key;
    let postId = snapshot.after.ref.parent.parent.key;

    if (eventData <= -5) {
        return root.child('posts/postComments/' + postId + '/' + commentId).once('value').then((snapshot) => { //Pega comment e salva em um nó de comments deletados
            data = snapshot.val();
            return root.child('posts/commentsDeleted/').push(data).then(() => {

                return root.child('posts/postComments/' + postId + '/' + commentId).remove().then(() => {//Remove comment do feed
                    return root.child('posts/postFeed/' + postId + '/comments') //Subtrai contador de comments no post
                        .transaction((current_value) => {
                            if (current_value) {
                                return (current_value) - 1;
                            } else {
                                return 0;
                            }

                        });
                })
            });
        })
    } else {
        return null
    }
}

function notifyComment(snapshot, context) {
    const commentData = snapshot.val();
    let author = commentData.author;
    let commentUid = commentData.user;
    const root = snapshot.ref.root;
    let postId = snapshot.ref.parent.key;
    console.log({ author, postId, commentUid });

    return root.child('posts/postFeed/' + postId + '/user').once('value').then((snapshot) => { //Pega comment e salva em um nó de comments deletados
        userUid = snapshot.val();
        console.log('userUid', userUid);
        if (commentUid != userUid) {
            return root.child('users/' + userUid + '/FCM').once('value').then((snapshot) => {
                console.log('FCM', snapshot.val());
                if (snapshot.exists()) {
                    token = snapshot.val();
                    let payload = {
                        notification: {
                            title: author,
                            body: author + ' comentou no seu post!'
                        }
                    }
                    return admin.messaging().sendToDevice(token, payload);
                } else {
                    return null;
                }

            })
        } else {
            return null
        }

    })

}


function levelUpNotification(snapshot, context) {
    const uid = context.params.pushId;
    const root = snapshot.after.ref.root;
    return root.child('users/' + uid + '/FCM').once('value').then((snapshot) => {
        if (snapshot.exists()) {
            token = snapshot.val();
            let payload = {
                notification: {
                    title: 'Level Up',
                    body: 'Você subiu de nível!'
                }
            }
            return admin.messaging().sendToDevice(token, payload);
        } else {
            return null;
        }

    })
}

function sendCouponNotification(snapshot, context) {
    const uid = context.params.pushId;
    const root = snapshot.after.ref.root;
    return root.child('users/' + uid + '/FCM').once('value').then((snapshot) => {
        if (snapshot.exists()) {
            token = snapshot.val();
            let payload = {
                notification: {
                    title: 'Você recebeu um cupom!',
                    body: 'Obrigado por ajudar a comunidade :)'
                }
            }
            return admin.messaging().sendToDevice(token, payload);
        } else {
            return null;
        }

    })
}

function shareListenerHandler(snapshot) {
    const eventData = snapshot.val();
    const root = snapshot.ref.root;
    // const key = snapshot.key;
    let direcao = snapshot.ref.parent.key;
    let linha = snapshot.ref.parent.parent.key;
    var package = {
        data: eventData,
        L: linha,
        D: direcao,
    }
    // console.log('direcao: ', snapshot.data.ref.parent.parent.key)
    // console.log('linha: ', snapshot.data.ref.parent.parent.parent.key)
    return root.child('Dados/ShareData/').push(package);
}

function updateProfileHandler(snapshot, context, hasReward) {
    const eventData = snapshot.after.val();
    const uid = context.params.pushId;
    // console.log(eventData);
    const odometer = eventData;
    const maxOdometer = 50000;
    const root = snapshot.after.ref.root;
    var request = require('request');

    function createCoupon() {
        let code = Math.random().toString(36).substr(2, 6);
        // make the request
        request({
            method: 'POST',
            uri: 'https://polvolouco.com.br/painel/api/default/criaCupom',
            body: JSON.stringify({
                token: 'z7j4NaT673ev3xKU',
                data: { codigo: code },
            })
        },
            function (error, response, body) {
                if (error) {
                    // this.createCoupon(); //ADICIONAR CODIGOS DE ERRO 
                    return console.error('upload failed:', error);
                }
                // console.log('Upload successful!  Server responded with:', body);

                return admin.database().ref().child('users/' + uid + '/Coupons').push({ code: code, revealed: false }).then(() => {
                    return admin.database().ref().child('Coupons/' + code).set(uid).then(() => {
                        return root.child('users/' + uid + '/FCM').once('value').then((snapshot) => {
                            if (snapshot.exists()) {
                                token = snapshot.val();
                                let payload = {
                                    notification: {
                                        title: 'Você recebeu um cupom!',
                                        body: 'Obrigado por ajudar a comunidade :)'
                                    }
                                }
                                return admin.messaging().sendToDevice(token, payload);
                            } else {
                                return null;
                            }

                        })
                    });
                });

            }
        )
    }

    if (odometer >= maxOdometer) {

        let quotient = Math.floor(odometer / maxOdometer);
        let remainder = Math.floor(odometer % maxOdometer);
        // console.log("Quotient: " + quotient);
        return root.child('users/' + uid + '/odometer').set(remainder).then(() => {
            root.child('users/' + uid + '/level').once('value', function (snapshot) {
                level = snapshot.val();
                newLevel = level + quotient;
                levelPercent = remainder / maxOdometer;
                return root.child('users/' + uid + '/level').set(newLevel).then(() => {
                    return root.child('users/' + uid + '/levelPercent').set(levelPercent).then(() => {
                        if (hasReward) {
                            createCoupon();
                        } else {
                            return;
                        }

                    })
                });
            });
        })
    } else {
        levelPercent = odometer / maxOdometer;
        return root.child('users/' + uid + '/levelPercent').set(levelPercent);
    }
}

function updateBusesHandler(snapshot) {
    const eventData = snapshot.val();
    const root = snapshot.ref.root;
    // var latitude = eventData.location.coords.latitude;
    // var longitude = eventData.location.coords.longitude;
    var latitude = eventData.location.la;
    var longitude = eventData.location.lo;
    var linha = eventData.L;
    var direction = eventData.D;
    var uid = eventData.U;
    const CUT_OFF_TIME = 10 * 60 * 1000; //5 minutes in milliseconds
    // var geofenceId = eventData.location.id;
    // var event = eventData.location.event;
    var num = 0;
    var timeStamp = Date.now();

    function deleteOldBuses(bus_key) {
        return root.child('Linhas/' + linha + '/' + direction + '/' + bus_key).remove();
    }

    function findUserIndex(user) {
        // console.log("Chamou função: " + user);
        return user === uid;
    }

    return root.child('Linhas/' + linha + '/' + direction).once('value').then(snap => { //Busca posições dos onibus existentes
        //Caso existam ônibus percorrendo a linha
        if (snap.exists()) {

            const num_children = snap.numChildren(); //Numero de onibus naquela linha
            console.log('Existem ' + num_children + ' ônibus!!');

            snap.forEach(function (child) {    //Para cada ônibus na linha
                var oldTime = child.val().time;
                var bus_key = child.key;  //PushID utilizada neste onibus
                var users = [];
                users = child.val().uid; //Pega lista de usuarios embarcados
                if (timeStamp - oldTime > CUT_OFF_TIME) {
                    console.log('Bus is outdated, deleting!');
                    deleteOldBuses(bus_key);
                }

                console.log('Key do ônibus: ' + bus_key);
                console.log('Users embarcados: ' + child);
                num = num + 1;  //Contador para quantos onibus foram testados na linha

                var bus_lat = child.val().la;	//Latitude e longitude do ônibus sendo testado
                var bus_lng = child.val().lo;

                var R = 6371; // Radius of the earth in km
                var dLat = (bus_lat - latitude) * Math.PI / 180;  // deg2rad below
                var dLon = (bus_lng - longitude) * Math.PI / 180;
                var a = 0.5 - Math.cos(dLat) / 2 + Math.cos(latitude * Math.PI / 180) * Math.cos(bus_lat * Math.PI / 180) * (1 - Math.cos(dLon)) / 2;

                var d = R * 2 * Math.asin(Math.sqrt(a));
                console.log('Distancia entre: ' + d);

                if (d < 1) {
                    console.log('Encontrou um onibus que encaixa');
                    const avg_latitude = (latitude + bus_lat) / 2; //Faz a posição média dos dois
                    const avg_longitude = (longitude + bus_lng) / 2;
                    // commited_bus = true;

                    num = num - 1; // Se posição coincide com a de um onibus existente, subtrai o contador	

                    //Checa se usuário já está ativo em uma ônibus para não adicioná-lo no array duas vezes.
                    if (users.includes(uid)) {
                        var package = {
                            la: avg_latitude,
                            lo: avg_longitude,
                            time: timeStamp
                        }
                    } else {
                        users.push(uid);
                        var package = {
                            la: avg_latitude,
                            lo: avg_longitude,
                            time: timeStamp,
                            uid: users,
                        }
                    }



                    return root.child('Linhas').child(linha + '/' + direction + '/' + bus_key).update(package);

                } else if (num == num_children) {
                    console.log('Nenhum onibus proximo. Criando um novo');
                    snap.forEach(function (child) {
                        var bus_key = child.key;
                        users = child.val().uid

                        if (users.includes(uid)) {
                            console.log('Usuário está em dois ônibus! Removendo do ônibus antigo!')
                            var index = users.findIndex(findUserIndex, users)
                            root.child('Linhas/' + linha + '/' + direction + '/' + bus_key + '/uid/' + index).remove().then(() => {
                                if (users.length <= 1) {
                                    root.child('Linhas/' + linha + '/' + direction + '/' + bus_key).remove()
                                }
                            })
                        }
                    })

                    var newUser = [];
                    newUser.push(uid);
                    var package = {
                        la: eventData.location.la,
                        lo: eventData.location.lo,
                        uid: newUser,
                        time: timeStamp
                    }

                    return root.child('Linhas/' + linha + '/' + direction).push(package);

                }
            });

            //Se não existirem ônibus percorrendo a linha, este usuário é o primeiro a embarcar
            //com o aplicativo. Logo, usa sua posição como referencia para o ônibus.
        } else {
            console.log("Nenhum ônibus existente na linha. Novo ônibus foi adicionado");
            var users = [];
            users.push(uid);
            var package = {
                la: eventData.location.la,
                lo: eventData.location.lo,
                uid: users,
                time: timeStamp
            }
            return root.child('Linhas/' + linha + '/' + direction).push(package)

        }
    });
}
//-------------------------------------------------------------------------------------------------
//-----------------------Funções para Porto Alegre ------------------------------------------------
//-------------------------------------------------------------------------------------------------
exports.notifyCommentPoa = functions.database.instance('buzee2-9c40b-d2c90').ref("/posts/postComments/{postId}/{commentId}").onCreate((snapshot, context) => {
    return notifyComment(snapshot, context)
})
exports.deleteOffensivePostPOA = functions.database.instance('buzee2-9c40b-d2c90').ref("/posts/postFeed/{pushId}/score").onUpdate((snapshot, context) => {
    return deleteOffensivePost(snapshot, context)
})
exports.deleteOffensiveCommentsPOA = functions.database.instance('buzee2-9c40b-d2c90').ref("/posts/postComments/{postId}/{commentId}/score").onUpdate((snapshot, context) => {
    return deleteOffensiveComment(snapshot, context)
})
exports.levelUpPOA = functions.database.instance('buzee2-9c40b-d2c90').ref("/users/{pushId}/level").onUpdate((snapshot, context) => {
    return levelUpNotification(snapshot, context)
})
exports.shareListenerPOA = functions.database.instance('buzee2-9c40b-d2c90').ref("/Linhas/{linha}/{direcao}/{bus}").onCreate((snapshot) => {
    return shareListenerHandler(snapshot)
});
exports.updateProfilePOA = functions.database.instance('buzee2-9c40b-d2c90').ref("/users/{pushId}/odometer").onUpdate((snapshot, context) => {
    return updateProfileHandler(snapshot, context, false)
})
exports.updateBusesPOA = functions.database.instance('buzee2-9c40b-d2c90').ref("/location/{pushId}").onCreate((snapshot, context) => {
    return updateBusesHandler(snapshot)
});
//-------------------------------------------------------------------------------------------------
//-----------------------Funções para Santa Maria -------------------------------------------------
//-------------------------------------------------------------------------------------------------
exports.notifyCommentSM = functions.database.instance('buzee2-9c40b').ref("/posts/postComments/{postId}/{commentId}").onCreate((snapshot, context) => {
    return notifyComment(snapshot, context)
})
exports.deleteOffensivePostSM = functions.database.instance('buzee2-9c40b').ref("/posts/postFeed/{pushId}/score").onUpdate((snapshot, context) => {
    return deleteOffensivePost(snapshot, context)
})
exports.deleteOffensiveCommentsSM = functions.database.instance('buzee2-9c40b').ref("/posts/postComments/{postId}/{commentId}/score").onUpdate((snapshot, context) => {
    return deleteOffensiveComment(snapshot, context)
})
exports.updateBusesSM = functions.database.instance('buzee2-9c40b').ref("/location/{pushId}").onCreate((snapshot, context) => {
    return updateBusesHandler(snapshot)
});
exports.shareListenerSM = functions.database.instance('buzee2-9c40b').ref("/Linhas/{linha}/{direcao}/{bus}").onCreate((snapshot) => {
    return shareListenerHandler(snapshot)
});
exports.updateProfileSM = functions.database.instance('buzee2-9c40b').ref("/users/{pushId}/odometer").onUpdate((snapshot, context) => {
    return updateProfileHandler(snapshot, context, true)
})
exports.levelUpSM = functions.database.instance('buzee2-9c40b').ref("/users/{pushId}/level").onUpdate((snapshot, context) => {
    return levelUpNotification(snapshot, context)
})
// exports.shareListener = functions.database.ref("/Linhas/{linha}/{direcao}/{bus}").onCreate((snapshot, context) => {
//     const eventData = snapshot.val();
//     const root = snapshot.ref.root;
//     // const key = snapshot.key;
//     let direcao = snapshot.ref.parent.key;
//     let linha = snapshot.ref.parent.parent.key;
//     var package = {
//         data: eventData,
//         L: linha,
//         D: direcao,
//     }
//     // console.log('direcao: ', snapshot.data.ref.parent.parent.key)
//     // console.log('linha: ', snapshot.data.ref.parent.parent.parent.key)
//     return root.child('Dados/ShareData/').push(package);
// });

// exports.updateBuses = functions.database.ref("/location/{pushId}").onCreate((snapshot, context) => {
//     const eventData = snapshot.val();
//     const root = snapshot.ref.root;
//     // var latitude = eventData.location.coords.latitude;
//     // var longitude = eventData.location.coords.longitude;
//     var latitude = eventData.location.la;
//     var longitude = eventData.location.lo;
//     var linha = eventData.L;
//     var direction = eventData.D;
//     var uid = eventData.U;
//     const CUT_OFF_TIME = 10 * 60 * 1000; //5 minutes in milliseconds
//     // var geofenceId = eventData.location.id;
//     // var event = eventData.location.event;
//     var num = 0;
//     var timeStamp = Date.now();

//     function deleteOldBuses(bus_key) {
//         return admin.database().ref('Linhas/' + linha + '/' + direction + '/' + bus_key).remove();
//     }

//     function findUserIndex(user) {
//         // console.log("Chamou função: " + user);
//         return user === uid;
//     }

//     return root.child('Linhas/' + linha + '/' + direction).once('value').then(snap => { //Busca posições dos onibus existentes
//         //Caso existam ônibus percorrendo a linha
//         if (snap.exists()) {

//             const num_children = snap.numChildren(); //Numero de onibus naquela linha
//             console.log('Existem ' + num_children + ' ônibus!!');

//             snap.forEach(function (child) {    //Para cada ônibus na linha
//                 var oldTime = child.val().time;
//                 var bus_key = child.key;  //PushID utilizada neste onibus
//                 var users = [];
//                 users = child.val().uid; //Pega lista de usuarios embarcados
//                 if (timeStamp - oldTime > CUT_OFF_TIME) {
//                     console.log('Bus is outdated, deleting!');
//                     deleteOldBuses(bus_key);
//                 }

//                 console.log('Key do ônibus: ' + bus_key);
//                 console.log('Users embarcados: ' + child);
//                 num = num + 1;  //Contador para quantos onibus foram testados na linha

//                 var bus_lat = child.val().la;	//Latitude e longitude do ônibus sendo testado
//                 var bus_lng = child.val().lo;

//                 var R = 6371; // Radius of the earth in km
//                 var dLat = (bus_lat - latitude) * Math.PI / 180;  // deg2rad below
//                 var dLon = (bus_lng - longitude) * Math.PI / 180;
//                 var a = 0.5 - Math.cos(dLat) / 2 + Math.cos(latitude * Math.PI / 180) * Math.cos(bus_lat * Math.PI / 180) * (1 - Math.cos(dLon)) / 2;

//                 var d = R * 2 * Math.asin(Math.sqrt(a));
//                 console.log('Distancia entre: ' + d);

//                 if (d < 1) {
//                     console.log('Encontrou um onibus que encaixa');
//                     const avg_latitude = (latitude + bus_lat) / 2; //Faz a posição média dos dois
//                     const avg_longitude = (longitude + bus_lng) / 2;
//                     // commited_bus = true;

//                     num = num - 1; // Se posição coincide com a de um onibus existente, subtrai o contador	

//                     //Checa se usuário já está ativo em uma ônibus para não adicioná-lo no array duas vezes.
//                     if (users.includes(uid)) {
//                         var package = {
//                             la: avg_latitude,
//                             lo: avg_longitude,
//                             time: timeStamp
//                         }
//                     } else {
//                         users.push(uid);
//                         var package = {
//                             la: avg_latitude,
//                             lo: avg_longitude,
//                             time: timeStamp,
//                             uid: users,
//                         }
//                     }



//                     return root.child('Linhas').child(linha + '/' + direction + '/' + bus_key).update(package);
//                     // .then(() => {
//                     //     if (event) {
//                     //         calculateTime();
//                     //         return;
//                     //     } else {
//                     //         return;
//                     //     }


//                     // });
//                 } else if (num == num_children) {
//                     console.log('Nenhum onibus proximo. Criando um novo');
//                     snap.forEach(function (child) {
//                         var bus_key = child.key;
//                         users = child.val().uid

//                         if (users.includes(uid)) {
//                             console.log('Usuário está em dois ônibus! Removendo do ônibus antigo!')
//                             var index = users.findIndex(findUserIndex, users)
//                             root.child('Linhas/' + linha + '/' + direction + '/' + bus_key + '/uid/' + index).remove().then(() => {
//                                 if (users.length <= 1) {
//                                     root.child('Linhas/' + linha + '/' + direction + '/' + bus_key).remove()
//                                 }
//                             })
//                         }
//                     })

//                     var newUser = [];
//                     newUser.push(uid);
//                     var package = {
//                         la: eventData.location.la,
//                         lo: eventData.location.lo,
//                         uid: newUser,
//                         time: timeStamp
//                     }

//                     return root.child('Linhas/' + linha + '/' + direction).push(package);
//                     // .then(() => {
//                     //     if (event) {
//                     //         calculateTime();
//                     //         return;
//                     //     } else {
//                     //         return;
//                     //     }

//                     // });
//                 }
//             });

//             //Se não existirem ônibus percorrendo a linha, este usuário é o primeiro a embarcar
//             //com o aplicativo. Logo, usa sua posição como referencia para o ônibus.
//         } else {
//             console.log("Nenhum ônibus existente na linha. Novo ônibus foi adicionado");
//             var users = [];
//             users.push(uid);
//             var package = {
//                 la: eventData.location.la,
//                 lo: eventData.location.lo,
//                 uid: users,
//                 time: timeStamp
//             }
//             return root.child('Linhas/' + linha + '/' + direction).push(package)
//             // .then(() => {
//             //     if (event) {
//             //         calculateTime();
//             //         return;
//             //     } else {
//             //         return;
//             //     }

//             // });
//         }
//     });

// });


//ESTA FUNÇÃO DÁ CUPONS PARA O USUÁRIO NO PRIMEIRO LOGIN DURANTE O TEMPO DE EVENTO
// exports.easter = functions.database.ref("/users/{pushId}/LS").onUpdate((snapshot, context) => {
//     const eventData = snapshot.after.val();
//     const uid = context.params.pushId;
//     var request = require('request');

//     function sendNotification() {
//         return admin.database().ref('users/' + uid + '/FCM').once('value').then((snapshot) => {
//             if (snapshot.exists()) {
//                 token = snapshot.val();
//                 let payload = {
//                     notification: {
//                         title: 'Feliz Páscoa!',
//                         body: 'Deixamos um cupom de Páscoa para você! 🐰'
//                     }
//                 }
//                 return admin.messaging().sendToDevice(token, payload);
//             } else {
//                 return null;
//             }

//         })
//     }

//     function createCoupon() {
//         let code = Math.random().toString(36).substr(2, 6);
//         // make the request
//         request({
//             method: 'POST',
//             uri: 'https://polvolouco.com.br/painel/api/default/criaCupom',
//             body: JSON.stringify({
//                 token: 'z7j4NaT673ev3xKU',
//                 data: { codigo: code },
//             })
//         },
//             function (error, response, body) {
//                 if (error) {
//                     // this.createCoupon(); //ADICIONAR CODIGOS DE ERRO 
//                     return console.error('upload failed:', error);
//                 }
//                 // console.log('Upload successful!  Server responded with:', body);
//                 return admin.database().ref().child('users/' + uid + '/Coupons').push({ code: code, revealed: false }).then(() => {
//                     return admin.database().ref().child('Coupons/' + code).set(uid).then(()=>{
//                         return admin.database().ref().child('users/' + uid + '/jaRecebeu').set('true').then(()=>{
//                             sendNotification();
//                         })
//                     });
//                 });

//             }
//         )
//     }

//     return admin.database().ref().child('users/' + uid + '/jaRecebeu').once('value',  (snapshot) => {
//         const eventData = snapshot.val();
//         console.log(eventData)
//         if (eventData !== 'true') {
//             console.log('Criando cupom')
//             createCoupon();
//         } else {
//             console.log('Já tem cupom')
//             return null
//         }
//     })
// });

// exports.updateProfile = functions.database.ref("/users/{pushId}/odometer").onUpdate((snapshot, context) => {
//     const eventData = snapshot.after.val();
//     const uid = context.params.pushId;
//     // console.log(eventData);
//     const odometer = eventData;
//     const maxOdometer = 50000;
//     var request = require('request');



//     function createCoupon() {
//         let code = Math.random().toString(36).substr(2, 6);
//         // make the request
//         request({
//             method: 'POST',
//             uri: 'https://polvolouco.com.br/painel/api/default/criaCupom',
//             body: JSON.stringify({
//                 token: 'z7j4NaT673ev3xKU',
//                 data: { codigo: code },
//             })
//         },
//             function (error, response, body) {
//                 if (error) {
//                     // this.createCoupon(); //ADICIONAR CODIGOS DE ERRO 
//                     return console.error('upload failed:', error);
//                 }
//                 // console.log('Upload successful!  Server responded with:', body);
//                 return admin.database().ref().child('users/' + uid + '/Coupons').push({ code: code, revealed: false }).then(() => {
//                     return admin.database().ref().child('Coupons/' + code).set(uid);
//                 });

//             }
//         )

//         //USAR A FUNÇÃO ACIMA, ESTA ABAIXO É APENAS PARA TESTES E PULA O POLVO LOUCO!!!!!
//         // return admin.database().ref().child('users/' + uid + '/Coupons').push({ code: code, revealed: false }).then(() => {
//         //     return admin.database().ref().child('Coupons/' + code).set(uid);
//         // });

//     }

//     if (odometer >= maxOdometer) {

//         let quotient = Math.floor(odometer / maxOdometer);
//         let remainder = Math.floor(odometer % maxOdometer);
//         // console.log("Quotient: " + quotient);
//         return admin.database().ref().child('users/' + uid + '/odometer').set(remainder).then(() => {
//             admin.database().ref().child('users/' + uid + '/level').once('value', function (snapshot) {
//                 level = snapshot.val();
//                 newLevel = level + quotient;
//                 levelPercent = remainder / maxOdometer;
//                 return admin.database().ref().child('users/' + uid + '/level').set(newLevel).then(() => {
//                     return admin.database().ref().child('users/' + uid + '/levelPercent').set(levelPercent).then(() => {
//                         createCoupon();
//                     })
//                 });
//             });
//         })
//     } else {
//         levelPercent = odometer / maxOdometer;
//         return admin.database().ref().child('users/' + uid + '/levelPercent').set(levelPercent);
//     }

// })

exports.couponUsed = functions.https.onRequest((req, res) => {
    // Grab the text parameter.r
    const response = JSON.parse(req.body);
    const token = response.token;
    const couponCode = response.couponCode;
    if (token == '14bYsZskpv') {
        return admin.database().ref('Coupons/' + couponCode).once('value').then((snap) => {
            uid = snap.val();
            return admin.database().ref('Coupons/' + couponCode).remove().then(() => {
                return admin.database().ref().child('users/' + uid + '/Coupons').once('value').then((snap) => {

                    snap.forEach(coupon => {
                        if (coupon.val().code == couponCode) {
                            return admin.database().ref().child('users/' + uid + '/Coupons/' + coupon.key).remove().then(() => {
                                return res.status(200).end();
                            });
                        }
                    })
                })
            })

        })
    } else {
        return res.status(401).send("Token invalido");
    }


});

exports.getAllUserEmails = functions.https.onRequest((req, res) => {
    console.log('FUNÇÃO DE BUSCAR USERS CHAMADA')
    let emailList = [];
    function listAllUsers(nextPageToken) {
        // List batch of users, 1000 at a time.
        admin.auth().listUsers(100, nextPageToken)
            .then(function (listUsersResult) {
                listUsersResult.users.forEach(function (userRecord) {
                    //   console.log('user', userRecord.toJSON());
                    user = userRecord.toJSON();
                    emailList.push(user.email)
                });
                console.log(emailList)
                emailList = [];
                if (listUsersResult.pageToken) {
                    // List next batch of users.
                    listAllUsers(listUsersResult.pageToken);
                }
            })
            .catch(function (error) {
                console.log('Error listing users:', error);
            });
    }
    // Start listing users from the beginning, 1000 at a time.
    listAllUsers();
});