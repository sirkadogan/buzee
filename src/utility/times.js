const times = [
    {
        time: "00:00",
        description: "FAIXA VELHA"
    },
    {
        time: "00:35",
        description: "FAIXA VELHA"
    },

    {
        time: "06:35",
        description: "FAIXA VELHA"
    },
    {
        time: "06:50",
        description: "FAIXA VELHA"
    },

    {
        time: "07:05",
        description: "FAIXA VELHA"
    },

    {
        time: "07:20",
        description: "FAIXA VELHA"
    },
    {
        time: "07:50",
        description: "FAIXA VELHA"
    },
    {
        time: "08:00",
        description: "FAIXA NOVA"
    },
    {
        time: "08:05",
        description: "FAIXA NOVA"
    },
    {
        time: "08:10",
        description: "FAIXA NOVA"
    },
    {
        time: "08:20",
        description: "FAIXA NOVA"
    },
    {
        time: "08:35",
        description: "FAIXA NOVA"
    },
    {
        time: "13:03",
        description: "FAIXA VELHA"
    },
    {
        time: "13:18",
        description: "FAIXA VELHA"
    },
    {
        time: "13:25",
        description: "FAIXA NOVA"
    },
    {
        time: "13:30",
        description: "FAIXA VELHA"
    },
    {
        time: "13:45",
        description: "FAIXA VELHA"
    },
    {
        time: "13:55",
        description: "FAIXA VELHA"
    },
    {
        time: "14:05",
        description: "FAIXA VELHA"
    },
    {
        time: "14:25",
        description: "FAIXA VELHA"
    },
    {
        time: "14:35",
        description: "FAIXA VELHA"
    },
    {
        time: "14:45",
        description: "FAIXA VELHA"
    },
    {
        time: "15:00",
        description: "FAIXA VELHA"
    },
]

export default times;






