import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    FlatList,
    ActivityIndicator,
    ScrollView,


} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import BackgroundGeolocation from "react-native-background-geolocation";
import firebase from "react-native-firebase";
import { ButtonGroup, Divider, SearchBar } from 'react-native-elements';

class AssaultScreen extends Component {
    constructor(props) {
        super(props);
    }


    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    openFinishAssaultScreen = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.FinishAssaultScreen',
                        passProps: {
                            sharing: this.props.sharing,
                            selectedLine: this.props.selectedLine,
                            selectedLineName: this.props.selectedLineName,
                            linesList: this.props.linesList,
                        }

                    }
                }]
            }
        });
    }


    render() {



        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '100%', width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='#EFE4CF' />
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ width: '80%' }}>
                            <Text style={styles.titleStyle}>Denunciar assédio</Text>
                        </View>

                    </View>


                </View>

                <View style={styles.middleContainer}>

                    <ScrollView >
                        <Text style={styles.textStyle}>Se sentir que está em perigo, chame a polícia e peça ajuda. Jamais ponha sua segurança em risco</Text>
                        <Text style={styles.textStyle}>Sente-se próxima ao cobrador ou a qualquer pessoa do ônibus, por mais que não a conheça.</Text>
                        <Text style={styles.textStyle}>Evite sentar no lado da janela para que o agressor não impeça sua passagem. </Text>
                        <Text style={styles.textStyle}>Denuncie o assédio formalmente na Delegacia da Mulher, já que esta ferramenta serve apenas para um levantamento estatístico e não é um canal oficial de denúncias.</Text>



                    </ScrollView>
                </View>



                <View style={styles.bottomContainer}>
                    <TouchableWithoutFeedback onPress={this.openFinishAssaultScreen}>
                        <View style={{ elevation: 4, backgroundColor: '#EFE4CF', borderRadius: 4, ...styles.buttonGroup, borderColor: 'white', borderWidth: 1, justifyContent: 'center' }}>
                            <Text style={styles.textStyle}>Denunciar</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#7B3E63'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#A15B37',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    middleContainer: {
        height: '80%',
        width: '100%',
        backgroundColor: '#EFE4CF',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    bottomContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#A15B37',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: '#A15B37',
        fontSize: 20,
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center',
        margin: 10
    },
    titleStyle: {
        color: '#EFE4CF',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '80%',
        elevation: 2

    }

});


export default AssaultScreen;
