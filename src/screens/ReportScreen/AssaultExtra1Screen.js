import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    FlatList,
    ActivityIndicator,
    ToastAndroid,


} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import BackgroundGeolocation from "react-native-background-geolocation";
import firebase from "react-native-firebase";
import { ButtonGroup, Divider, SearchBar } from 'react-native-elements';

class AssaultExtra1Screen extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        lines: this.props.linesList,
        dataSource: this.props.linesList

    }

    searchFilterFunction = (text) => {
        //passing the inserted text in textinput
        let lines = this.state.lines;
        const newData = lines.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.line ? item.line.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            dataSource: newData,
            search: text,
        });
    }

    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    selectedLineHandler = (itemValue) => {
        console.log(itemValue);
        this.setState({ selectedLineKey: itemValue.key, selectedLine: itemValue });
    }

    buttonHandler = () => {
        let content = (this.state.selectedLine)
        // firebase.database().ref().child('Dados/Assault/' + this.props.reportKey + '/line').set(this.state.selectedLine.line);
        // firebase.database().ref().child('Dados/Assault/' + this.props.reportKey + '/key').set(this.state.selectedLine.key);
        // firebase.database().ref().child('Dados/Assault/' + this.props.reportKey + '/line').set(content.key);
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.AssaultExtra2Screen',
                        passProps: {
                            content: content
                        }
                    }
                }]
            }
        });
    }


    render() {
        const { search } = this.state.lines;


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '50%', width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10  }}>
                                <Icon size={30} name='chevron-left' color='#EFE4CF' />
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ width: '80%' }}>
                            <Text style={styles.titleStyle}>Qual linha?</Text>
                        </View>

                    </View>

                    <View style={{ height: '50%', width: '100%', alignItems: 'center' }}>
                        <SearchBar
                            placeholder="Pesquisar"
                            onChangeText={this.searchFilterFunction}
                            value={this.state.search}
                            onClear={text => this.SearchFilterFunction('')}
                            platform={'android'}
                            searchIcon={<Icon size={30} name='search' color='#A15B37' />}
                            cancelIcon={<Icon size={30} name='search' color='#A15B37' />}
                            clearIcon={null}
                            containerStyle={{ backgroundColor: '#EFE4CF', marginBottom: 4 }}
                            inputContainerStyle={{ backgroundColor: '#EFE4CF', width: '100%', height: '100%' }}
                            placeholderTextColor='#A15B37'
                            inputStyle={{ fontFamily: 'BarlowCondensed-Light', color: '#A15B37' }}
                        />
                    </View>

                </View>


                <View style={styles.middleContainer}>
                    <FlatList
                        style={{ width: '100%' }}
                        data={this.state.dataSource}
                        renderItem={({ item, index }) => (
                            <TouchableWithoutFeedback onPress={() => this.selectedLineHandler(item)}>
                                <View style={{
                                    backgroundColor: index % 2 ? '#EFE4CF' : '#A15B37',
                                    paddingTop: 20,
                                    paddingBottom: 20,
                                    alignItems: 'center',
                                    borderWidth: item.key == this.state.selectedLineKey ? 3 : null,
                                    borderColor: 'white'
                                }}>

                                    <Text style={{ ...styles.textStyle, color: index % 2 ? '#A15B37': '#EFE4CF' }}>{item.line}</Text>


                                </View>
                            </TouchableWithoutFeedback>
                        )}
                    />
                </View>


                <View style={styles.bottomContainer}>
                    <TouchableWithoutFeedback onPress={this.buttonHandler}>
                        <View style={{ elevation: 4, backgroundColor: '#EFE4CF', borderRadius: 4, ...styles.buttonGroup, borderColor: 'white', borderWidth: 1, justifyContent: 'center' }}>
                            <Text style={styles.textStyle}>Próximo</Text>
                        </View>
                    </TouchableWithoutFeedback>

                </View>                 
               
            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#7B3E63'
    },
    topContainer: {
        height: '20%',
        width: '100%',
        backgroundColor: '#A15B37',

        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    middleContainer: {
        height: '70%',
        width: '100%',
        backgroundColor: '#EFE4CF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bottomContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#A15B37',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: '#A15B37',
        fontSize: 20,
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center',
        margin: 10
    },
    titleStyle: {
        color: '#EFE4CF',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '80%',
        elevation: 2

    }
});


export default AssaultExtra1Screen;
