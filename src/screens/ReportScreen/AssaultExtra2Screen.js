import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    Alert,
    ToastAndroid,
    TextInput,


} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import BackgroundGeolocation from "react-native-background-geolocation";
import firebase from "react-native-firebase";
import { ButtonGroup, Divider, SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';

class AssaultExtra2Screen extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        reportKey: null,
        story: null
    }


    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    updateStory = (val) => {
        this.setState(prevState => {
            return {
                story: val
            }
        })
    }

    buttonHandler = () => {
        Alert.alert(
            'Enviar denúncia?',
            ' ',
            [
                {
                    text: 'Cancelar',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Sim', onPress: () => {
                        this.firebaseRef = this.props.firebaseRef.firebaseRef
                        BackgroundGeolocation.getCurrentPosition({
                            timeout: 30,          // 30 second timeout to fetch location
                            maximumAge: 5000,     // Accept the last-known-location if not older than 5000 ms.
                            desiredAccuracy: 10,  // Try to fetch a location with an accuracy of `10` meters.
                            samples: 3,           // How many location samples to attempt.

                        }).then((position) => {
                            let content;
                            content = { ...this.props.content, date: Date.now(), position: position.coords, story: this.state.story }
                            this.firebaseRef.database().ref().child('Dados/Assault/').push(content);
                            ToastAndroid.show('Sua denúncia foi registrada!', ToastAndroid.SHORT);
                            Navigation.dismissAllModals();
                        }).catch((err) => {
                            console.warn(err);
                            ToastAndroid.show('Sua denúncia foi registrada!', ToastAndroid.SHORT);
                            Navigation.dismissAllModals();
                        });
                        
                        
                    }
                },
            ],
            { cancelable: true },
        );

    }



    render() {


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '100%', width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='#EFE4CF' />
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ width: '80%' }}>
                            <Text style={styles.titleStyle}>O que aconteceu?</Text>
                        </View>

                    </View>


                </View>


                <View style={styles.middleContainer}>
                    <Text style={styles.textStyle}>O relato é opcional</Text>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={{ width: '90%', height: '50%', fontFamily: 'BarlowCondensed-Light', color: '#A15B37', fontSize: 18 }}
                        placeholder='Relate o acontecido'
                        value={this.state.story}
                        onChangeText={(val) => this.updateStory(val)}
                        placeholderTextColor='#A15B37'
                        multiline={true}

                    />


                </View>

                <View style={styles.bottomContainer}>
                    <TouchableWithoutFeedback onPress={this.buttonHandler}>
                        <View style={{ elevation: 4, backgroundColor: '#EFE4CF', borderRadius: 4, ...styles.buttonGroup, borderColor: 'white', borderWidth: 1, justifyContent: 'center' }}>
                            <Text style={styles.textStyle}>Enviar</Text>
                        </View>
                    </TouchableWithoutFeedback>

                </View>

            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#7B3E63'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#A15B37',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    middleContainer: {
        height: '80%',
        width: '100%',
        backgroundColor: '#EFE4CF',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    bottomContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#A15B37',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: '#A15B37',
        fontSize: 20,
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center',
        margin: 10
    },
    titleStyle: {
        color: '#EFE4CF',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '80%',
        elevation: 2

    }
});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(AssaultExtra2Screen);

