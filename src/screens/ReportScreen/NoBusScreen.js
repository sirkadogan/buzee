import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    ScrollView,
    FlatList,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';

import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Divider } from 'react-native-elements';


class NoBusScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);

    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    openNextScreen = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.NoBusScreen2',
                        passProps: {
                            ...this.props
                        }
                    }
                }]
            }
        });
    }

    render() {

        let content = 'Ficou um tempão esperando na parada e o ônibus não passou?\n'

        let content2 = 'Envie sua reclamação aqui. Juntos temos força pra exigir mudanças!\n'
        let content3 = 'Mas lembre-se: o trânsito é uma coisa orgânica e atrasos de alguns minutos são normais. Use esta ferramenta apenas se acreditar que não estejam cumprindo o cronograma.\n'


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>

                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Ônibus não passou</Text>
                </View>



                <View style={styles.middleContainer}>

                    <Text style={styles.textStyle}>{content}</Text>
                    <Text style={styles.textStyle}>{content2}</Text>
                    <Text style={styles.textStyle}>{content3}</Text>
                    <TouchableWithoutFeedback onPress={this.openNextScreen}>

                        <View style={{ borderRadius: 10, borderWidth: 3, borderColor: 'white', padding: 10, alignItems: 'center', justifyContent: 'center', width: '80%', marginBottom: 20 }}>
                            <Text style={{ ...styles.textStyle, fontSize: 22 }}>Notificar</Text>
                        </View>

                    </TouchableWithoutFeedback>



                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center',
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

export default (NoBusScreen);