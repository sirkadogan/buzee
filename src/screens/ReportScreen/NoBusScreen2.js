import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    FlatList,
    Alert,
    ToastAndroid,


} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import BackgroundGeolocation from "react-native-background-geolocation";
import firebase from "react-native-firebase";
import { ButtonGroup, Divider, SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';

class NoBusScreen2 extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        sharing: this.props.sharing,
        lines: this.props.linesList,
        dataSource: this.props.linesList,
        selectedLine: null

    }

    searchFilterFunction = (text) => {
        //passing the inserted text in textinput
        let lines = this.state.lines;
        const newData = lines.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.line ? item.line.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            dataSource: newData,
            search: text,
        });
    }


    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    selectedLineHandler = (itemValue) => {
        console.log(itemValue);
        this.setState({ selectedLineKey: itemValue.key, selectedLine: itemValue });
    }

    buttonHandler = () => {

        if (this.state.selectedLine) {
            Alert.alert(
                'Este ônibus não passou?',
                this.state.selectedLine.line,
                [
                    {
                        text: 'Cancelar',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Isso', onPress: () => {
                            this.firebaseRef = this.props.firebaseRef.firebaseRef
                            BackgroundGeolocation.getCurrentPosition({
                                timeout: 30,          // 30 second timeout to fetch location
                                maximumAge: 5000,     // Accept the last-known-location if not older than 5000 ms.
                                desiredAccuracy: 10,  // Try to fetch a location with an accuracy of `10` meters.
                                samples: 3,           // How many location samples to attempt.

                            }).then((position) => {
                                let content = { ...this.state.selectedLine, date: Date.now(), position: position.coords }
                                this.firebaseRef.database().ref().child('Dados/NoBus/').push(content);
                                this.props.addCounterOnProfile('noBus');
                                this.props.addPoints();
                                ToastAndroid.show('Sua reclamação foi registrada! Obrigado por colaborar. \n + 3500 pontos', ToastAndroid.SHORT);
                            }).catch((err) => {
                                console.warn(err);
                                ToastAndroid.show('Permita o acesso a localização para registrar esta ação.', ToastAndroid.LONG);
                            });
                            Navigation.dismissModal(this.props.componentId)
                        }
                    },
                ],
                { cancelable: true },
            );
        } else {
            alert('Selecione a linha que não passou!');
        }

    }


    render() {
        const { search } = this.state.lines;


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '50%', width: '100%', flexDirection: 'row', justifyContent: 'center' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='white' />
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ width: '80%' }}>
                            <Text style={styles.titleStyle}>Qual ônibus não passou?</Text>
                        </View>

                    </View>

                    <View style={{ height: '50%', width: '100%', alignItems: 'center' }}>
                        <SearchBar
                            placeholder="Pesquisar"
                            onChangeText={this.searchFilterFunction}
                            value={this.state.search}
                            onClear={text => this.SearchFilterFunction('')}
                            platform={'android'}
                            searchIcon={<Icon size={30} name='search' color='white' />}
                            cancelIcon={<Icon size={30} name='search' color='white' />}
                            clearIcon={null}
                            containerStyle={{ backgroundColor: '#5E9BD4' }}
                            inputContainerStyle={{ backgroundColor: '#5E9BD4', width: '100%', height: '100%' }}
                            placeholderTextColor='white'
                            inputStyle={{ fontFamily: 'BarlowCondensed-Light', color: 'white' }}
                        />
                    </View>

                </View>


                <View style={styles.middleContainer}>

                    <FlatList
                        style={{ width: '100%', height: '100%' }}
                        data={this.state.dataSource}
                        renderItem={({ item, index }) => (
                            <TouchableWithoutFeedback onPress={() => this.selectedLineHandler(item)}>
                                <View style={{
                                    backgroundColor: index % 2 ? '#5E9BD4' : '#0854A2',
                                    paddingTop: 20,
                                    paddingBottom: 20,
                                    alignItems: 'center',
                                    borderWidth: item.key == this.state.selectedLineKey ? 3 : null,
                                    borderColor: 'white'
                                }}>

                                    <Text style={styles.textStyle}>{item.line}</Text>


                                </View>
                            </TouchableWithoutFeedback>
                        )}
                    />
                </View>



                <View style={styles.bottomContainer}>
                    <TouchableWithoutFeedback onPress={this.buttonHandler}>
                        <View style={{ elevation: 2, backgroundColor: '#5E9BD4', borderRadius: 4, ...styles.buttonGroup, borderColor: 'white', borderWidth: 1, justifyContent: 'center' }}>
                            <Text style={styles.textStyle}>Confirmar</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>

            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    topContainer: {
        height: '20%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'flex-start',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '70%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    bottomContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '80%',
        elevation: 2
    },
    titleStyle: {
        color: '#EFE4CF',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(NoBusScreen2);


