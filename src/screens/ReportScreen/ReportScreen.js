import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    ScrollView,
    ActivityIndicator,
    ToastAndroid,
    Alert


} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
// import * as firebase from 'firebase';
import firebase from "react-native-firebase";
import technical from '../../assets/Technical.png';
import dirty from '../../assets/dirty.png';
import noBus from '../../assets/noBus.png';
import crazy from '../../assets/crazy.png';
import { connect } from 'react-redux';

class ReportScreen extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        sharing: this.props.sharing,
        dirtyReported: this.props.dirtyReported,
        crazyReported: this.props.crazyReported,
        rating: this.props.rating,
        ratingReported: this.props.ratingReported
    }

    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef
    }

    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    openNoBusScreen = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.NoBusScreen',
                        passProps: {
                            sharing: this.state.sharing,
                            linesList: this.props.linesList,
                            addPoints: this.addPoints,
                            addCounterOnProfile: this.addCounterOnProfile
                        }
                    }
                }]
            }
        });
    }

    addPoints = () => {
        let uid = this.props.uid;
        return this.firebaseRef.database().ref().child('users/' + uid + '/odometer')
            .transaction((current_value) => {
                console.log(current_value)
                if (current_value) {
                    return (current_value || 0) + 2500;
                } else {
                    return 2500;
                }

            });
    }


    addCounterOnProfile = (value) => {
        let uid = this.props.uid
        this.firebaseRef.database().ref().child('users/' + uid + '/game/' + value + '/')
            .transaction((current_value) => {
                console.log(current_value)
                if (current_value) {
                    return (current_value || 0) + 1;
                } else {
                    return 1;
                }

            });
    }

    openAssaultScreen = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.AssaultScreen',
                        passProps: {
                            sharing: this.props.sharing,
                            selectedLine: this.props.selectedLine,
                            selectedLineName: this.props.selectedLineName,
                            linesList: this.props.linesList,
                        }
                    }
                }]
            }
        });
    }

    openTechnicalScreen = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.TechnicalScreen',
                        passProps: {
                            sharing: this.props.sharing,
                            selectedLine: this.props.selectedLine,
                            selectedLineName: this.props.selectedLineName,
                            addPoints: this.addPoints,
                            linesList: this.props.linesList,
                            addCounterOnProfile: this.addCounterOnProfile
                        }
                    }
                }]
            }
        });
    }

    dirtyBus = () => {
        if (this.state.sharing) {

            Alert.alert(
                'Reportar ônibus sujo',
                'Este ônibus está sujo?',
                [
                    {
                        text: 'Não',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sim', onPress: () => {
                            this.addPoints();
                            this.setState({
                                dirtyReported: true
                            })
                            this.addCounterOnProfile('dirty');
                            this.props.reportDirty();
                            ToastAndroid.show('Sua reclamação foi registrada! Obrigado por colaborar. \n + 2500 pontos', ToastAndroid.SHORT);
                            return this.firebaseRef.database().ref().child('Dados/dirtyBus/' + this.props.selectedLine + '/').push(Date.now());

                        }
                    },
                ],
                { cancelable: true },
            );

        }

    }

    crazyDriver = () => {
        if (this.state.sharing) {

            Alert.alert(
                'Reportar motorista imprudente',
                'Este motorista está dirigindo de forma perigosa?',
                [
                    {
                        text: 'Não',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sim', onPress: () => {
                            this.addPoints();
                            this.setState({
                                crazyReported: true
                            })
                            this.addCounterOnProfile('crazy');
                            this.props.reportCrazy();
                            ToastAndroid.show('Sua reclamação foi registrada! Obrigado por colaborar. \n + 2500 pontos', ToastAndroid.SHORT);
                            return this.firebaseRef.database().ref().child('Dados/crazyDriver/' + this.props.selectedLine + '/').push(Date.now());

                        }
                    },
                ],
                { cancelable: true },
            );

        }
    }

    setRating = (rating) => {
        if (this.state.sharing) {
            let uid = this.props.uid;
            this.setState({
                rating: rating
            })

            Alert.alert(
                'Avaliar a viagem',
                'Enviar a nota ' + rating + '?',
                [
                    {
                        text: 'Não',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sim', onPress: () => {
                            this.addPoints();
                            this.setState({
                                ratingReported: true
                            })
                            this.props.reportRating(this.state.rating);
                            ToastAndroid.show('Sua avaliação foi registrada! Obrigado por colaborar. \n +2500 pontos', ToastAndroid.SHORT);
                            //Somatorio da quantidade de usuarios que fizeram rating da linha selecionada
                            this.firebaseRef.database().ref().child('Dados/rating/' + this.props.selectedLine + '/num')
                                .transaction((current_value) => {
                                    // console.log(current_value)

                                    if (current_value) {
                                        return (current_value || 0) + 1;
                                    } else {
                                        return 1;
                                    }
                                });
                            //Faz a média do rating do usuário do rating geral e salva no FB
                            this.firebaseRef.database().ref().child('Dados/rating/' + this.props.selectedLine + '/rating')
                                .transaction((current_val) => {
                                    // console.log('rating atual ' + current_val + ' num: '+ num)
                                    // console.log('state ', this.state.rating)
                                    if (current_val) {
                                        console.log('entrou no val')
                                        return ((current_val + this.state.rating) / 2);
                                    } else {
                                        console.log('entrou nom sem val')
                                        return (this.state.rating);
                                    }
                                });

                            //Salva a média de nota no perfil do usuario
                            this.firebaseRef.database().ref().child('users/' + uid + '/game/rating/')
                                .transaction((current_val) => {
                                    if (current_val) {
                                        console.log('entrou no val')
                                        return ((current_val + this.state.rating) / 2);
                                    } else {
                                        console.log('entrou nom sem val')
                                        return (this.state.rating);
                                    }
                                });
                        }
                    },
                ],
                { cancelable: true },
            );
        }

    }



    showTutorial = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.ReportTutorial',
                    }
                }]
            }
        });
    }

    render() {
        let shareCover;
        let shareCoverText;
        if (this.state.sharing) {
            shareCover = null;
        } else {
            shareCover = (
                <View style={{ backgroundColor: 'rgba(128,128,128,0.8)', position: 'absolute', right: 0, left: 0, bottom: 0, top: 0 }}>
                </View>
            )
            shareCoverText = (
                <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(128,128,128,0.8)', position: 'absolute', right: 0, left: 0, bottom: 0, top: 0 }}>
                    <View style={{ width: '80%' }}>
                        <Text style={styles.textStyle}>Compartilhe sua localização para liberar</Text>
                    </View>

                </View>
            )
        }


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Reportar</Text>

                    <TouchableWithoutFeedback onPress={this.showTutorial}>
                        <View style={{ position: 'absolute', right: 0, padding: 10 }}>
                            <Icon size={30} name='question-circle' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                </View>


                <View style={styles.middleContainer}>

                    <View style={styles.largeContainer}>
                        <View style={styles.largeButtonStyle}>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '80%' }}>
                                <TouchableWithoutFeedback onPress={this.state.ratingReported ? null : () => this.setRating(1)}>
                                    <Icon size={40} name='star' color={this.state.rating >= 1 ? '#FED162' : 'white'} regular />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={this.state.ratingReported ? null : () => this.setRating(2)}>
                                    <Icon size={40} name='star' color={this.state.rating >= 2 ? '#FED162' : 'white'} regular />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={this.state.ratingReported ? null : () => this.setRating(3)}>
                                    <Icon size={40} name='star' color={this.state.rating >= 3 ? '#FED162' : 'white'} regular />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={this.state.ratingReported ? null : () => this.setRating(4)}>
                                    <Icon size={40} name='star' color={this.state.rating >= 4 ? '#FED162' : 'white'} regular />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={this.state.ratingReported ? null : () => this.setRating(5)}>
                                    <Icon size={40} name='star' color={this.state.rating >= 5 ? '#FED162' : 'white'} regular />
                                </TouchableWithoutFeedback>


                            </View>

                            <View style={{ padding: 10 }}>
                                <Text style={{ ...styles.textStyle, fontSize: 16 }}>{this.state.ratingReported ? 'Avaliação enviada' : 'Avalie esta viagem'}</Text>
                            </View>


                            {shareCoverText}
                        </View>

                    </View>

                    <View style={styles.largeContainer}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableWithoutFeedback onPress={this.state.dirtyReported ? null : this.dirtyBus}>
                                <View style={styles.smallButtonStyle}>
                                    <Image style={styles.imageStyle} source={dirty} />
                                    <Text style={{ ...styles.textStyle, fontSize: 16 }}>{this.state.dirtyReported ? 'Avaliação enviada' : 'Ônibus sujo'}</Text>
                                    {shareCover}
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={this.state.crazyReported ? null : this.crazyDriver}>
                                <View style={styles.smallButtonStyle}>
                                    <Image style={styles.imageStyle} source={crazy} />
                                    <Text style={{ ...styles.textStyle, fontSize: 16 }}>{this.state.crazyReported ? 'Avaliação enviada' : 'Motorista imprudente'}</Text>
                                    {shareCover}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>


                    <View style={styles.largeContainer}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableWithoutFeedback onPress={this.openNoBusScreen}>
                                <View style={styles.smallButtonStyle}>
                                    <Image style={styles.imageStyle} source={noBus} />
                                    <Text style={{ ...styles.textStyle, fontSize: 16 }}>Ônibus não passou</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={this.openTechnicalScreen}>
                                <View style={styles.smallButtonStyle}>
                                    <Icon size={50} name='exclamation-triangle' color='white' />
                                    <Text style={{ ...styles.textStyle, fontSize: 16 }}>Problema técnico</Text>
                                </View>
                            </TouchableWithoutFeedback>

                        </View>
                    </View>

                    <View style={styles.largeContainer}>
                        <TouchableWithoutFeedback onPress={this.openAssaultScreen}>
                            <View style={{ ...styles.largeButtonStyle, backgroundColor: '#A15B37' }}>
                                <Text style={{ ...styles.textStyle, fontSize: 30 }}>Denunciar assédio</Text>
                            </View>

                        </TouchableWithoutFeedback>

                    </View>
                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',

        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    largeContainer: {
        flex: 1,
        width: '100%',
        padding: 5
    },
    largeButtonStyle: {
        flex: 1,
        // margin: 5,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: 'white',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-evenly',

    },
    smallButtonStyle: {
        // flex: 1,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: 'white',
        height: '100%',
        width: (window_width - 20) / 2,
        alignItems: 'center',
        justifyContent: 'space-evenly',

    },
    imageStyle: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    }
});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(ReportScreen);
