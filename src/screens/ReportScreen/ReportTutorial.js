import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    ScrollView,
    FlatList,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';

import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Divider } from 'react-native-elements';


class ReportTutorial extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);
        this.state = {
            percent: 0,
            level: 1,
            coupons: [],
            revealed: false,
            selectedCoupon: null,
            tutorial: null
        };
    }


    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    render() {

        let content = 'Quantas vezes você já esperou na parada e seu ônibus não passou? Ou então estava indo pro trabalho e o ônibus quebrou no caminho? \n'
        let content2 = 'Também já passamos muito por isso e percebemos que nós, os passageiros, não temos voz para reclamar do serviço que nos é prestado.\n';
        let content3 = 'Por isso criamos esta funcionalidade de report no Buzee. É a sua chance de ter voz e pedir por um serviço melhor!\n'
        let content4 = 'Estes dados serão exibidos em nossa página na internet para colocar um holofote nos problemas do transporte coletivo urbano e, assim, gerar mudança! \n'
        let content5 = 'Ajude sua comunidade, mostre sua voz!\n'

        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Ajuda</Text>
                </View>



                <View style={styles.middleContainer}>

                    <ScrollView >
                        <Text style={{...styles.titleStyle, fontFamily:'Barlow-BoldItalic', marginBottom: 20}}>Para que servem os reports?</Text>
                        <Text style={styles.textStyle}>{content}</Text>
                        <Text style={styles.textStyle}>{content2}</Text>
                        <Text style={styles.textStyle}>{content3}</Text>
                        <Text style={styles.textStyle}>{content4}</Text>
                        <Text style={styles.textStyle}>{content5}</Text>
                    </ScrollView>

                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

export default (ReportTutorial);