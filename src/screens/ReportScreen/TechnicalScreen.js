import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    ScrollView,
    ActivityIndicator,
    ToastAndroid,


} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
// import * as firebase from 'firebase';
import firebase from "react-native-firebase";
import flattire from '../../assets/flattire.png';
import technical from '../../assets/Technical.png';
import crash from '../../assets/crash.png';
import BackgroundGeolocation from "react-native-background-geolocation";
import { connect } from 'react-redux';

class TechnicalScreen extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        sharing: this.props.sharing
    }
    
    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    complete = (condition) => {
        this.firebaseRef = this.props.firebaseRef.firebaseRef
        BackgroundGeolocation.getCurrentPosition({
            timeout: 30,          // 30 second timeout to fetch location
            maximumAge: 5000,     // Accept the last-known-location if not older than 5000 ms.
            desiredAccuracy: 10,  // Try to fetch a location with an accuracy of `10` meters.
            samples: 3,           // How many location samples to attempt.

        }).then((position) => {
            let content;
            if (this.props.sharing) {
                content = { line: this.props.selectedLineName, key: this.props.selectedLine, date: Date.now(), position: position.coords }
            } else {
                content = { date: Date.now(), position: position.coords }
            }

            this.firebaseRef.database().ref().child('Dados/technical/' + condition).push(content);
        });
        this.props.addPoints();
        ToastAndroid.show('Sua reclamação foi registrada! Obrigado por colaborar. \n + 2500 pontos', ToastAndroid.SHORT);
        this.close();
    }

    openTechDetailScreen = (condition) => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.TechDetailScreen',
                        passProps: {
                            sharing: this.state.sharing,
                            linesList: this.props.linesList,
                            addPoints: this.props.addPoints,
                            condition: condition,
                            addCounterOnProfile: this.props.addCounterOnProfile
                        }
                    }
                }]
            }
        });
    }

    render() {

        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>

                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>

                    <View style={{ width: '80%' }}>
                        <Text style={styles.titleStyle}>O que houve?</Text>
                    </View>


                </View>


                <View style={styles.middleContainer}>
                    <TouchableWithoutFeedback onPress={this.props.sharing ? () => this.complete('crash') : () => this.openTechDetailScreen('crash')}>
                        <View style={styles.largeContainer}>
                            <View style={styles.largeButtonStyle}>
                            <Image style={{ width: 50, height: 50, resizeMode: 'contain' }} source={crash} />
                                <Text style={{ ...styles.textStyle, fontSize: 16 }}>Acidente envolvendo ônibus</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={this.props.sharing ? () => this.complete('flattire') : () => this.openTechDetailScreen('flattire')}>
                        <View style={styles.largeContainer}>
                            <View style={styles.largeButtonStyle}>
                                <Image style={{ width: 50, height: 50, resizeMode: 'contain' }} source={flattire} />
                                <Text style={{ ...styles.textStyle, fontSize: 16 }}>Pneu furado</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={this.props.sharing ? () => this.complete('engine') : () => this.openTechDetailScreen('engine')}>
                        <View style={styles.largeContainer}>
                            <View style={styles.largeButtonStyle}>
                                <Image style={{ width: 50, height: 50, resizeMode: 'contain' }} source={technical} />
                                <Text style={{ ...styles.textStyle, fontSize: 16 }}>Problema mecânico</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>

                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    largeContainer: {
        flex: 1,
        width: '100%',
        padding: 5
    },
    largeButtonStyle: {
        flex: 1,
        // margin: 5,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: 'white',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-evenly',

    },
    smallButtonStyle: {
        // flex: 1,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: 'white',
        height: '100%',
        width: (window_width - 20) / 2,
        alignItems: 'center',
        justifyContent: 'space-evenly',

    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(TechnicalScreen);
