import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    FlatList,
    Alert,
    ScrollView,
    ToastAndroid,


} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import BackgroundGeolocation from "react-native-background-geolocation";
import firebase from "react-native-firebase";
import { ButtonGroup, Divider, SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';

class FinishAssaultScreen extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        reportKey: null
    }

    reportAndClose = () => {
        Alert.alert(
            'Enviar denúncia?',
            ' ',
            [
                {
                    text: 'Cancelar',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Sim', onPress: () => {
                        this.firebaseRef = this.props.firebaseRef.firebaseRef
                        BackgroundGeolocation.getCurrentPosition({
                            timeout: 30,          // 30 second timeout to fetch location
                            maximumAge: 5000,     // Accept the last-known-location if not older than 5000 ms.
                            desiredAccuracy: 10,  // Try to fetch a location with an accuracy of `10` meters.
                            samples: 3,           // How many location samples to attempt.

                        }).then((position) => {
                            let content;
                            if (this.props.sharing) {
                                content = { line: this.props.selectedLineName, key: this.props.selectedLine, date: Date.now(), position: position.coords }
                            } else {
                                content = { date: Date.now(), position: position.coords }
                            }
                            let key = this.firebaseRef.database().ref().child('Dados/Assault/').push(content).key;
                            this.setState({
                                reportKey: key
                            })
                            this.firebaseRef.database().ref().child('Dados/Assault/' + key + '/').set(content);
                            ToastAndroid.show('Sua denúncia foi registrada!', ToastAndroid.SHORT);
                            Navigation.dismissAllModals();
                        }).catch((err) => {
                            console.warn(err);
                            ToastAndroid.show('Sua denúncia foi registrada!', ToastAndroid.SHORT);
                            Navigation.dismissAllModals();
                        });


                    }
                },
            ],
            { cancelable: true },
        );

    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    openExtraScreen = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.AssaultExtra1Screen',
                        passProps: {
                            linesList: this.props.linesList,
                        }

                    }
                }]
            }
        });
    }


    render() {


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '100%', width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='#EFE4CF' />
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ width: '80%' }}>
                            <Text style={styles.titleStyle}>Denunciar assédio</Text>
                        </View>

                    </View>


                </View>


                <View style={styles.middleContainer}>
                    <ScrollView contentContainerStyle={{ alignItems: 'center', justifyContent: 'space-evenly' }}>
                        <View>
                            <Text style={styles.textStyle}>{'Reiteramos que esse canal serve apenas para um levantamento estatístico relacionado ao transporte público.\n'}</Text>
                            <Text style={styles.textStyle}>{'É imprescindível que você registre uma ocorrência formal junto a Delegacia da Mulher para que os órgãos públicos possam tomar medidas e desenvolver ações para que este tipo de situação lamentável não se repita.\n'}</Text>

                        </View>

                        <TouchableWithoutFeedback onPress={this.openExtraScreen}>

                            <View style={{ backgroundColor: '#A15B37', borderRadius: 4, paddingTop: 5, paddingBottom: 5, alignItems: 'center', justifyContent: 'center', width: '80%', marginBottom: 10, borderColor: 'white', borderWidth: 1 }}>
                                <Text style={{ ...styles.textStyle, color: '#EFE4CF' }}>Quero contar mais</Text>
                            </View>

                        </TouchableWithoutFeedback>
                    </ScrollView>

                </View>

                <View style={styles.bottomContainer}>
                    <TouchableWithoutFeedback onPress={this.reportAndClose}>
                        <View style={{ elevation: 4, backgroundColor: '#EFE4CF', borderRadius: 4, borderColor: 'white', borderWidth: 1, justifyContent: 'center', width: '80%' }}>
                            <Text style={styles.textStyle}>Finalizar denúncia</Text>
                        </View>
                    </TouchableWithoutFeedback>

                </View>

            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#7B3E63'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#A15B37',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    middleContainer: {
        height: '80%',
        width: '100%',
        backgroundColor: '#EFE4CF',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    bottomContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#A15B37',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: '#A15B37',
        fontSize: 20,
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center',
        margin: 10
    },
    titleStyle: {
        color: '#EFE4CF',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '80%',
        elevation: 2

    }
});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(FinishAssaultScreen);

