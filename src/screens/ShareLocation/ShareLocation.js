import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    ScrollView,
    FlatList,
    ActivityIndicator

} from 'react-native';

import BackgroundGeolocation from "react-native-background-geolocation";
import { Navigation } from 'react-native-navigation';
import { ButtonGroup, Divider } from 'react-native-elements';
// import * as firebase from 'firebase';
import firebase from "react-native-firebase";
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';



class ShareLocation extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        lines: this.props.lines,
        selectedLine: this.props.lines[0].key,
        selectedIndex: 0,
        direction: null,
        loading: true,
        locationEnabled: false

    }

    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef
        this.getDirections(this.state.selectedLine);
        this.testLocationEnabled();

    }

    testLocationEnabled = () => {
        return new Promise((resolve, reject) => {
            RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 1000, fastInterval: 500 })
                .then(data => {
                    // The user has accepted to enable the location services
                    // data can be :
                    //  - "already-enabled" if the location services has been already enabled
                    //  - "enabled" if user has clicked on OK button in the popup
                    // clearInterval(timer);
                    // this.start();
                    this.setState({
                        locationEnabled: true
                    })
                    setTimeout(() => {
                        resolve();
                    }, 1000)


                })
                .catch(err => {
                    if (err.code == 'ERR00') {
                        setTimeout(() => { resolve() }, 1000)
                    }
                    // alert('Codigo de erro: ' + err.code);
                    // The user has not accepted to enable the location services or something went wrong during the process
                    // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
                    // codes : 
                    //  - ERR00 : The user has clicked on Cancel button in the popup
                    //  - ERR01 : If the Settings change are unavailable
                    //  - ERR02 : If the popup has failed to open
                });


        })

    }

    getDirections = (line) => {
        this.setState({
            loading: true
        })
        this.firebaseRef.database().ref().child('Linha/' + line + '/Sentido').once('value').then(snapshot => {
            console.log(snapshot.val());
            let direction = snapshot.val(); //Este objeto vem com null na posição 0, se tiver apenas uma direção
            let availableDirection = []
            direction.forEach((direction, index) => { //Este filtro remove o null
                if (direction) {
                    availableDirection.push({ direction: direction, index: index })
                }
            })
            console.log('available', availableDirection)
            if (!availableDirection[this.state.selectedIndex]) { //Testa se a posição do index selecionado ainda existe
                this.setState({
                    selectedIndex: this.state.selectedIndex == 1 ? 0 : 1
                })
            }

            if (availableDirection.length == 1) {
                this.setState({
                    oneDirection: true
                })
            } else {
                this.setState({
                    oneDirection: false
                })
            }
            this.setState({
                direction: availableDirection,//Array de direções possíveis
                loading: false
            })

        }).catch(err => console.log(err));


    }

    selectedLineHandler = (itemValue) => {
        this.setState({ selectedLine: itemValue });
        this.getDirections(itemValue);
    }

    shareLocation = () => {
        console.log('index ', this.state.selectedIndex)
        console.log(this.state.direction[this.state.selectedIndex])
        if (this.state.locationEnabled) {
            this.props.sendHandler(this.state.selectedLine, this.state.direction, this.state.direction[this.state.selectedIndex].direction, this.state.direction[this.state.selectedIndex].index);
            this.close()
        } else {
            alert('Para compartilhar, você deve ativar a sua localização!')
        }


    }


    updateIndex = (selectedIndex) => {

        this.setState({
            selectedIndex: selectedIndex,
            selectedDirection: this.state.direction[selectedIndex]
        })


    }

    showTutorial = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.ShareTutorial',

                    }
                }]
            }
        });
    }
    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    render() {

        if (this.state.loading) {
            content = (
                <ActivityIndicator size='large' color='white' />
            )
        } else {
            content = (
                <Text style={styles.textStyle}>Compartilhar</Text>
            )
        }

        if (this.state.direction) {
            buttonGroup = (
                <ButtonGroup
                    onPress={this.updateIndex}
                    selectedIndex={this.state.selectedIndex}
                    buttons={this.state.direction.length > 1 ? [this.state.direction[0].direction, this.state.direction[1].direction] : [this.state.direction[0].direction]}
                    containerStyle={styles.buttonGroup}
                    textStyle={{ fontFamily: 'Decker', textAlign: 'center' }}
                    selectedButtonStyle={{ backgroundColor: "#5E9BD4" }}

                />
            )
        } else {
            buttonGroup = (
                <ActivityIndicator size='large' color='white' />
            )
        }


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{ width: '80%' }}>
                        <Text style={styles.textStyle}>Compartilhe sua posição</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={this.showTutorial}>
                        <View style={{ position: 'absolute', right: 0, padding: 10 }}>
                            <Icon size={30} name='question-circle' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.middleContainer}>

                    <FlatList
                        style={{ width: '100%', height: '100%' }}
                        data={this.state.lines}
                        renderItem={({ item, index }) => (
                            <TouchableWithoutFeedback onPress={() => this.selectedLineHandler(item.key)}>
                                <View style={{
                                    backgroundColor: index % 2 ? '#5E9BD4' : '#0854A2',
                                    paddingTop: 20,
                                    paddingBottom: 20,
                                    alignItems: 'center',
                                    borderWidth: item.key == this.state.selectedLine ? 3 : null,
                                    borderColor: 'white'
                                }}>

                                    <Text style={styles.textStyle}>{item.line}</Text>


                                </View>
                            </TouchableWithoutFeedback>
                        )}
                    />

                </View>
                <View style={styles.bottomContainer}>
                    {buttonGroup}
                    <TouchableWithoutFeedback onPress={this.state.loading ? null : this.shareLocation}>
                        <View style={{ elevation: 2, backgroundColor: '#5E9BD4', borderRadius: 4, ...styles.buttonGroup, borderColor: 'white', borderWidth: 1, justifyContent: 'center' }}>
                            {content}
                        </View>
                    </TouchableWithoutFeedback>
                </View>

            </View>

        )



    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    middleContainer: {
        height: '60%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    bottomContainer: {
        height: '30%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '30%',
        elevation: 2

    }
});

const mapStateToProps = state => {
    return {
        user: state.user.user,
        firebaseRef: state.firebaseRef

    }
}
export default connect(mapStateToProps, null)(ShareLocation);

