import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Text,
    ScrollView,

} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';

class ShareTutorial extends Component {
    constructor(props) {
        super(props);
    }


    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    render() {
        let content = "O Buzee é uma rede colaborativa. Isso quer dizer que quando você vê um ônibus no mapa é outra pessoa compartilhando para que você saiba onde ele está. Assim, podemos ajudar uns aos outros como uma comunidade! \n";
        let content2 = "Além disso, ao compartilhar, você acumula pontos que te dão descontos nas ofertas do Polvo Ofertas! Você ajuda os outros e ainda ganha um desconto pro lanche do fim de semana!\n";
        let content3 = "Não, compartilhar a posição é como manter uma conversa normal no WhatsApp. O sistema todo foi desenvolvido para ser o mais econômico possível.\n"
        let content4 = "Diminuímos ao máximo o consumo de dados e bateria e levamos em consideração diversos padrôes de movimentação para buscar sua localização apenar quando necessário.\n"
        let content5 = "Apenas saberemos sua localização a partir do momento que você a compartilhar no aplicativo até o momento que você descer do ônibus.\n"
        let content6 = "O compartilhamento é completamente anônimo. Ninguém saberá que você está compartilhando sua localização, apenas que algum passageiro está ajudando a comunidade.\n"
        let content7 = "Se você esquecer de desligar, sem problemas. O sistema reconhece quando você desceu do ônibus e se desliga sozinho. Assim, não adiciona mais nenhuma preocupação no seu dia a dia!\n"

        return (


            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Ajuda</Text>
                </View>



                <View style={styles.middleContainer}>

                    <ScrollView>
                        <Text style={{...styles.titleStyle, fontFamily: 'Barlow-BoldItalic', marginBottom: 20}}>Por quê compartilhar minha localização?</Text>
                        <Text style={styles.textStyle}>{content}</Text>
                        <Text style={styles.textStyle}>{content2}</Text>
                        <Text style={{...styles.titleStyle, fontFamily: 'Barlow-BoldItalic', marginBottom: 20}}>Gasta muito dos meus dados?</Text>
                        <Text style={styles.textStyle}>{content3}</Text>
                        <Text style={styles.textStyle}>{content4}</Text>
                        <Text style={{...styles.titleStyle, fontFamily: 'Barlow-BoldItalic', marginBottom: 20}}>Vocês vão saber minha localização o tempo todo? É anônimo?</Text>
                        <Text style={styles.textStyle}>{content5}</Text>
                        <Text style={styles.textStyle}>{content6}</Text>
                        <Text style={{...styles.titleStyle, fontFamily: 'Barlow-BoldItalic', marginBottom: 20}}>E se eu esquecer de desligar o compartilhamento?</Text>
                        <Text style={styles.textStyle}>{content7}</Text>
                    </ScrollView>

                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >

        )




    }
}


const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});


export default ShareTutorial;
