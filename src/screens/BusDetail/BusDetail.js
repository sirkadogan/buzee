import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Text,
    ActivityIndicator,
    ToastAndroid,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';

//Esta é a tela apresentada quando o usuário toca no ícone de ônibus
//Nela é possível reclamar de ônibus cheio, ou se possui acessibilidade e ar condicionado

class BusDetail extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        selectedBus: null,
        AC: 0,
        accessible: 0,
        crowded: 0,
        loading: true,
        ACClicked: this.props.ACReported,
        crowdedClicked: this.props.crowdedReported,
        accessibleClicked: this.props.accessibleReported
    }

    componentDidMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef //Referência ao firebase
        let bus2;
        let accessible = null;
        let crowded = null;
        let AC = null

        //Pega a lista de ônibus ativos sendo renderizados na tela principal
        const entries = Object.entries(this.props.activeBuses);
        entries.forEach(bus => { //Procura entre os ônibus ativos qual foi o ônibus tocado
            let coords = bus[1];
            let key = bus[0];
            if (this.props.selectedBusId == key) {
                bus2 = coords
            }
        })

        //Pega a linha e direção do ônibus tocado
        let linha = bus2.path.split('/')[3];
        let direcao = bus2.path.split('/')[4];

        linha = this.props.linesList.find(item => { //Procura nome completo da linha
            return item.key == linha
        })

        //Seta o estado com as informações obtidas
        this.setState({
            selectedBus: bus2,
            linha: linha.line,
            direcao: direcao,
            lineKey: linha.key
        })
        //Busca as informações se o ônibus tocado está cheio, com AC ligado ou possui acessiblidade e seta
        //O estado
        this.firebaseRef.database().ref().child(bus2.path + '/accessible/').once('value').then((snap) => {
            accessible = snap.val()
        }).then(() => {
            this.firebaseRef.database().ref().child(bus2.path + '/crowded/').once('value').then((snap) => {
                crowded = snap.val()
            }).then(() => {
                this.firebaseRef.database().ref().child(bus2.path + '/AC/').once('value').then((snap) => {
                    AC = snap.val()
                }).then(() => {

                    this.setState({
                        accessible: accessible,
                        crowded: crowded,
                        AC: AC,
                        loading: false
                    })
                })
            })
        })
    }

    //Fecha janela atual
    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    //Adiciona pontos na gamificação do app por informar alguma das 
    //3 informações da tela (cheio, AC ou accessível)
    addPoints = () => {
        let uid = this.props.uid;
        return this.firebaseRef.database()
            .ref()
            .child('users/' + uid + '/odometer')
            .transaction((current_value) => {
                if (current_value) {
                    return (current_value || 0) + 2500;
                } else {
                    return 2500;
                }
            });
    }

    //Soma quantas vezes usuário fez aquele report para deixar
    //no perfil como gamificação
    addCounterOnProfile = (value) => {
        let uid = this.props.uid
        this.firebaseRef.database()
            .ref()
            .child('users/' + uid + '/game/' + value + '/')
            .transaction((current_value) => {
                if (current_value) {
                    return (current_value || 0) + 1;
                } else {
                    return 1;
                }
            });
    }

    //Função auxiliar para salvar os reports na instância de ônibus que está sendo
    //processada. Adiciona o report do usuário na quantidade de reports já existente
    runTransaction = (path) => {
        return this.firebaseRef.database()
            .ref()
            .child(this.state.selectedBus.path + '/' + path + '/')
            .transaction((current_value) => {
                if (current_value) {
                    return (current_value || 0) + 1;
                } else {
                    return 1;
                }

            });
    }

    //Report de ar condicionado ligado. Alerta explicativo sobre o funcionamento e para confirmação
    reportAC = () => {
        if (!this.state.ACClicked) {

            Alert.alert(
                'Ar condicionado ligado?',
                'Isto irá avisar a comunidade que este ônibus está com o ar ligado',
                [
                    {
                        text: 'Não',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sim', onPress: () => {

                            ToastAndroid.show('Obrigado por ajudar a comunidade! \n + 2500 pontos', ToastAndroid.SHORT);
                            this.runTransaction('AC')
                            this.setState({
                                AC: this.state.AC + 1,
                                ACClicked: true
                            })
                            this.props.reportAC();
                            this.addCounterOnProfile('AC');
                            this.addPoints();
                            return this.firebaseRef.database().ref().child('Dados/AC/' + this.state.lineKey + '/').push(Date.now());
                        }
                    },
                ],
                { cancelable: true },
            );

        }

    }

    //Report de ônibus muito cheio. Alerta explicativo sobre o funcionamento e para confirmação
    reportCrowded = () => {

        if (!this.state.crowdedClicked) {


            Alert.alert(
                'Muito cheio?',
                'Isto irá avisar a comunidade que este ônibus está muito cheio',
                [
                    {
                        text: 'Não',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sim', onPress: () => {
                            this.addPoints();
                            ToastAndroid.show('Obrigado por ajudar a comunidade! \n + 2500 pontos', ToastAndroid.SHORT);
                            this.runTransaction('crowded')
                            this.setState({
                                crowded: this.state.crowded + 1,
                                crowdedClicked: true
                            })
                            this.addCounterOnProfile('crowded');
                            this.props.reportCrowded();
                            return this.firebaseRef.database().ref().child('Dados/crowded/' + this.state.lineKey + '/').push(Date.now());
                        }
                    },
                ],
                { cancelable: true },
            );


        }
    }
    //Report de acessibildade para cadeirantes. Alerta explicativo sobre o funcionamento e para confirmação
    reportAccessible = () => {
        if (!this.state.accessibleClicked) {

            Alert.alert(
                'Possui elevador?',
                'Isto irá avisar a comunidade que este ônibus é acessível',
                [
                    {
                        text: 'Não',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sim', onPress: () => {
                            this.addPoints();
                            ToastAndroid.show('Obrigado por ajudar a comunidade! \n + 2500 pontos', ToastAndroid.SHORT);
                            this.runTransaction('accessible')
                            this.setState({
                                accessible: this.state.accessible + 1,
                                accessibleClicked: true
                            })
                            this.addCounterOnProfile('acessible');
                            this.props.reportAccessible();
                            return this.firebaseRef.database().ref().child('Dados/accessible/' + this.state.lineKey + '/').push(Date.now());
                        }
                    },
                ],
                { cancelable: true },
            );

        }


    }

    render() {
        let userNum = 0;
        const text1 = 'Este ônibus está sendo compartilhado pela comunidade.'
        userNum = this.state.selectedBus ? this.state.selectedBus.uid.length : 0;

        // ESTE IF DEFINE OS BOTÕES DA BARRA INFERIOR
        if (this.state.loading) { //CARREGANDO
            busDetails = (
                <View style={{ width: '100%', height: (window_width - 30) / 3, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' color='white' />
                </View>
            )
        } else {

            busDetails = (
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    {/* BOTÃO DE MUITO CHEIO */}
                    <TouchableWithoutFeedback onPress={this.state.crowdedClicked ? null : this.reportCrowded}>
                        <View style={{ ...styles.buttonStyle, backgroundColor: this.state.crowded ? '#114484' : '#5E9BD4' }}>
                            <View style={{ position: 'absolute', right: 5, top: 5 }}>
                                <Text style={{ ...styles.textStyle, fontSize: 14 }}>{this.state.crowded}</Text>
                            </View>
                            <Icon size={40} name='users' color='white' />
                            <Text style={{ ...styles.textStyle, fontSize: 16 }}>Muito cheio</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    {/* BOTÃO DE ACESSÍVEL */}
                    <TouchableWithoutFeedback onPress={this.state.accessibleClicked ? null : this.reportAccessible}>
                        <View style={{ ...styles.buttonStyle, backgroundColor: this.state.accessible ? '#114484' : '#5E9BD4' }}>
                            <View style={{ position: 'absolute', right: 5, top: 5 }}>
                                <Text style={{ ...styles.textStyle, fontSize: 14 }}>{this.state.accessible}</Text>
                            </View>
                            <Icon size={40} name='wheelchair' color='white' />
                            <Text style={{ ...styles.textStyle, fontSize: 16 }}> Possui Acessibilidade</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    {/* BOTÃO DE AR CONDICIONADO */}
                    <TouchableWithoutFeedback onPress={this.state.ACClicked ? null : this.reportAC}>
                        <View style={{ ...styles.buttonStyle, backgroundColor: this.state.AC ? '#114484' : '#5E9BD4' }}>
                            <View style={{ position: 'absolute', right: 5, top: 5 }}>
                                <Text style={{ ...styles.textStyle, fontSize: 14 }}>{this.state.AC}</Text>
                            </View>
                            <Icon size={40} name='asterisk' color='white' />
                            <Text style={{ ...styles.textStyle, fontSize: 16 }}>{'AC ligado'}</Text>

                        </View>
                    </TouchableWithoutFeedback>

                </View>
            )

        }


        return (
            <View style={styles.screen}>
                {/* CABEÇALHO */}
                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                {/* INFORMAÇÕES DO ONIBUS NO CORPO CENTRAL */}
                <View style={styles.middleContainer}>
                    <View style={{ height: '60%', width: '100%', justifyContent: 'space-evenly', alignItems: 'center' }}>
                        <View style={{ width: '80%' }}>
                            <Text style={styles.textStyle}>{this.state.linha}</Text>
                            <Text style={{ ...styles.textStyle, fontSize: 18 }}>{this.state.direcao}</Text>
                        </View>
                        <Text style={{ ...styles.textStyle, fontSize: 18, width: '90%' }}>{text1}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.textStyle}>Usuários neste ônibus: </Text>
                            <Text style={styles.textStyle}>{userNum}</Text>
                        </View>
                    </View>

                    {/* INFORMAÇÕES SOBRE A CONDIÇÃO DO ÔNIBUS, BARRA INFERIOR */}
                    <View style={{ height: '40%', width: '100%', justifyContent: 'space-evenly', alignItems: 'center' }}>
                        <View>
                            <Text style={styles.textStyle}>Condições do ônibus</Text>
                            <Text style={{ ...styles.textStyle, fontSize: 18 }}>Toque para avisar a comunidade</Text>
                        </View>

                        <View style={{ width: '100%' }}>
                            {/* BOTOES DEFINIDOS NO IF INICIAL*/}
                            {busDetails}
                        </View>
                    </View>


                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonStyle: {
        margin: 5,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: 'white',
        height: (window_width - 30) / 3,
        width: (window_width - 30) / 3,
        alignItems: 'center',
        justifyContent: 'space-evenly',

    }
});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(BusDetail);
