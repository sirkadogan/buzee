import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Text,
    TouchableWithoutFeedback,
    ActivityIndicator,
    AsyncStorage,
    ToastAndroid,

} from 'react-native';

import { ButtonGroup } from 'react-native-elements';
import { Navigation } from 'react-native-navigation';
import BusTime from '../../components/BusTime/BusTime';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
//ESTA CLASSE DEFINE A TELA DE HORÁRIOS QUE APARECE AO TOCAR 
//UMA PARADA DE ÔNIBUS
class BusTimeModal extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        arrivalTimes: [],
        terminalTime: [],
        totalTime: [],
        terminal: null,
        selectedIndex: 2,
        exists: null,
        totalTime: [],
        directionIndex: this.props.directionIndex,
        filteredLines: this.props.lines, //LINHAS QUE PASSAM POR ESTA PARADA
        loading: true,
        sortedTimes: [],
        availableLines: this.props.linesList,
        timeIndex: 0,
        favorite: false,
        favoriteStops: this.props.favoriteStops
    }

    //Seta o dia de acordo com a tabela:
    //Sábado = 6
    //Dia da semana = 1
    //Domingo e feriado = 0
    setDay = (date) => {
        this.setState({
            day: date
        })
    }

    updateIndex = (selectedIndex, day) => {
        this.setState({
            selectedIndex: selectedIndex,
        })
        if (day) { //Se função receber um dia
            if (day == 6) {
                //Data é sábado 
                this.setDay(6);
                this.getTimes(this.state.directionIndex, 6)
            } else if (day == 0) {
                //Data é domingo
                this.setDay(0);
                this.getTimes(this.state.directionIndex, 0)
            } else {
                //Data é qualquer outro dia da semana
                this.setDay(1);
                this.getTimes(this.state.directionIndex, 1)
            }
        } else { //Se usuário seleciona uma nova aba de dias
            if (selectedIndex == 0) {
                this.setDay(1);
                this.getTimes(this.state.directionIndex, 1)
            } else if (selectedIndex == 1) {
                this.setDay(6);
                this.getTimes(this.state.directionIndex, 6)
            } else if (selectedIndex == 2) {
                this.setDay(0);
                this.getTimes(this.state.directionIndex, 0)
            }
        }
    }

    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef; //referência ao firebase
        let date = new Date();
        let day = date.getDay();
        let hour = date.getHours();

        //Verifica se parada foi favoritada e seta o estado local
        if (this.props.favoriteStops.includes(this.props.id)) {
            this.setState({
                favorite: true
            })
        }

        this.setState({
            hour: hour
        })

        //Muda a posição do toggler de dia de acordo com o dia selecionado
        if (day == 6) {
            //Data é sábado 
            this.updateIndex(1, 6) //Parâmetros são (posição no index, dia da semana)
        } else if (day == 0) {
            //Data é domingo
            this.updateIndex(2, 0)
        } else {
            //Data é qualquer outro dia da semana
            this.updateIndex(0, 1)
        }


    }

    //Busca os horários para a parada selecionada de acordo com a direção e data da tabela (0,1,6)
    getTimes = (directionIndex, date) => {

        this.setState({
            arrivalTimes: [],
            sortedTimes: [],
            loading: true
        }, () => {
            let lineCounter = 0;
            this.state.filteredLines.forEach(line => { //Para cada linha selecionada pelo usuário
                return new Promise((resolve) => {
                    //Busca nome da direção(bairro->Centro, centro->Bairro) 
                    //baseado na posição de direção selecionada (0,1)
                    this.firebaseRef.database()
                        .ref()
                        .child('Linha/' + line + '/Sentido/' + directionIndex)
                        .once('value')
                        .then(snapshot => {
                            dir = snapshot.val();
                            this.setState({
                                directionIndex: directionIndex
                            })
                            return dir //Retorna nome da direção por completo
                        })
                        .then((dir) => {
                            //Pega horários de partida dos terminais de cada linha selecionada
                            this.firebaseRef.database()
                                .ref()
                                .child('Linha/' + line + '/' + dir + '/H/' + date + '/')
                                .orderByKey()
                                .once('value')
                                .then(snapshot => {
                                    if (snapshot.exists()) {
                                        let terminalTime = [];
                                        let markerId = this.props.id; //parada selecionada
                                        let lineStopList = this.props.lineStopList;

                                        //Pega o index da parada selecionada baseado no markerID para 
                                        //saber qual posição a parada ocupa na linha sendo processada
                                        let selectedStopInItsLine = lineStopList[line].indexOf(parseInt(markerId));

                                        if (selectedStopInItsLine >= 0) { //Se encontrou a parada nesta linha
                                            let date = new Date();
                                            let currentDay = date.getDay();
                                            let totalTime = [];
                                            totalTime.fill(1); //cria array de 1s

                                            //Pega os horários do terminal
                                            snapshot.forEach(childSnapshot => {
                                                let childKey = childSnapshot.key;
                                                let childData = childSnapshot.val();

                                                terminalTime.push({ key: childKey, description: childData });
                                            });

                                            //Busca tempo entre paradas e calcula o tempo até chegar na parada selecionada
                                            //Isso se faz fazendo o somatório de todo o tempo entre trechos até chegar 
                                            //à parada selecionada
                                            this.firebaseRef.database()
                                                .ref()
                                                .child('Time/' + line + '/' + dir + '/' + currentDay)
                                                .once('value')
                                                .then(snapshot => {
                                                    snapshot.forEach(childSnapshot => {
                                                        let childKey = parseInt(childSnapshot.key); //Key do trecho
                                                        let childData = childSnapshot.val(); //Tempo do trecho por hora do dia

                                                        if (selectedStopInItsLine == 0) { //Parada selecionada é o terminal
                                                            //Não necessita calcular tempo entre trechos, finaliza 
                                                            //O calculo de tempo estimado ali
                                                            let calculatedHours = [];
                                                            terminalTime.map(time => {
                                                                let hours = parseInt(time.key.slice(0, 2));
                                                                if (!totalTime[hours]) {
                                                                    totalTime[hours] = 0;
                                                                }
                                                                if (!calculatedHours.includes(hours)) {
                                                                    totalTime[hours] = 0;
                                                                    calculatedHours.push(hours);
                                                                }
                                                            })
                                                            totalTime = totalTime;

                                                            //Se não é a parada inicial e o id do trecho é menor que o id da parada
                                                        } else if (childKey <= selectedStopInItsLine) {
                                                            let calculatedHours = [];
                                                            //childData é o tempo estimado de deslocamento para cada hora do dia para cada trecho
                                                            terminalTime.map(time => {
                                                                let hours = parseInt(time.key.slice(0, 2)); //Pega a hora de saída do terminal
                                                                let path = childData[hours]; //Busca o tempo estimado de viagem para aquele horário
                                                                if (!totalTime[hours]) {
                                                                    totalTime[hours] = 0;
                                                                }
                                                                //Testa se o tempo estimado das 16h (exemplo) já está presente no array para não duplicar
                                                                if (!calculatedHours.includes(hours)) {
                                                                    totalTime[hours] = totalTime[hours] + parseInt(path.Dwell) + parseInt(path.Running);
                                                                    calculatedHours.push(hours);

                                                                }


                                                            })
                                                            totalTime = totalTime//Tempo total entre paradas acumulado até a parada selecionada
                                                        }


                                                    });
                                                    arrivalTimes = this.state.arrivalTimes;
                                                    let timeString;
                                                    let date = new Date();

                                                    //pega o nome completo da linha a partir da tag
                                                    this.props.linesList.forEach(item => {
                                                        if (item.key == line) {
                                                            lineName = item.line
                                                            return;
                                                        }
                                                    });

                                                    //Faz a soma dos horários do terminal com o tempo total entre paradas até a parada selecionada
                                                    terminalTime.map(time => {
                                                        let hours = parseInt(time.key.slice(0, 2));
                                                        let minutes = time.key.slice(3, 5);
                                                        date.setHours(hours);
                                                        date.setMinutes(minutes);
                                                        timeString = (new Date(date.valueOf() + (totalTime[hours] * 1000))).toTimeString().slice(0, 5)

                                                        arrivalTimes.push({ time: timeString, description: { line: lineName, direction: dir } });
                                                    })
                                                    this.setState({
                                                        arrivalTimes: arrivalTimes
                                                    }, () => {

                                                        times = arrivalTimes
                                                        //Organiza o array por horario
                                                        times.sort(function (a, b) {
                                                            a = a.time.replace(':', '')
                                                            b = b.time.replace(':', '')
                                                            return parseInt(a) - parseInt(b)
                                                        })
                                                        this.setState({
                                                            sortedTimes: times,
                                                        }, () => {
                                                            lineCounter++;
                                                            if (this.props.lines.length == lineCounter) {
                                                                this.setState({
                                                                    exists: true,
                                                                    loading: false
                                                                })
                                                            }
                                                            resolve()
                                                        })

                                                    })
                                                })
                                        } else {
                                            lineCounter++;
                                            if (this.props.lines.length == lineCounter) {
                                                this.setState({
                                                    exists: true,
                                                    loading: false
                                                })
                                            }
                                            resolve()
                                        }

                                    } else { //Não existem horários para este dia
                                        lineCounter++;
                                        if (this.props.lines.length == lineCounter) {
                                            this.setState({
                                                exists: true,
                                                loading: false
                                            })
                                        }
                                        resolve()
                                    }
                                })
                        })

                })

            })
        })
    }

    //Fecha a tela
    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    //Favorita a parada selecionada
    favoriteStop = () => {
        let favoriteStops = this.state.favoriteStops;
        favoriteStops.push(this.props.id)
        ToastAndroid.show('Parada favoritada!', ToastAndroid.SHORT);
        this.setState({
            favorite: true
        })
        AsyncStorage.setItem('buzee:favoriteStops', JSON.stringify(favoriteStops)); //Salva na memória
        //Força re renderização dos marcaroders para atualizar o ícone da parada favoritada
        this.props.updateFavoriteMarkers(favoriteStops)

    }
    //Remove parada dos favoritos
    unfavoriteStop = () => {
        let favoriteStops = this.state.favoriteStops;
        favoriteStops = favoriteStops.filter(id => this.props.id != id)
        ToastAndroid.show('Parada removida dos favoritos!', ToastAndroid.SHORT);
        this.setState({
            favorite: false
        })
        AsyncStorage.setItem('buzee:favoriteStops', JSON.stringify(favoriteStops));
        this.props.updateFavoriteMarkers(favoriteStops)
    }


    render() {
        let timesList = null

        if (this.state.loading) { //carregando
            timesList = (
                <ActivityIndicator size='large' color='white' />
            )
        } else {
            //SE CARREGOU ALGUM HORÁRIO
            if (this.state.sortedTimes.length > 0) { 

                timesList = (

                    <FlatList
                        data={this.state.sortedTimes}
                        maxToRenderPerBatch={1000}
                        windowSize={60}
                        updateCellsBatchingPeriod={50}
                        initialNumToRender={50}
                        renderItem={({ item }) => (

                            <View style={{
                                backgroundColor: '#5E9BD4',
                                alignItems: 'center',
                                borderBottomWidth: 0.5,
                                borderColor: 'white'

                            }}>
                                <BusTime time={item.time} line={item.description.line} direction={item.description.direction} />
                            </View>


                        )}
                    />


                )

            } else { //SE NÃO TIVER RETORNADO NENHUM RESULTADO
                timesList = (
                    <Text style={styles.textStyle}>Não há horários disponíveis para este dia.</Text>
                )
            }

        }

        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '50%', width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='white' />
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{ width: '80%' }}>
                            <Text style={styles.textStyle}>Horários desta parada</Text>
                        </View>


                        <TouchableWithoutFeedback onPress={this.state.favorite ? this.unfavoriteStop : this.favoriteStop}>
                            <View style={{ position: 'absolute', right: 0, padding: 10 }}>
                                <Icon size={30} name='star' color={this.state.favorite ? '#FED162' : 'white'} />
                            </View>
                        </TouchableWithoutFeedback>

                    </View>
                    <View style={{ height: '50%', width: '100%', alignItems: 'center' }}>
                        <ButtonGroup
                            onPress={this.updateIndex}
                            selectedIndex={this.state.selectedIndex}
                            buttons={['Dias úteis', 'Sábado', 'Domingos e feriados']}
                            containerStyle={styles.buttonGroup}
                            textStyle={{ fontFamily: 'Decker', textAlign: 'center' }}
                            selectedButtonStyle={{ backgroundColor: "#5E9BD4" }}
                        />
                    </View>
                </View>
                <View style={styles.middleContainer}>

                    {timesList}
                </View>

                <View style={styles.bottomContainer}>
                </View>

            </View >

        )
    }

}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    topContainer: {
        height: '20%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center',

    },
    middleContainer: {
        height: '75%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '80%',
        elevation: 2

    }
});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(BusTimeModal);
