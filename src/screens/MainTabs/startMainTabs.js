import { Navigation } from 'react-native-navigation';
import { AsyncStorage } from 'react-native';

const startTabs = () => {
    console.log('tentou start tabs')
    AsyncStorage.getItem('buzee:showTutorial').then((value) => {
        if (value !== null) {
            if (value === 'true') {
                console.log('Tutorial desabilitado');
                console.log("Vai abrir tela principal")
                Navigation.setRoot({
                    root:{
                      stack:{
                        children:[{
                          component:{
                            name:'buzee.MainScreen'
                          }
                        }]
                      }
                    },
                    layout: {
                      orientation: ['portrait']
                    }
                }); 
                // Navigation.startSingleScreenApp({
                //     screen: {
                //         screen: 'buzee.MainScreen', // unique ID registered with Navigation.registerScreen
                //         animationType: 'fade' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                //     },


                // });
            } 
        } else {
            console.log('Nada na memoria, mostrando tutorial')
            Navigation.setRoot({
                root:{
                  stack:{
                    children:[{
                      component:{
                        name:'buzee.TutorialScreen'
                      }
                    }]
                  }
                },
                layout: {
                  orientation: ['portrait']
                }
            }); 
            // Navigation.startSingleScreenApp({
            //     screen: {
            //         screen: 'buzee.TutorialScreen', // unique ID registered with Navigation.registerScreen
            //         animationType: 'fade' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
            //     },
            // });
            
        }
    })


};


export default startTabs;