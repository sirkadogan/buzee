import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableWithoutFeedback,
    StyleSheet,
    Dimensions,
    PermissionsAndroid,
    Image,
    AsyncStorage,
    ToastAndroid,
    ActivityIndicator,
    Linking
} from 'react-native';
import MapView from 'react-native-maps';
import BackgroundGeolocation from "react-native-background-geolocation";
// import * as firebase from 'firebase';
import { Navigation } from 'react-native-navigation';
import Pin from '../../assets/Pinn.png';
import favPin from '../../assets/favPinn.png';
import Buz from '../../assets/Buzz.png';
import On from '../../assets/On.png';
import Off from '../../assets/Off.png';
import Icon from 'react-native-vector-icons/FontAwesome';

import SideMenu from 'react-native-side-menu';
import RatingRequestor from 'react-native-rating-requestor';

// import mapStyle from '../../assets/mapStyle';

import InverterButton from '../../assets/Inverter.png';

import firebase from "react-native-firebase";
import Menu from '../SideMenu/SideMenu';

//Connecting to redux
import { connect } from 'react-redux';
import { updateLevel, storeStops, uiStopLoading } from '../../store/actions/index'
import { UI_START_LOADING } from '../../store/actions/actionTypes';

const channel = new firebase.notifications.Android.Channel('notification-channel', 'Notification Channel', firebase.notifications.Android.Importance.Max)
    .setDescription('General notification channel');

// Create the channel
firebase.notifications().android.createChannel(channel);

let RatingTracker = new RatingRequestor('com.buzee', {
    title: 'Gostando do Buzee?',
    message: 'Que tal deixar uma resenha na loja agora?',
    actionLabels: {
        decline: 'Não',
        delay: 'Mais tarde',
        accept: 'Sim'
    },
    storeAppName: 'com.buzee',
    storeCountry: 'br',
});


class MainScreen extends Component {
    constructor(props) {
        super(props);
        console.ignoredYellowBox = ['Warning: Each', 'Setting'];
        // console.disableYellowBox = true;
    }

    //removes the navigation bar at the top
    static navigatorStyle = {
        navBarHidden: true
    }

    state = {
        // focusedLocation: {
        //     latitude: -29.7161,
        //     longitude: -53.7157,
        //     latitudeDelta: 0.0122,
        //     longitudeDelta: Dimensions.get('window').width / Dimensions.get('window').height * 0.0122
        // },
        focusedLocation: {
            latitude: -29.7161,
            longitude: -53.7157,
            latitudeDelta: 0.0122,
            longitudeDelta: 0.0122
        },
        selectedMarker: null,
        activeBuses: [],
        locationPermission: false,
        sharing: false,
        clicked: false,
        entryTimeStamp: 0,
        runningTime: 0,
        timesArray: [],
        passedStops: [],
        lastExitTime: 0,
        stopMarkers: [],
        busMarkers: [],
        menuButton: true,
        // direction: ['Bairro->Centro', 'Centro->Bairro'],
        selectedLine: 'UniFxV',
        selectedDirection: 'Bairro ► Centro',
        // selectedLineName: ['Universidade Fx. Velha'],
        // direction: ['Bairro ► Centro', 'Centro ► Bairro'],
        filteredLines: [],
        linesList: [],
        oneDirection: false,
        stopIndexes: [],
        lineStopList: {},
        directionIndex: 0,
        selectedLineIndex: 0,
        favoriteStops: [],
        loading: false,
        crowdedReported: false,
        ACReported: false,
        accessibleReported: false,
        dirtyReported: false,
        crazyReported: false,
        ratingReported: false,
        rating: 0,
        viewLines: [],
        width: 355,
        isOpen: false,
        configs: {},
        location: null
    }


    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef

        ////
        // 1.  Wire up event-listeners
        // //
        // console.log(this.props.firebaseRef)

        // 2.  Execute #ready method (required)

        BackgroundGeolocation.ready({
            // triggerActivities: 'in_vehicle',
            // reset: true,
            // enableHeadless: true,
            // Geolocation Config

            desiredAccuracy: 10,
            distanceFilter: 10,
            disableElasticty: true,
            // notificationLargeIcon: 'mipmap/ic_notification',
            notificationSmallIcon: 'drawable/src_assets_white_logo',
            notificationText: 'Localização ativada! Você está acumulando pontos :)',
            notificationColor: '#095796',
            // Activity Recognition
            stopTimeout: 1,
            // Application config
            // debug: true, // <-- enable this hear sounds for background-geolocation life-cycle.
            logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
            stopOnTerminate: true,   // <-- Allow the background-service to continue tracking when user closes the app.
            startOnBoot: false,        // <-- Auto start tracking when device is powered-up.
            geofenceInitialTriggerEntry: true,
            // HTTP / SQLite config
            url: this.firebaseRef.options.databaseURL + 'location.json',
            batchSync: false,       // <-- [Default: false] Set true to sync locations to server in a single HTTP request.
            autoSync: false,         // <-- [Default: true] Set true to sync each location to server as it arrives.
            foregroundService: true,
            // headers: {              // <-- Optional HTTP headers
            //     "X-FOO": "bar"
            // },
            // params: {               // <-- Optional HTTP params
            //     //"auth_token": "maybe_your_server_authenticates_via_token_YES?"
            //     'L': 'UniFxV'
            // },
            maxRecordsToPersist: 1,
            //CONFERIR SE ESSES DADOS SÃO SUFICIENTES
            locationTemplate: '{"la":<%= latitude %>,"lo":<%= longitude %>}',
            geofenceTemplate: '{"la":<%= latitude %>,"lo":<%= longitude %>,"event":<%= geofence.action %>, "id": <%= geofence.identifier %> }'

        }, (state) => {
            BackgroundGeolocation.onEnabledChange(isEnabled => {
                this.onEnableGeofence(isEnabled);
            });

            console.log("- BackgroundGeolocation is configured and ready: ", state.enabled);

            this.setState({
                sharing: state.enabled,
            })
        });

        AsyncStorage.getItem('buzee:isRunning').then((value) => {
            if (value !== null) {
                if (value == 'true') {
                    this.stopSharingLocation();
                }
            }
        })

        AsyncStorage.getItem('buzee:favoriteStops').then((value) => {
            if (value !== null) {
                this.setState({
                    favoriteStops: JSON.parse(value)
                })
            }
        })

        if (this.swapTimer) {
            clearInterval(this.swapTimer)
        }

        this.getConfigs();
        this.getAvailableLines().then(() => {
            this.getFilteredLines().then(() => {
                this.getStops();
            })
        })
    }

    getPosition = () => {
        // console.log('Buscou posição ao abrir o mapa')
        this.checkLocationAccess().then((hasLocationAccess) => {
            // console.log('checklocation', hasLocationAccess);
            if (hasLocationAccess) {
                BackgroundGeolocation.getCurrentPosition({
                    timeout: 30,          // 30 second timeout to fetch location
                    maximumAge: 86400000, // Accept the last-known-location if not older than 5000 ms.
                    desiredAccuracy: 50,  // Try to fetch a location with an accuracy of `10` meters.
                    samples: 1,           // How many location samples to attempt.
                }).then(location => {

                    // if (this.map) {
                    this.firebaseRef.database().ref().child('Dados/openLocation').push({ lat: location.coords.latitude, lng: location.coords.longitude, acc: location.coords.accuracy, timestamp: Date.now() });
                    this.onLocation(location);
                    // }
                })
            }
        });



    }



    getFilteredLines = () => { //Pega quais são as linhas selecionadas da memória interna
        return new Promise((resolve) => {
            AsyncStorage.getItem('buzee:filteredLines').then((lines) => {
                if (lines !== null && lines.length >= 1) {
                    parseLines = JSON.parse(lines);
                    this.setState({
                        filteredLines: parseLines
                    }, () => {
                        console.log('filtered lines da memoria:', this.state.filteredLines)
                        resolve();
                    })
                } else {
                    resolve()
                }//Se não tiver nenhuma linha, usa as padrões no estado
            })
        })
    }

    updateFilteredLines = (filteredLines) => {
        return new Promise((resolve) => {
            this.setState({
                filteredLines: filteredLines
            })
            AsyncStorage.setItem('buzee:filteredLines', JSON.stringify(filteredLines)).then(() => {
                resolve()
            });
        })
    }

    getConfigs = () => {
        this.firebaseRef.database().ref().child('Config').once('value').then(snapshot => {
            configs = snapshot.val();
            this.setState({
                configs: configs
            })

        })
    }

    getDirections = (line, directionIndex) => {
        this.firebaseRef.database().ref().child('Linha/' + line + '/Sentido/' + directionIndex).once('value').then(snapshot => {
            dir = snapshot.val();
            this.setState({
                selectedDirection: dir
            })

        })
    }

    findFilteredLineNames = () => {
        let data = this.state.linesList; //Lista completa de linhas disponiveis
        let filteredNames = [];
        let dir;
        this.state.linesList.forEach(item => {
            this.state.viewLines.forEach(e => { //Procura direção equivalente a linha sendo procurada
                if (e.key == item.key) {
                    dir = e.direction;
                    filteredNames.push({ line: item.line, dir: dir })
                    return
                }
            })
        })
        this.setState({
            filteredLineNames: filteredNames,
            selectedLineIndex: 0
        }, () => {
            filteredLines = this.state.filteredLines;
            viewLines = this.state.viewLines;
            names = this.state.filteredLineNames;
            if (this.state.filteredLineNames) {
                this.swapSelectedLine()
            }

        })
    }

    swapSelectedLine = () => {

        this.swapTimer = setInterval(() => {
            if (this.state.selectedLineIndex == this.state.filteredLineNames.length - 1) {
                this.setState({
                    viewSelectedLineName: this.state.filteredLineNames[this.state.selectedLineIndex].line,
                    viewSelectedDirection: this.state.filteredLineNames[this.state.selectedLineIndex].dir,
                    selectedLineIndex: 0
                })
            } else {
                this.setState({
                    viewSelectedLineName: this.state.filteredLineNames[this.state.selectedLineIndex].line,
                    viewSelectedDirection: this.state.filteredLineNames[this.state.selectedLineIndex].dir,
                    selectedLineIndex: this.state.selectedLineIndex + 1
                })
            }
        }, 3000)
    }

    getStops = () => {
        return new Promise((resolve, reject) => {
            if (this.swapTimer) {
                clearInterval(this.swapTimer)
            }
            this.setState({
                stopMarkers: [],
                stopIndexes: [],
                loading: true,
                viewLines: [],
                filteredLineNames: [],
                viewSelectedDirection: null,
                viewSelectedLineName: null,
            }, () => {

                this.getStopIndexes().then(() => {


                    let stopMarkers = [];
                    this.state.stopIndexes.forEach(index => {
                        this.firebaseRef.database().ref().child('Paradas/P/' + index).once('value').then(snapshot => {
                            let data = snapshot.val();
                            stopMarkers.push({ identifier: index, latitude: parseFloat(data.la), longitude: parseFloat(data.lo), notifyOnEntry: true, notifyOnExit: true, notifyOnDwell: false });
                        }).then(() => {
                            if (stopMarkers.length == this.state.stopIndexes.length) {
                                this.setState({
                                    stopMarkers: stopMarkers,
                                    loading: false
                                }, () => {
                                    this.findFilteredLineNames();
                                    // console.log(stopMarkers)
                                    resolve()
                                })
                            }
                        })
                    })
                })
            })
        })

    }

    getStopIndexes = () => {
        return new Promise((resolve, reject) => {
            if (this.itemsRef) {
                this.itemsRef.off() //Limpa as referencias anteriores
            }
            let counter = 0;
            this.state.filteredLines.forEach(line => {
                return new Promise((resolve) => {
                    this.firebaseRef.database().ref().child('Linha/' + line + '/Sentido/' + this.state.directionIndex).once('value').then(snapshot => {
                        dir = snapshot.val();
                        let viewLine = this.state.viewLines;
                        if (dir) {
                            console.log('ENTRA AQUI SE TIVER A DIREÇÃO CORRETA')
                            viewLine.push({ key: line, direction: dir });
                            this.setState({
                                viewLines: viewLine,
                                // selectedDirection: dir
                            })
                            this.itemsRef = this.firebaseRef.database().ref().child('Linhas/' + line + '/' + dir); //Lê as posições dos ônibus no FB
                            this.listenForItems(this.itemsRef); //Escuta por mudanças nas posições dos ônibus
                            resolve(dir)
                        } else if (this.state.filteredLines.length == 1) {
                            if (this.state.directionIndex == 0) {
                                console.log('ENTRA AQUI SE TIVER A DIREÇÃO ERRADA MAS TIVER SÓ UMA LINHA')
                                this.firebaseRef.database().ref().child('Linha/' + line + '/Sentido/1/').once('value').then(snapshot => {
                                    dir = snapshot.val();
                                    viewLine.push({ key: line, direction: dir });
                                    // console.log(dir)
                                    if (dir) {
                                        this.setState({
                                            viewLines: viewLine,
                                            directionIndex: 1
                                            // selectedDirection: dir
                                        })
                                        this.itemsRef = this.firebaseRef.database().ref().child('Linhas/' + line + '/' + dir); //Lê as posições dos ônibus no FB
                                        this.listenForItems(this.itemsRef); //Escuta por mudanças nas posições dos ônibus
                                        resolve(dir)
                                    }
                                })
                            } else {
                                console.log('ENTRA AQUI SE TIVER A DIREÇÃO ERRADA MAS TIVER SÓ UMA LINHA')
                                this.firebaseRef.database().ref().child('Linha/' + line + '/Sentido/0/').once('value').then(snapshot => {
                                    dir = snapshot.val();
                                    viewLine.push({ key: line, direction: dir });
                                    // console.log(dir)
                                    if (dir) {
                                        this.setState({
                                            viewLines: viewLine,
                                            // selectedDirection: dir,
                                            directionIndex: 0
                                        })
                                        this.itemsRef = this.firebaseRef.database().ref().child('Linhas/' + line + '/' + dir); //Lê as posições dos ônibus no FB
                                        this.listenForItems(this.itemsRef); //Escuta por mudanças nas posições dos ônibus
                                        resolve(dir)
                                    }
                                })
                            }

                        } else {
                            resolve()
                        }
                    })


                }).then((dir) => {
                    this.firebaseRef.database().ref().child('Linha/' + line + '/' + dir + '/P/').orderByKey().once('value').then(snapshot => {
                        if (snapshot.exists()) {
                            let prevIndexes = this.state.stopIndexes;
                            let newIndexes = snapshot.val();
                            let lineStopList = this.state.lineStopList; //Salva o array pego do firebase com a separação de paradas por linha
                            //Filters duplicate items and creates a single array
                            let mergedIndexes = prevIndexes.concat(newIndexes.filter(function (item) {
                                return prevIndexes.indexOf(item) < 0;
                            }));
                            this.setState({
                                stopIndexes: mergedIndexes,
                                lineStopList: { ...lineStopList, [line]: snapshot.val() } //Esse valor será usado para encontrar a posição no array da parada selecionada no array posteriormente
                            }, () => {
                                counter++;
                                if (counter == this.state.filteredLines.length) {
                                    resolve();

                                }
                            })
                        } else {
                            counter++;
                            if (counter == this.state.filteredLines.length) {
                                resolve();

                            }

                        }


                    })
                })



            })

            // .then((stopMarkers) => {
            //     AsyncStorage.setItem('buzee:' + line + '/' + direction + '/P', JSON.stringify(stopMarkers)).then(() => {
            //         resolve();
            //     })
            // }).catch(err => {
            //     console.log(err)
            //     reject();
            // });



        })

    }
    // getStopMarkers = (line, direction) => {
    //     return new Promise((resolve, reject) => {
    //         this.setState({
    //             stopMarkers: []
    //         }, () => {
    //             this.firebaseRef.database().ref().child(line + '/' + direction + '/P').once('value').then(snapshot => {
    //                 let stopMarkers = this.state.stopMarkers;

    //                 snapshot.forEach(childSnapshot => {
    //                     let childKey = childSnapshot.key;
    //                     let childData = childSnapshot.val();

    //                     stopMarkers.push({ identifier: childKey, latitude: childData.la, longitude: childData.lo, notifyOnEntry: true, notifyOnExit: true, notifyOnDwell: false });
    //                 });

    //                 // this.props.onStoreStops(stopMarkers)
    //                 this.setState({
    //                     stopMarkers: stopMarkers
    //                 }, () => {
    //                     resolve()

    //                 })
    //                 // return stopMarkers
    //             })
    //             // .then((stopMarkers) => {
    //             //     AsyncStorage.setItem('buzee:' + line + '/' + direction + '/P', JSON.stringify(stopMarkers)).then(() => {
    //             //         resolve();
    //             //     })
    //             // }).catch(err => {
    //             //     console.log(err)
    //             //     reject();
    //             // });
    //         })


    //     })

    // }

    compare = (a, b) => {
        // Use toUpperCase() to ignore character casing
        const genreA = a.line.toUpperCase();
        const genreB = b.line.toUpperCase();

        let comparison = 0;
        if (genreA > genreB) {
            comparison = 1;
        } else if (genreA < genreB) {
            comparison = -1;
        }
        return comparison;
    }

    getAvailableLines = () => {
        return new Promise((resolve) => {
            this.firebaseRef.database().ref().child('Li/TS').once('value').then(snapshot => {
                let timeStamp = snapshot.val();
                console.log('Pegou o TS das linhas do servidor: ', timeStamp);
                return timeStamp

            }).then((timeStamp) => {
                AsyncStorage.getItem('buzee:Li/TS').then((value) => {
                    console.log('Pegou o TS das linhas da memoria: ', value);
                    if (value !== null) {
                        if (value.toString() == timeStamp.toString()) {
                            console.log('Valores de data são iguais');
                            AsyncStorage.getItem('buzee:Li').then((linhas) => {
                                parseLines = JSON.parse(linhas);
                                this.setState({
                                    linesList: parseLines,
                                    filteredLines: [parseLines[0].key, parseLines[1].key]  //PEGA AS DUAS PRIMERIAS LINHAS DA LISTA POR PADRÃO
                                })
                                resolve();
                            })


                        } else {
                            console.log('valores de data são diferentes');
                            AsyncStorage.setItem('buzee:Li/TS', timeStamp.toString()).then(() => {
                                console.log('Setou TS na memoria')
                                return this.firebaseRef.database().ref().child('Li/Li').once('value').then(snapshot => {
                                    let parseLines = [];
                                    snapshot.forEach(childSnapshot => {
                                        let childKey = childSnapshot.key;
                                        let childData = childSnapshot.val();

                                        parseLines.push({ key: childKey, line: childData });
                                    });
                                    parseLines.sort(this.compare); //Array of objects
                                    this.setState({
                                        linesList: parseLines,
                                        filteredLines: [parseLines[0].key, parseLines[1].key]//PEGA AS DUAS PRIMERIAS LINHAS DA LISTA POR PADRÃO
                                    })
                                    return parseLines
                                }).then((parseLines) => {
                                    AsyncStorage.setItem('buzee:Li', JSON.stringify(parseLines)).then(() => {
                                        resolve();
                                    })
                                })
                            })

                        }
                    } else {
                        console.log('Não há nada na memória, pegando dados novos');

                        AsyncStorage.setItem('buzee:Li/TS', timeStamp.toString()).then(() => {
                            console.log('Setou TS na memoria')
                            return this.firebaseRef.database().ref().child('Li/Li').once('value').then(snapshot => {
                                let parseLines = [];
                                snapshot.forEach(childSnapshot => {
                                    let childKey = childSnapshot.key;
                                    let childData = childSnapshot.val();

                                    parseLines.push({ key: childKey, line: childData });
                                });
                                parseLines.sort(this.compare);
                                this.setState({
                                    linesList: parseLines,
                                    filteredLines: [parseLines[0].key, parseLines[1].key]//PEGA AS DUAS PRIMERIAS LINHAS DA LISTA POR PADRÃO
                                })
                                return parseLines
                            }).then((parseLines) => {
                                AsyncStorage.setItem('buzee:Li', JSON.stringify(parseLines)).then(() => {
                                    resolve();
                                })
                            })
                        })
                    }
                })
            })
        })

    }

    initializeNotification = () => {
        // RNfirebase.messaging().hasPermission()
        //     .then(enabled => {
        //         if (enabled) {
        //             // user has permissions
        //         } else {
        //             RNfirebase.messaging().requestPermission()
        //                 .then(() => {
        //                     // User has authorised  
        //                 })
        //                 .catch(error => {
        //                     // User has rejected permissions  
        //                 });
        //         }
        //     });
        console.log('Notification Listener activated');
        this.notificationListener = firebase.notifications().onNotification((notification) => {

            notification.android.setChannelId('notification-channel')
            notification.android.setSmallIcon('drawable/src_assets_white_logo')
            // notification.android.setLargeIcon('mipmap/ic_notification')
            notification.android.setColor('#095796')
            notification.android.setColorized('true')
            firebase.notifications().displayNotification(notification)
        });
    }

    componentDidMount() {

        firebase.messaging().getToken().then((token) => {
            this.firebaseRef.database().ref().child('users/' + this.props.user.uid + '/FCM').set(token).then(() => {
                this.firebaseRef.database().ref().child('users/' + this.props.user.uid + '/LS').set(Date.now())
            })
        })

        this.initializeNotification();

    }

    componentWillUnmount() {
        BackgroundGeolocation.stop();
        BackgroundGeolocation.removeGeofences();
        BackgroundGeolocation.removeListeners();
        if (this.itemsRef) {
            this.itemsRef.off();
        }
        if (this.swapTimer) {
            clearInterval(this.swapTimer)
        }


        this.notificationListener();
    }


    checkLocationAccess = () => {
        return new Promise((resolve) => {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(result => {
                console.log('result:', result);
                this.setState({
                    locationPermission: result
                })
                resolve(result);

            })
        })

    }

    requestLocationAccess = () => {
        return new Promise((resolve, reject) => {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(result => {
                if (result === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("You can use the location")
                    this.setState({
                        locationPermission: true
                    }, () => {
                        resolve()
                    })
                } else {
                    console.log("Location permission denied")
                    ToastAndroid.show('Para compartilhar um bus, permita acesso à localização nas configurações', ToastAndroid.LONG)
                    reject();
                }
            }).catch(err => {
                console.warn(err);
            })
        })
    }

    clearBuses = () => {
        return new Promise((resolve) => {
            this.setState({
                activeBuses: null
            })
            resolve()
        })
    }

    /*----------------------------------------------*/
    /*----------------------------------------------*/
    /*EVENT LISTENERS GO HERE */
    /*----------------------------------------------*/
    /*----------------------------------------------*/

    listenForItems(itemsRef) {
        console.log('itemsref is on');

        this.clearBuses().then(() => {
            this.updateBusMarkers();

            itemsRef.on('child_removed', (snap) => { //Listener for deleting buses
                console.log('Bus deleted on database')
                if (this.state.activeBuses) {
                    let busKey = snap.key;
                    let busList = this.state.activeBuses;

                    console.log('Before ', busList);
                    console.log(this.state.activeBuses)
                    console.log(busList['busKey']);
                    if (busList[busKey]) {
                        delete busList[busKey];
                    }

                    this.setState({
                        activeBuses: busList
                    })
                    console.log('After ', busList);
                    this.updateBusMarkers();
                }
            })

            itemsRef.on('child_changed', (snap) => { //Listener for buses that are being updated. Avoids reading old buses
                let path = snap.ref.path; //Pega caminho até o ônibus que acabou de ser atualizado

                this.setState(prevState => {
                    return {
                        activeBuses: { ...prevState.activeBuses, [snap.key]: { ...snap.val(), path: path } }
                    }
                })
                console.log('Active Buses list: ', this.state.activeBuses)

                this.updateBusMarkers();


            })
        })





    }

    onLocation = (location) => {
        // console.log(location);

        this.map.animateToRegion({
            ...this.state.focusedLocation,
            latitude: location.coords.latitude,
            longitude: location.coords.longitude
        });
    }

    // onLocation = (location) => {
    //     console.log('- [event] location: ', location);
    //     if (location.coords.latitude && location.coords.longitude){
    //         this.map.animateToRegion({
    //             ...this.state.focusedLocation,
    //             latitude: location.coords.latitude,
    //             longitude: location.coords.longitude
    //         });
    //         this.setState(prevState => {
    //             return {
    //                 focusedLocation: {
    //                     ...prevState.focusedLocation,
    //                     latitude: location.coords.latitude,
    //                     longitude: location.coords.longitude
    //                 }
    //             };
    //         })
    //     }
    // }

    onError = (error) => {
        console.log(error);
    }



    onEnableGeofence = (isEnabled) => {
        this.setState({
            sharing: isEnabled
        })

    }

    onActivityChange = (activityChange) => {
        // alert('Atividade: ' + activityChange.activity + ' ' + activityChange.confidence);
        if (activityChange.activity == 'in_vehicle' && this.state.sharing && activityChange.confidence > 70) {

            this.setState({
                embarked: true
            })

        }
        if (this.state.embarked && activityChange.activity === 'on_foot' && activityChange.confidence > 80) {
            this.stopSharingLocation();
        }
    }






    /*----------------------------------------------*/
    /*----------------------------------------------*/
    /*EVENT LISTENERS END HERE */
    /*----------------------------------------------*/
    /*----------------------------------------------*/

    openBusTimeModal = (event) => {
        let id = event.nativeEvent.id;
        console.log(id);
        this.firebaseRef.database().ref().child('tag/' + id).once('value').then((snapshot) => {
            console.log(snapshot.val());
            linhas = snapshot.val();
            selectedLines = linhas.filter(linha => this.state.filteredLines.includes(linha)) //Das linhas que passam pela parada selecionada, seleciona apenas as filtradas pelo user
            return selectedLines
        }).then((selectedLines) => {
            Navigation.showModal({
                stack: {
                    children: [{
                        component: {
                            name: 'buzee.BusTimeModal',
                            passProps: {
                                id: id,
                                lines: selectedLines,
                                lineStopList: this.state.lineStopList,
                                linesList: this.state.linesList,
                                direction: this.state.direction,
                                getStops: this.getStops,
                                directionIndex: this.state.directionIndex,
                                favoriteStops: this.state.favoriteStops,
                                updateFavoriteMarkers: this.updateFavoriteMarkers
                            }

                        }
                    }]
                }
            });

        })

    }

    startSharingLocation = () => {
        if (this.state.locationPermission) {
            let filteredLines = [];
            let orderedLines = this.state.linesList; //Lista completa de linhas disponiveis
            this.state.filteredLines.forEach(line => { //Separa as linhas filtradas das linhas disponiveis
                orderedLines = orderedLines.filter(item => {
                    if (item.key !== line) {
                        return true
                    } else {
                        filteredLines = [...filteredLines, item]
                        return false
                    }
                })
            })
            orderedLines = [...filteredLines, ...orderedLines] //Coloca as filtradas no topo da pilha
            Navigation.showModal({
                stack: {
                    children: [{
                        component: {
                            name: 'buzee.ShareLocation',
                            passProps: {
                                lines: orderedLines,
                                // selectedDirection: this.state.selectedDirection,
                                direction: this.state.direction,
                                sendHandler: this.updateSelectedLineOnSend,
                            }

                        }
                    }]
                }
            });
        } else {
            this.requestLocationAccess().then(() => {
                this.startSharingLocation();
            }).catch(() => {
                console.log('User didnt allow access to location');
            });
        }




    }

    stopSharingLocation = () => {
        let selectedLine;
        let selectedDirection;
        let uid = this.props.user.uid;
        AsyncStorage.setItem('buzee:isRunning', 'false').then(() => {
            BackgroundGeolocation.stop();
            BackgroundGeolocation.removeListeners();
            BackgroundGeolocation.removeGeofences();

            this.setState({
                embarked: false
            })


            // this.props.updateLevel(this.props.user);
            //Pontua para o usuário
            console.log(this.props.user.uid);
            console.log(this.props.user);
            console.log(this.firebaseRef)
            BackgroundGeolocation.getOdometer().then((result) => {
                // alert('Total de ' + result + ' km percorridos');
                this.firebaseRef.database().ref().child('users/' + uid + '/odometer').once('value').then((snapshot) => {
                    console.log(snapshot.val())
                    let newOdometer = result + snapshot.val();
                    return this.firebaseRef.database().ref().child('users/' + uid + '/odometer').set(newOdometer);
                });
            });
            BackgroundGeolocation.getState((state2) => {
                return new Promise((resolve) => {
                    selectedLine = state2.params.L;
                    selectedDirection = state2.params.D;
                    resolve();
                }).then(() => {
                    console.log('selectedline')
                    console.log(selectedLine);
                    this.firebaseRef.database().ref().child('Linhas/' + selectedLine + '/' + selectedDirection).once('value').then((snapshot) => {
                        snapshot.forEach(childSnapshot => {
                            var bus_key = childSnapshot.key;
                            var users = [];
                            // let index = 0;
                            console.log(bus_key);
                            users = childSnapshot.val().uid; //Pega lista de usuarios embarcados
                            console.log('users: ', users);
                            users.map((user, index) => {
                                if (user == this.props.user.uid) {
                                    console.log('Encontrou usuário na lista, desembarcando..')
                                    console.log({ selectedLine, selectedDirection, bus_key, index })
                                    this.firebaseRef.database().ref().child('Linhas/' + selectedLine + '/' + selectedDirection + '/' + bus_key + '/uid/' + index).remove().then(() => {

                                        if (users.length <= 1) {
                                            console.log('Apagando ônibus por falta de usuários')
                                            console.log('Linhas/' + selectedLine + '/' + selectedDirection + '/' + bus_key)
                                            this.firebaseRef.database().ref().child('Linhas/' + selectedLine + '/' + selectedDirection + '/' + bus_key).remove()
                                        }
                                        return;
                                    })
                                }
                                // index = index + 1;
                            })

                        })
                    });
                })
            })


            BackgroundGeolocation.getOdometer().then((result) => {

                this.showNotification(result);

            });
        })

    }

    showNotification = (odometer) => {

        const notification = new firebase.notifications.Notification()
            .setNotificationId('notificationId')
            .setTitle('Obrigado por compartilhar!')
            .setBody('Você acumulou ' + Math.trunc(odometer / 1000) + ' km com esta viagem!')
        notification.android.setSmallIcon('drawable/src_assets_white_logo')
        notification.android.setColor('#095796')
        notification.android.setLargeIcon('mipmap/ic_notification')
        notification.android.setChannelId('notification-channel')
        notification.android.setColorized('true')
        firebase.notifications().displayNotification(notification)

    }


    // selectedLineHandler = (itemValue) => {
    //     this.setState({
    //         selectedLine: itemValue.key,
    //         selectedLineName: itemValue.line,
    //     });

    //     this.getDirections(itemValue.key).then(() => {
    //         this.getStops(itemValue.key, this.state.selectedDirection).then(() => {
    //             //Restarts listener for buses on newly selected line
    //             this.itemsRef.off();
    //             this.itemsRef = firebase.database().ref().child('Linhas/' + itemValue.key + '/' + this.state.selectedDirection);
    //             this.listenForItems(this.itemsRef);

    //         });
    //     });


    // }

    updateSelectedLineOnSend = (key, direction, selectedDirection, directionIndex) => {
        RatingTracker.handlePositiveEvent();
        return new Promise((resolve, reject) => {
            BackgroundGeolocation.onEnabledChange(isEnabled => {
                this.onEnableGeofence(isEnabled);
            });

            let lineName;
            this.state.linesList.forEach(item => {
                if (item.key == key) {
                    lineName = item.line
                    return;
                }
            });
            //REAL
            // console.log(this.state.filteredLines)
            return new Promise((resolve) => {
                this.setState({
                    direction: direction,
                    selectedDirection: selectedDirection,
                    selectedLineName: lineName,
                    selectedLine: key,
                    directionIndex: directionIndex
                }, () => {
                    if (!this.state.filteredLines.includes(key)) {
                        console.log('Adiciona linha')
                        this.setState({
                            filteredLines: [...this.state.filteredLines, key]
                        }, () => {
                            resolve()
                        })

                    } else {
                        resolve()
                    }
                });


            }).then(() => {

                BackgroundGeolocation.setConfig({
                    autoSync: true,
                    params: {               // <-- Optional HTTP params
                        //REAL
                        'L': key,
                        //TESTE!
                        // 'L': 'testeFxV',
                        'D': selectedDirection,
                        'U': this.props.user.uid
                    },
                });
                BackgroundGeolocation.start();

                this.getStops().then(() => {
                    console.log(this.state.stopMarkers)
                    // BackgroundGeolocation.addGeofences(this.props.stopMarkers);
                    BackgroundGeolocation.resetOdometer();
                    BackgroundGeolocation.on('activitychange', this.onActivityChange);
                    BackgroundGeolocation.on('location', this.onLocation);
                    AsyncStorage.setItem('buzee:isRunning', 'true').then(() => {
                        resolve()
                    })

                }).catch(() => {
                    reject();
                });

            })

        })

    }
    reportAC = () => {
        this.setState({
            ACReported: true
        })
    }
    reportAccessible = () => {
        this.setState({
            accessibleReported: true
        })
    }
    reportCrowded = () => {
        this.setState({
            crowdedReported: true
        })
    }
    reportDirty = () => {
        this.setState({
            dirtyReported: true
        })
    }
    reportCrazy = () => {
        this.setState({
            crazyReported: true
        })
    }
    reportRating = (rating) => {
        this.setState({
            ratingReported: true,
            rating: rating
        })
    }


    openBusDetail = (event) => {
        let id = event.nativeEvent.id;

        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.BusDetail',
                        passProps: {
                            selectedBusId: id,
                            activeBuses: this.state.activeBuses,
                            linesList: this.state.linesList,
                            uid: this.props.user.uid,
                            crowdedReported: this.state.crowdedReported,
                            accessibleReported: this.state.accessibleReported,
                            ACReported: this.state.ACReported,
                            reportCrowded: this.reportCrowded,
                            reportAC: this.reportAC,
                            reportAccessible: this.reportAccessible,

                        }
                    },

                }]
            }
        });



    }


    updateBusMarkers = () => {
        //console.log(this.state.activeBuses);
        if (this.state.activeBuses) {
            let buses = [];
            let marker;
            const entries = Object.entries(this.state.activeBuses);

            entries.forEach(bus => {
                console.log(bus);
                let coords = bus[1];
                let key = bus[0];
                let bus1 = bus;
                marker = <MapView.Marker
                    coordinate={{ latitude: coords.la, longitude: coords.lo }}
                    // image={Buz}
                    identifier={key}
                    onPress={(event) => this.openBusDetail(event)}>

                    <Image source={Buz} style={{ height: 40, width: 40 }} resizeMode='contain' />
                </MapView.Marker >

                buses.push(marker);

            })
            this.setState({
                busMarkers: buses
            })
        } else {
            this.setState({
                busMarkers: null
            })
        }

    }

    openProfileScreen = () => {

        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.ProfileScreen',
                        passProps: {
                            configs: this.state.configs
                        }
                    }
                }]
            }
        });



    }

    menuHandler = () => {
        this.setState(prevState => {
            return {
                menuButton: !prevState.menuButton
            }
        })
    }

    showLines = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.LinesScreen',
                        passProps: {
                            linesList: this.state.linesList,
                            filteredLines: this.state.filteredLines,
                            getStops: this.getStops,
                            updateFilteredLines: this.updateFilteredLines
                        }

                    }
                }]
            }
        });
    }

    openReportScreen = () => {

        let filteredLines = [];
        let orderedLines = this.state.linesList; //Lista completa de linhas disponiveis
        this.state.filteredLines.forEach(line => { //Separa as linhas filtradas das linhas disponiveis
            orderedLines = orderedLines.filter(item => {
                if (item.key !== line) {
                    return true
                } else {
                    filteredLines = [...filteredLines, item]
                    return false
                }
            })
        })
        orderedLines = [...filteredLines, ...orderedLines] //Coloca as filtradas no topo da pilha

        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.ReportScreen',
                        passProps: {
                            sharing: this.state.sharing,
                            linesList: orderedLines,
                            uid: this.props.user.uid,
                            selectedLine: this.state.selectedLine,
                            selectedLineName: this.state.selectedLineName,
                            dirtyReported: this.state.dirtyReported,
                            crazyReported: this.state.crazyReported,
                            ratingReported: this.state.ratingReported,
                            reportDirty: this.reportDirty,
                            reportCrazy: this.reportCrazy,
                            reportRating: this.reportRating,
                            rating: this.state.rating
                        }
                    }
                }]
            }
        });

    }

    // changeDirection = () => {
    //     let direction;
    //     if (this.state.direction[0] == this.state.selectedDirection) {
    //         this.setState({
    //             selectedDirection: this.state.direction[1]
    //         }, () => {
    //             this.getStops();
    //         })
    //         direction = this.state.direction[1]

    //     } else {
    //         this.setState({
    //             selectedDirection: this.state.direction[0]
    //         }, () => {
    //             this.getStops();
    //         })
    //         direction = this.state.direction[0]
    //     }




    //     // this.itemsRef.off();
    //     // this.itemsRef = firebase.database().ref().child('Linhas/' + this.state.selectedLine + '/' + direction);
    //     // this.listenForItems(this.itemsRef);

    // }


    changeDirection = () => {
        // console.log(this.state.directionIndex)
        let directionIndex = null

        switch (this.state.directionIndex) {
            case 0:
                directionIndex = 1
                this.setState({
                    directionIndex: 1,
                    exists: null
                })
                break;
            case 1:
                directionIndex = 0
                this.setState({
                    directionIndex: 0,
                    exists: null
                })
                break;
        }
        console.log(directionIndex)
        // this.getTimes(directionIndex, this.state.day);
        // this.getDirections(this.state.filteredLines[0], directionIndex)
        this.getStops();

    }



    isFavorite = (id) => {
        if (this.state.favoriteStops.includes(id)) {
            return true
        } else {
            return false
        }
    }


    //Função passada pra tela das paradas como prop pra atualizar a lista de paradas favoritas e 
    //re renderizar para atualiazr a imagem dos marcadores
    updateFavoriteMarkers = (favoriteStops) => {
        this.setState({
            favoriteStops: favoriteStops
        }, () => {
            this.markerRef.redraw(); //rerender dos markers
        })

    }



    toggleSideMenu = () => {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen });
    }

    onMenuItemSelected = (item) => {
        this.setState({
            isOpen: false,
        });
        switch (item) {
            case 'Ajuda': {
                Navigation.showModal({
                    stack: {
                        children: [{
                            component: {
                                name: 'buzee.SideAjuda',
                            }
                        }]
                    }
                });
                break;
            }
            case 'Quem': {
                Navigation.showModal({
                    stack: {
                        children: [{
                            component: {
                                name: 'buzee.SideQuem',
                            }
                        }]
                    }
                });
                break;
            }
            case 'Contato': {
                Navigation.showModal({
                    stack: {
                        children: [{
                            component: {
                                name: 'buzee.SideContato',
                            }
                        }]
                    }
                });
                break;
            }
            case 'Cidade': {
                Navigation.showModal({
                    stack: {
                        children: [{
                            component: {
                                name: 'buzee.SideCidade',
                            }
                        }]
                    }
                });
                break;
            }
            case 'Facebook': {

                Linking.canOpenURL('https://www.facebook.com/buzeemob/')
                    .then((supported) => {
                        if (!supported) {
                            console.log("Can't handle url: " + 'https://www.facebook.com/buzeemob/');
                        } else {
                            return Linking.openURL('https://www.facebook.com/buzeemob/');
                        }
                    })
                    .catch((err) => console.error('An error occurred', err));
                break;
            }
            case 'Instagram': {
                Linking.canOpenURL('https://www.instagram.com/buzeemob/')
                    .then((supported) => {
                        if (!supported) {
                            console.log("Can't handle url: " + 'https://www.instagram.com/buzeemob/');
                        } else {
                            return Linking.openURL('https://www.instagram.com/buzeemob/');
                        }
                    })
                    .catch((err) => console.error('An error occurred', err));
                break;
            }
            case 'Twitter': {
                Linking.canOpenURL('https://twitter.com/Buzeemob')
                    .then((supported) => {
                        if (!supported) {
                            console.log("Can't handle url: " + 'https://twitter.com/Buzeemob');
                        } else {
                            return Linking.openURL('https://twitter.com/Buzeemob');
                        }
                    })
                break;
            }
            case 'Convidar': {
                Navigation.showModal({
                    stack: {
                        children: [{
                            component: {
                                name: 'buzee.InviteFriends',
                            }
                        }]
                    }
                });

                break;
            }
            case 'Chat': {
                Navigation.showModal({
                    stack: {
                        children: [{
                            component: {
                                name: 'buzee.ChatFeed',
                            }
                        }]
                    }
                });
                break;
            }
        }
    }

    render() {
        let marker = this.state.stopMarkers; //Marcadores das paradas
        let buses = null;
        if (this.state.busMarkers) {
            buses = this.state.busMarkers //Marcadores dos onibus 
        } else {
            buses = null
        }
        if (this.state.loading) {
            loading = (
                <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='large' color='#5E9BD4' />
                </View>
            )
        } else {
            loading = null
        }

        const menu = <Menu navigator={navigator} onItemSelected={this.onMenuItemSelected} city={this.state.configs.Cidade} />;
        return (
            <SideMenu
                menu={menu}
                isOpen={this.state.isOpen}
                onChange={(isOpen) => {
                    this.updateMenuState(isOpen)
                }}

            >

                <View style={{ flex: 1, width: '100%' }}>

                    <View style={styles.container}>

                        <MapView
                            initialRegion={{
                                ...this.props.location,
                                latitudeDelta: 0.0122,
                                longitudeDelta: 0.0122
                            }}
                            mapPadding={{ top: 0, right: 0, left: 0, bottom: (Dimensions.get('window').width / 2.03) }}
                            style={[styles.map, { width: this.state.width }]}
                            onMapReady={() => {

                                this.getPosition();
                                this.setState({ mapReady: true, width: Dimensions.get('window').width })
                            }}
                            // style={styles.map}
                            ref={ref => this.map = ref} //this creates a reference to the "mapView" component, which allows methods inside the component to be called within the PickLocation class
                            showsUserLocation={true}
                            rotateEnabled={false}
                            showsMyLocationButton={false}
                        // customMapStyle={
                        //     mapStyle
                        // }
                        >

                            {buses}
                            {marker.map(marker => (
                                <MapView.Marker
                                    ref={(markerRef) => { this.markerRef = markerRef; }}
                                    identifier={marker.identifier.toString()}
                                    coordinate={{ latitude: marker.latitude, longitude: marker.longitude }}
                                    image={this.isFavorite(marker.identifier.toString()) ? favPin : Pin}
                                    onPress={(event) => this.openBusTimeModal(event)}
                                />



                            ))}

                        </MapView>
                        {loading}
                        <View style={{ padding: 5, position: 'absolute', flexDirection: 'row', top: 0, left: 0, width: '100%', backgroundColor: '#5E9BD4', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableWithoutFeedback onPress={this.toggleSideMenu}>
                                <View style={{ justifyContent: 'center', width: '10%', margin: 5 }}>
                                    <Icon size={30} name='bars' color='white' />
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={this.showLines}>
                                <View style={{ justifyContent: 'center', width: '50%', }}>
                                    <Text style={{ fontSize: 18, color: 'white', fontFamily: 'Decker' }}>{this.state.viewSelectedLineName}</Text>
                                </View>
                            </TouchableWithoutFeedback>

                            <TouchableWithoutFeedback onPress={this.state.oneDirection ? () => alert('Só há um sentido nesta linha') : this.changeDirection}>
                                <View style={{ justifyContent: 'center', width: '30%', height: '100%' }}>
                                    <Text style={{ fontSize: 15, color: 'white', fontFamily: 'Decker' }}>{this.state.viewSelectedDirection}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={this.state.oneDirection ? () => alert('Só há um sentido nesta linha') : this.changeDirection}>
                                <Image source={InverterButton} style={{ ...styles.directionButton, transform: this.state.directionIndex == 1 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} />
                            </TouchableWithoutFeedback>
                        </View>


                        <View style={{ position: 'absolute', bottom: 0, right: 0, alignItems: 'center', width: '100%', height: (Dimensions.get('window').width / 2.03) }}>
                            <View style={{ position: 'absolute', bottom: 0 }}>
                                <Image source={this.state.sharing ? Off : On} style={{ width: Dimensions.get('window').width, height: (Dimensions.get('window').width / 2.03), resizeMode: 'contain' }} />
                            </View>



                            <View style={{ height: '65%', width: '100%' }}>
                                <TouchableWithoutFeedback onPress={this.openProfileScreen}>
                                    <View style={{ position: 'absolute', bottom: 0, left: 0, width: Dimensions.get('window').width / 3, height: '80%', borderRadius: 50 }}>

                                    </View>
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={this.state.sharing ? this.stopSharingLocation : this.startSharingLocation}>
                                    <View style={{ position: 'absolute', bottom: 0, left: Dimensions.get('window').width / 3, width: Dimensions.get('window').width / 3, height: '100%', borderRadius: 50 }}>

                                    </View>
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={this.showLines}>
                                    <View style={{ position: 'absolute', bottom: 0, left: 2 * Dimensions.get('window').width / 3, width: Dimensions.get('window').width / 3, height: '80%', borderRadius: 50 }}>

                                    </View>
                                </TouchableWithoutFeedback>
                            </View>


                            <View style={{ height: '35%', width: '100%' }}>
                                <TouchableWithoutFeedback onPress={this.openReportScreen}>
                                    <View style={{ position: 'absolute', bottom: 0, right: 0, alignItems: 'center', justifyContent: 'center', height: '100%', width: '100%' }}>

                                    </View>
                                </TouchableWithoutFeedback>
                            </View>




                        </View>


                    </View >




                </View>
            </SideMenu>

        );
    }

}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    map: {
        width: '100%',
        height: '100%',
    },
    textStyle: {
        color: '#5E9BD4',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    dropStyle: {
        flex: 1
    },
    button: {
        position: 'absolute'
    },
    menu: {
        position: 'absolute',
        bottom: 70,
        // width: '100%',
        // height: 45,
        right: 20,
        // borderWidth: 1,
        // borderColor: 'black'
    },
    directionButton: {

        // borderRadius: 50,
        width: 27,
        height: 35,
        resizeMode: 'contain',

        // justifyContent: 'center',
        // alignItems: 'center',

    },
    lineBar: {
        position: 'absolute',
        top: 20,
        left: 20,
        width: Dimensions.get('window').width * 0.65,
        backgroundColor: 'rgba(7, 84, 162, 0.8)',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: 'white',
        padding: 5
    }
});

const mapStateToProps = state => {
    return {
        user: state.user.user,
        stopMarkers: state.geolocation.stopMarkers,
        location: state.geolocation.location,
        firebaseRef: state.firebaseRef
    }
}
const mapDispatchToProps = dispatch => {
    return {
        updateLevel: (user) => dispatch(updateLevel(user)),
        onStoreStops: (stopMarkers) => dispatch(storeStops(stopMarkers))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);