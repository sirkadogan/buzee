import React, { Component } from 'react';
import { View, Image, StyleSheet, ImageBackground, PermissionsAndroid, AsyncStorage, NetInfo } from 'react-native';
import login from '../../assets/login.jpg';
import Logo from '../../assets/White_Logo.png';

// import * as firebase from 'firebase';

import { Navigation } from 'react-native-navigation';
import { GoogleSignin, } from 'react-native-google-signin';
import startMainTabs from '../../screens/MainTabs/startMainTabs/';
import BackgroundGeolocation from 'react-native-background-geolocation';
//Connecting to redux
import { connect } from 'react-redux';
import { googleLogin, storeUser, storeStops, storeLocation, storeFirebase } from '../../store/actions/index';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import firebase from 'react-native-firebase';
// Initialize Firebase



class SplashScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
    }

    constructor(props) {
        super(props);
        // console.ignoredYellowBox = ['Warning: Each', 'Setting'];
        // console.disableYellowBox = true;
        GoogleSignin.configure({
            webClientId: '319959934665-b44f43c1t1c0ik370alhpibfrohblgd6.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            hostedDomain: '', // specifies a hosted domain restriction
            forceConsentPrompt: false, // [Android] if you want to show the authorization prompt at each login
            accountName: '', // [Android] specifies an account name on the device that should be used
        });

    }

    componentDidMount() {
        // RNfirebase.messaging().getToken().then((token) => {
        //     console.log(token);
        // })

        let firebaseConfig;
        let firebaseApp;
        AsyncStorage.getItem('buzee:selectedCity').then((value) => {
            if (value !== null) {
                switch (value) {
                    case 'SM':
                        firebaseConfig = {
                            apiKey: "AIzaSyCK36qUnfS0UetJwTNlKRqEVpsmjvivHgc",
                            authDomain: "buzee2-9c40b.firebaseapp.com",
                            databaseURL: "https://buzee2-9c40b.firebaseio.com/",
                            projectId: "buzee2-9c40b",
                            storageBucket: "buzee2-9c40b.appspot.com",
                            messagingSenderId: "319959934665",
                            appId: 'SM'
                        };
                        firebaseApp = firebase.initializeApp(firebaseConfig, 'SM');
                        this.props.onStoreFirebase(firebase.app('SM'))
                        break;
                    case 'POA':
                        firebaseConfig = {
                            apiKey: "AIzaSyCK36qUnfS0UetJwTNlKRqEVpsmjvivHgc",
                            authDomain: "buzee2-9c40b.firebaseapp.com",
                            databaseURL: "https://buzee2-9c40b-d2c90.firebaseio.com/",
                            projectId: "buzee2-9c40b",
                            storageBucket: "buzee2-9c40b.appspot.com",
                            messagingSenderId: "319959934665",
                            appId: 'POA'
                        };
                        firebaseApp = firebase.initializeApp(firebaseConfig, 'POA');
                        this.props.onStoreFirebase(firebase.app('POA'))
                        break;
                }

                firebaseApp.onReady().then(() => {
                    BackgroundGeolocation.setConfig({
                        url: firebaseApp.options.databaseURL + 'location.json',
                    }).then(() => {
                        this.start();
                    })

                })

            } else {
                //Se não tiver nenhuma cidade selecionada, vai para tela de seleção
                Navigation.setRoot({
                    root: {
                        stack: {
                            children: [{
                                component: {
                                    name: 'buzee.SideCidade'
                                }
                            }]
                        }
                    },
                    layout: {
                      orientation: ['portrait']
                    }
                });
            }

        })





    }

    getPosition = () => {
        return new Promise((resolve, reject) => {

            this.props.firebaseRef.firebaseRef.database().ref().child('Config/location').once('value').then((snapshot) => {
                let location = snapshot.val();
                this.props.onStoreLocation(location);
                resolve();
            })

        })
    }

    start = () => {
        NetInfo.getConnectionInfo().then((connectionInfo) => {

            if (connectionInfo.type == 'none') {
                alert('Você precisa estar conectado a internet para utilizar o Buzee!');
            } else {
                this.authSubscription = this.props.firebaseRef.firebaseRef.auth().onAuthStateChanged((user) => {
                    // this.timer = setTimeout(() => { alert('Algo deu errado :( Confira sua conexão com a internet e abra o app novamente.') }, 30000)
                    if (user) {
                        this.props.onStoreUser(user);
                        return this.props.firebaseRef.firebaseRef.database().ref().child('users/' + user.uid + '/').once('value').then((snapshot) => {
                            if (!snapshot.exists()) {
                                console.log('User não foi criado ainda. Criando..')
                                this.props.firebaseRef.firebaseRef.database().ref().child('users/' + user.uid + '/').set({ level: 1, odometer: 0 })
                            }
                        }).then(() => {
                            // clearTimeout(this.timer);
                            this.getPosition().then(() => {
                                startMainTabs();
                            })
                        })

                    } else {
                        // No user is signed in.
                        // setTimeout(() => {
                        // clearTimeout(this.timer);
                        // this.requestLocationAccess().then(() => {
                        Navigation.setRoot({
                            root: {
                                stack: {
                                    children: [{
                                        component: {
                                            name: 'buzee.AuthScreen'
                                        }
                                    }]
                                }
                            },
                            layout: {
                              orientation: ['portrait']
                            }
                        });
                        // });

                        // }, 500)

                    }
                });




            }

        });
    }

    // clearTimeout = () => {
    //     // if (this.timer) {
    //     //     clearTimeout(this.timer);
    //     // }
    //     this.authSubscription();
    // }


    componentWillUnmount() {
        if (this.authSubscription) {
            this.authSubscription();
        }
        // if (this.timer) {
        //     clearTimeout(this.timer);
        // }
    }


    requestLocationAccess = () => {
        return new Promise((resolve, reject) => {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(result => {
                if (result === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("You can use the location")
                    resolve();



                } else {
                    console.log("Location permission denied")
                    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(result => {
                        if (result === PermissionsAndroid.RESULTS.GRANTED) {
                            console.log("You can use the location")
                            resolve();
                        } else {
                            console.log("Location permission denied")

                            alert('Para o Buzee funcionar corretamente, você deve permitir o acesso a localização!');
                            reject();
                        }
                    }).catch(err => {
                        console.warn(err);
                        // alert('Permission denied')
                    })
                }
            }).catch(err => {
                console.warn(err);
                // alert('Permission denied')
            })

        })

    }

    render() {
        return (
            <ImageBackground source={login} style={styles.background}>
                <View style={styles.container}>
                    <Image source={Logo} style={{ width: 270, height: 110, resizeMode: 'contain' }} />
                </View>
            </ImageBackground>
        );
    }

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    background: {
        width: '100%',
        flex: 1
    }
});

const mapStateToProps = state => {
    return {
        isLoading: state.ui.isLoading,
        user: state.user.user,
        firebaseRef: state.firebaseRef
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onGoogleSignIn: () => dispatch(googleLogin()),
        onStoreUser: (user) => dispatch(storeUser(user)),
        onStoreStops: (stopMarkers) => dispatch(storeStops(stopMarkers)),
        onStoreLocation: (location) => dispatch(storeLocation(location)),
        onStoreFirebase: (firebaseRef) => dispatch(storeFirebase(firebaseRef))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
