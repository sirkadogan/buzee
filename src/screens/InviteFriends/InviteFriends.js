import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    ScrollView,
    TouchableWithoutFeedback,
    Linking,
    Share
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import email from 'react-native-email'

class SideContato extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);

    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    inviteFriends = () => {
        Share.share({
            message: 'Já ouviu falar do Buzee?\nÉ uma comunidade de usuários que compartilham a localização do ônibus :)\n\nBaixa lá:\nhttps://bit.ly/2W36gso'
        })

    }

    render() {

        let content = 'Quanto maior a comunidade, mais gente se ajudando. Já pensou?\n'
        let content2 = 'Convide já os seus amigos!\n'
        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>

                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Convidar Amigos</Text>
                </View>



                <View style={styles.middleContainer}>

                    <View style={{ alignItems: 'center', justifyContent: 'space-evenly', width: '100%', height: '100%' }} >
                        <Text style={styles.textStyle}>{content}</Text>
                        <Text style={styles.textStyle}>{content2}</Text>
                        <TouchableWithoutFeedback onPress={this.inviteFriends}>
                            <View style={{ borderRadius: 10, borderWidth: 3, borderColor: 'white', padding: 10, alignItems: 'center', justifyContent: 'center', width: '80%', marginBottom: 20 }}>
                                <Text style={{ ...styles.textStyle, fontSize: 22 }}>Convidar amigos</Text>
                            </View>
                        </TouchableWithoutFeedback>


                    </View>

                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 22,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

export default (SideContato);