import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    FlatList,
    Text,
    Dimensions,
    TouchableWithoutFeedback,
    AsyncStorage,
    Button,
    ActivityIndicator,
    ScrollView

} from 'react-native';

import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';


class LinesScreen extends Component {
    constructor(props) {
        super(props);
    }

    close = () => {
        Navigation.dismissModal(this.props.componentId)
    }

    openContato = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.SideContato',
                    }
                }]
            }
        });
    }

    render() {

        let content = "Ainda não tivemos a oportunidade de cadastrar todas as linhas da sua cidade, estamos em constante progresso para fornecer um melhor serviço.\n";
        let content2 = "Mas você sabia que você pode ajudar? O Buzee é uma rede colaborativa, queremos que você faça parte do desenvolvimento da nossa comunidade! Você mesmo pode cadastrar uma linha e ajudar outros usuários!\n";
        let content3 = "Se quiser saber como, fale conosco e saiba como você pode ajudar a comunidade: \n"

        return (

            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Ajuda</Text>
                </View>



                <View style={styles.middleContainer}>
                    <ScrollView contentContainerStyle={{ alignItems: 'center', justifyContent: 'space-evenly' }}>
                        <Text style={{...styles.titleStyle, fontFamily: 'Barlow-BoldItalic', marginBottom: 20}}>Por quê não achei minha linha?</Text>
                        <Text style={styles.textStyle}>{content}</Text>
                        <Text style={styles.textStyle}>{content2}</Text>
                        <Text style={styles.textStyle}>{content3}</Text>
                        <TouchableWithoutFeedback onPress={this.openContato}>
                            <View style={{ borderRadius: 10, borderWidth: 3, borderColor: 'white', padding: 10, alignItems: 'center', justifyContent: 'center', width: '80%', marginBottom: 20 }}>
                                <Text style={{ ...styles.textStyle, fontSize: 22 }}>Fale conosco</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </ScrollView>


                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >
        )
    }



}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
});



export default LinesScreen;
