import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    FlatList,
    Text,
    Dimensions,
    TouchableWithoutFeedback,
    AsyncStorage,
    Button,
    ActivityIndicator,
    ScrollView

} from 'react-native';

import { ButtonGroup, Divider, SearchBar } from 'react-native-elements';
import { Navigation } from 'react-native-navigation';
import BusTime from '../../components/BusTime/BusTime';
import ListItem from '../../components/ListItem/ListItem';
import Icon from 'react-native-vector-icons/FontAwesome';


class LinesScreen extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        filteredLines: this.props.filteredLines,
        loading: true,
        hasChange: false,
        search: '',
        availableLines: [],
    }

    searchFilterFunction = (text) => {
        //passing the inserted text in textinput
        let lines = this.state.availableLines;
        let newData = lines.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.line ? item.line.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        
        if(newData.length < 1){
            newData = lines.filter(function (item) {
                //applying filter for the inserted text in search bar
                const itemData = item.key ? item.key.toUpperCase() : ''.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
        }

        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            dataSource: newData,
            search: text,
        });
    }

    

    
    componentWillMount() {
        //Organiza as linhas disponives com as linhas filtradas no topo        
        if (this.state.filteredLines !== null) {
            let filteredLines = [];
            let data = this.props.linesList; //Lista completa de linhas disponiveis
            this.state.filteredLines.forEach(line => { //Separa as linhas filtradas das linhas disponiveis
                data = data.filter(item => {
                    if (item.key !== line) {
                        return true
                    } else {
                        filteredLines = [...filteredLines, item]
                        return false
                    }
                })
            })
            data = [...filteredLines, ...data] //Coloca as filtradas no topo da pilha
            this.setState({
                availableLines: data,
                dataSource: data
            })
        } else {
            this.setState({
                availableLines: this.props.linesList,
                dataSource: this.props.linesList
            })
        }

    }

    showTutorial = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.LinesTutorial',

                    }
                }]
            }
        });
    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);

    }

    buttonHandler = () => {
        if (this.state.hasChange) { //Se houver mudança, tem que atualizar as paradas
            this.setState({
                selectLines: null
            })
            if (this.state.filteredLines.length < 1) {
                alert('Selecione ao menos uma linha')
            } else {
                this.props.updateFilteredLines(this.state.filteredLines).then(() => {
                    this.props.getStops();
                    // this.props.swapSelectedLine();
                    Navigation.dismissModal(this.props.componentId)
                });

            }
        } else {
            //Fecha tela sem fazer nada
            Navigation.dismissModal(this.props.componentId)
        }
    }

    localAddToList = (lineKey) => {
        lines = this.state.filteredLines
        lines.push(lineKey)
        this.setState({
            filteredLines: lines,
            hasChange: true
        })
    }

    localRemoveFromList = (lineKey) => {
        lines = this.state.filteredLines
        lines = lines.filter(line => lineKey != line)
        console.log('removed: ', lines)
        this.setState({
            filteredLines: lines,
            hasChange: true
        })
    }


    render() {
        const { search } = this.state.availableLines;

        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '50%', width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{  position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='white' />
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ width: '80%' }}>
                            <Text style={styles.textStyle}>Selecionar linhas</Text>
                        </View>


                        <TouchableWithoutFeedback onPress={this.showTutorial}>
                            <View style={{   position: 'absolute', right: 0, padding: 10}}>
                                <Icon size={30} name='question-circle' color='white' />
                            </View>
                        </TouchableWithoutFeedback>


                    </View>
                    <View style={{ height: '50%', width: '100%', alignItems: 'center' }}>
                        <SearchBar
                            placeholder="Pesquisar"
                            onChangeText={this.searchFilterFunction}
                            value={this.state.search}
                            onClear={text => this.SearchFilterFunction('')}
                            platform={'android'}
                            searchIcon={<Icon size={30} name='search' color='white' />}
                            cancelIcon={<Icon size={30} name='search' color='white' />}
                            clearIcon={null}
                            containerStyle={{ backgroundColor: '#5E9BD4' }}
                            inputContainerStyle={{ backgroundColor: '#5E9BD4', width: '100%', height: '100%' }}
                            placeholderTextColor='white'
                            inputStyle={{ fontFamily: 'Decker', color: 'white' }}
                        />
                    </View>

                </View>
                <View style={styles.middleContainer}>
                    <FlatList
                        data={this.state.dataSource}
                        // maxToRenderPerBatch={1000} //Carrega masi itens por vez, melhora UX
                        // windowSize={60}           //Carrega masi itens por vez, melhora UX
                        // updateCellsBatchingPeriod={50}   //Carrega masi itens por vez, melhora UX
                        // initialNumToRender={50}//Carrega masi itens por vez, melhora UX
                        renderItem={({ item, index }) => (
                            <ListItem
                                index={index}
                                lineName={item.line}
                                lineKey={item.key}
                                addToList={this.localAddToList}
                                removeFromList={this.localRemoveFromList}
                                filteredLines={this.state.filteredLines}
                            />
                        )}
                    />
                </View>

                <View style={styles.bottomContainer}>
                    <TouchableWithoutFeedback onPress={this.buttonHandler}>
                        <View style={{ elevation: 2, backgroundColor: '#5E9BD4', borderRadius: 4, ...styles.buttonGroup, borderColor: 'white', borderWidth: 1, justifyContent: 'center' }}>
                            <Text style={styles.textStyle}>Confirmar</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>

            </View >

           

        )

    }



}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    topContainer: {
        height: '20%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center',

    },
    middleContainer: {
        height: '70%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    bottomContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '80%',
        elevation: 2

    }
});



export default LinesScreen;
