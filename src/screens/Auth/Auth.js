import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ImageBackground,
    Image,
    ActivityIndicator,
    TouchableNativeFeedback,
    PermissionsAndroid,
    ToastAndroid
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import backgroundImage from '../../assets/login.jpg';
import GoogleButton from '../../assets/GButton.png';
import EmailButton from '../../assets/EmailButton.png';
import Logo from '../../assets/White_Logo.png';
import { GoogleSignin } from 'react-native-google-signin';
import startMainTabs from '../../screens/MainTabs/startMainTabs/';
// import * as firebase from 'firebase';
import firebase from 'react-native-firebase';


//Connecting to redux
import { connect } from 'react-redux';
import { googleLogin, storeUser, storeLocation, storeStops, uiStopLoading, storeFirebaseAuth } from '../../store/actions/index';



class AuthScreen extends Component {


    state = {
        user: null,
        hasLocation: false,
        isLoading: false
    }

    constructor(props) {
        super(props);
        GoogleSignin.configure({
            webClientId: '319959934665-b44f43c1t1c0ik370alhpibfrohblgd6.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            hostedDomain: '', // specifies a hosted domain restriction
            forceConsentPrompt: false, // [Android] if you want to show the authorization prompt at each login
            accountName: '', // [Android] specifies an account name on the device that should be used
        });
    }

    getPosition = () => {
        return new Promise((resolve, reject) => {

            this.props.firebaseRef.firebaseRef.database().ref().child('Config/location').once('value').then((snapshot) => {
                let location = snapshot.val();
                this.props.onStoreLocation(location);
                resolve();
            })

        })
    }

    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef //Referência a instância do firebase
        this.requestLocationAccess()
        //Listener que fica esperando mudança no estado de autenticação. Ao autenticar, altera a tela
        this.authSubscription = this.firebaseRef.auth().onAuthStateChanged((user) => {
            this.props.onStoreFirebaseAuth(this.authSubscription); //Salva informações de autenticação do firebase
            if (user) {

                this.props.onStoreUser(user); //Guarda informações do user no redux
                this.firebaseRef.database()
                    .ref()
                    .child('users/' + user.uid + '/')
                    .once('value')
                    .then((snapshot) => {
                        if (!snapshot.exists()) {
                            console.log('User não foi criado ainda. Criando..');
                            let randomUsername = 'buzeer' + Math.floor(Math.random() * 89999 + 10000) //Gera um nome "aleatório" para o Buzee
                            this.firebaseRef.database()
                                .ref()
                                .child('users/' + user.uid + '/')
                                .set({ level: 1, odometer: 0, username: randomUsername })
                        }
                    }).then(() => {
                        this.getPosition().then(() => {
                            startMainTabs();
                        })
                    })
            } else {
                // No user is signed in.
            }
        });


    };

    componentWillUnmount() {
        this.authSubscription(); //Limpa o listener de autenticação
    }

    requestLocationAccess = () => {
        // return new Promise((resolve, reject) => {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(result => {
            if (result === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the location")
                // resolve();



            } else {
                console.log("Location permission denied")
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(result => {
                    if (result === PermissionsAndroid.RESULTS.GRANTED) {
                        console.log("You can use the location")
                        // resolve();
                    } else {
                        console.log("Location permission denied")
                        ToastAndroid.show('O Buzee funciona melhor com a localização ativada. Experimente habilitá-la depois :)', ToastAndroid.LONG)
                        // reject();
                    }
                }).catch(err => {
                    console.warn(err);
                    // alert('Permission denied')
                })
            }
        }).catch(err => {
            console.warn(err);
            // alert('Permission denied')
        })

        // })

    }


    //Função para acessar pelo google, ao autenticar
    //o listener setado em componentWillMount vai rodar e passar de tela
    onGoogleSignIn = () => {
        GoogleSignin.hasPlayServices().then(
            GoogleSignin.signIn().then(data => {
                const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
                this.setState({
                    isLoading: true
                })
                return this.firebaseRef.auth().signInAndRetrieveDataWithCredential(credential);
            })
        )
    };

    //Abre a tela de login por email
    onEmailSignIn = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.LoginModal',
                    }
                }]
            }
        });
    }



    render() {
        let circle = null;

        if (this.state.isLoading) {
            circle = <ActivityIndicator size='large' color='white' />
        }

        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                    <View style={styles.topContainer}>
                        <Image
                            style={{ width: 230, height: 110, resizeMode: 'contain' }}
                            source={Logo}
                        />
                        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            {circle}
                        </View>

                    </View>
                    <View style={styles.bottomContainer}>

                        <TouchableNativeFeedback onPress={this.onGoogleSignIn} >
                            <Image
                                style={{ width: 230, height: 48, marginBottom: 5 }}
                                source={GoogleButton}
                            />
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback onPress={this.onEmailSignIn} >
                            <Image
                                style={{ width: 230, height: 48, marginBottom: 5 }}
                                source={EmailButton}
                            />
                        </TouchableNativeFeedback>
                    </View>
                </ImageBackground>

            </View>
        );

    }
}

const styles = StyleSheet.create({
    topContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 20
    },
    bottomContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 20

    },
    backgroundImage: {
        width: '100%',
        flex: 1
    },
    portraitPasswordContainer: {
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    portraitPasswordWrapper: {
        width: '100%',

    }
});

const mapStateToProps = state => {
    return {
        isLoading: state.ui.isLoading,
        user: state.user.user,
        location: state.geolocation.location,
        firebaseRef: state.firebaseRef
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onGoogleSignIn: () => dispatch(googleLogin()),
        onStoreUser: (user) => dispatch(storeUser(user)),
        onStoreLocation: (location) => dispatch(storeLocation(location)),
        onStoreStops: (stopMarkers) => dispatch(storeStops(stopMarkers)),
        onStopLoading: () => dispatch(uiStopLoading()),
        onStoreFirebaseAuth: (authRef) => dispatch(storeFirebaseAuth(authRef))

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);