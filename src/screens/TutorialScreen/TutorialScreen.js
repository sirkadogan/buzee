import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    AsyncStorage,
    Text,
    ImageBackground,
    Image
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import fundo from '../../assets/fundo.png';
import TT1 from '../../assets/TT1.png';
import TT2 from '../../assets/TT2.png';
import TT3 from '../../assets/TT3.png';
import TT4 from '../../assets/TT4.png';
import TT5 from '../../assets/TT5.png';
import TT6 from '../../assets/TT6.png';
import TT7 from '../../assets/TT7.png';
import hand from '../../assets/hand.gif'
import pinGif from '../../assets/pinGif.gif'
import topGif from '../../assets/topGif.gif'

class TutorialScreen extends Component {
    constructor(props) {
        super(props);

    }
    static navigatorStyle = {
        navBarHidden: true
    }

    state = {
        activeSlide: 0,
    }

    componentWillMount() {





        let TT1view = (
            <View style={{ width: '100%', height: '100%' }}>
                <Image source={TT1} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                <View style={{ position: 'absolute', bottom: 5, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={hand} style={{ width: 125, height: 125 }} />
                </View>
            </View>
        )
        let TT2view = (
            <View style={{ width: '100%', height: '100%' }}>
                <Image source={TT2} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
            </View>
        )
        let TT3view = (
            <View style={{ width: '100%', height: '100%' }}>
                <Image source={TT3} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
            </View>
        )
        let TT4view = (
            <View style={{ width: '100%', height: '100%' }}>
                <Image source={TT4} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                <View style={{ position: 'absolute', bottom: '30%', left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={pinGif} style={{ width: 100, height: 100, resizeMode: 'contain' }} />
                </View>

            </View>
        )
        let TT5view = (
            <View style={{ width: '100%', height: '100%' }}>
                <Image source={TT5} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                <View style={{ position: 'absolute', bottom: '20%', left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={topGif} style={{ width: '80%', resizeMode: 'contain' }} />
                </View>
            </View>
        )
        let TT6view = (
            <View style={{ width: '100%', height: '100%' }}>
                <Image source={TT6} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
            </View>
        )
        let TT7view = (
            <View style={{ width: '100%', height: '100%' }}>
                <Image source={TT7} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                <TouchableWithoutFeedback onPress={this.startMainScreen}>
                    <View style={{ position: 'absolute', bottom: 20, left: 0, right: 0, justifyContent: 'center', alignItems: 'center', }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', borderColor: 'white', borderRadius: 10, borderWidth: 3, padding: 10, width: '70%' }}>
                            <Text style={{ fontFamily: 'Futura', color: 'white', fontSize: 22 }}>{'Bora lá! :)'}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )

        let imageArray = [TT1view, TT2view, TT3view, TT4view, TT5view, TT6view, TT7view];
        this.setState({
            imageArray: imageArray
        })
        AsyncStorage.setItem('buzee:showTutorial', true.toString());

        return new Promise((resolve, reject) => {
            AsyncStorage.getItem('buzee:showTutorial').then((value) => {
                console.log('Pegou o TS da memoria: ', value);
                if (value !== null) {
                    if (value === 'false') {
                        this.setState({
                            isChecked: false
                        })
                        resolve()
                    } else if (value === 'true') {
                        this.setState({
                            isChecked: true
                        })
                        resolve()
                    }
                }
            })
        })


    }

    startMainScreen = () => {
        Navigation.setRoot({
            root: {
                stack: {
                    children: [{
                        component: {
                            name: 'buzee.MainScreen',
                            passProps: {
                                showTutorial: true
                            }

                        }
                    }]
                }
            }
        });

    }


    render() {
        let finishButton;
        if (this.state.activeSlide == this.state.imageArray.length - 1) {
            finishButton = (
                <TouchableWithoutFeedback onPress={this.startMainScreen}>
                    <View style={{ position: 'absolute', bottom: 20, left: 0, right: 0, justifyContent: 'center', alignItems: 'center', }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', borderColor: 'white', borderRadius: 5, borderWidth: 3, padding: 10, width: '70%' }}>
                            <Text style={{ fontFamily: 'Futura', color: 'white', fontSize: 22 }}>{'Bora lá! :)'}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            )
        } else {
            finishButton = null;
        }

        return (
            <ImageBackground source={fundo} style={styles.background}>
                <View style={{ height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center', }}>
                    <View style={{ height: '80%' }}>
                        <Carousel
                            ref={(c) => { this._carousel = c; }}
                            data={this.state.imageArray}
                            renderItem={({ item, index }) => (
                                <View style={{ width: '100%', height: '100%' }}>
                                    {item}
                                </View>

                            )}
                            containerCustomStyle={{ flexGrow: 0 }}
                            sliderWidth={size.width}
                            itemWidth={size.width * 0.8}
                            onSnapToItem={(index) => this.setState({ activeSlide: index })}
                        />
                    </View>

                    <Pagination
                        activeDotIndex={this.state.activeSlide}
                        dotsLength={this.state.imageArray.length}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 8,
                            backgroundColor: 'white'
                        }}
                        inactiveDotStyle={{
                            // Define styles for inactive dots here
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />




                </View>

            </ImageBackground>






        )
    }
}
const size = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignContent: 'center'
    },
    background: {
        width: '100%',
        flex: 1,
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },

});

export default TutorialScreen;