import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    TextInput,
    FlatList,
    ActivityIndicator,
    TouchableWithoutFeedback,
    Alert,
    ToastAndroid
} from 'react-native';

import { connect } from 'react-redux';
import * as Progress from 'react-native-progress';
import Coupon from '../../components/Coupon/Coupon';
import Unknown from '../../assets/unknown.jpg';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { Divider } from 'react-native-elements';
// name = user.displayName;
// email = user.email;
// photoUrl = user.photoURL;
// emailVerified = user.emailVerified;
// uid = user.uid;
// USER OBJECT


class ProfileScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);
        this.state = {
            percent: 0,
            level: 1,
            coupons: [],
            revealed: false,
            selectedCoupon: null,
            tutorial: null,
            loading: true,
            editUsername: false,
            userReports: {
                rating: '0',
                noBus: '0',
                crowded: '0',
                dirty: '0',
                crazy: '0',
                crash: '0',
                flattire: '0',
                engine: '0'
            }

        };
    }

    componentWillMount() {
        let levelPercent = null;
        let level = 0;
        this.firebaseRef = this.props.firebaseRef.firebaseRef;
        let username;
        if (this.props.user) {
            this.firebaseRef.database().ref().child('users/' + this.props.user.uid).on('value', (snapshot) => {
                let coupons = [];
                let userReports = []
                data = snapshot.val();
                level = data.level;
                levelPercent = data.levelPercent;
                username = data.username;
                if (data.Coupons) {
                    const entries = Object.entries(data.Coupons);
                    entries.forEach(coupon => {
                        let childKey = coupon[0];
                        let childData = coupon[1];
                        coupons.push({ key: childKey, code: childData.code, revealed: childData.revealed });
                    });
                }
                if (data.game) {
                    const entries = Object.entries(data.game);
                    entries.forEach(value => {
                        // let childKey = coupon[0];
                        // let childData = value[1];
                        userReports = { ...userReports, [value[0]]: value[1] }

                    });
                    this.setState({
                        userReports: userReports
                    })
                }


                if (level) {
                    this.setState({
                        level: level,
                        percent: levelPercent,
                        coupons: coupons,
                        loading: false,
                        username: username

                    })

                } else {
                    this.setState({
                        percent: levelPercent,
                        loading: false,
                        username: username
                    })
                }


            });

        }

    }

    componentWillUnmount() {

        this.firebaseRef.database().ref().child('users/' + this.props.user.uid).off();
    }


    closeProfileScreen = () => {
        Navigation.dismissModal(this.props.componentId);

    }

    deleteCoupon = (key) => {
        Alert.alert('Tem certeza que quer deletar este cupom?', null, [
            {
                text: 'Sim',
                onPress: () => {
                    console.log('Deleting this coupon: ', key);
                    this.firebaseRef.database().ref().child('users/' + this.props.user.uid + '/Coupons/' + key).remove();
                }
            },
            {
                text: 'Cancelar',
                onPress: () => {
                    console.log('Cancelou delete')
                }
            }
        ])

    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    showTutorial = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.ProfileTutorial',

                    }
                }]
            }
        });
    }

    updateUsername = () => {
        let username = this.state.newName;
        if (!username) {
            ToastAndroid.show('Nome não pode ser vazio.', ToastAndroid.SHORT);
        } else {
            username = username.toUpperCase();

            if (!username.includes('BUZEE') && !username.includes('BUZZEE') && !username.includes('BUZZE') && !username.includes('BUUZZEE') && !username.includes('BUUZEE') && !username.includes('BUUZZE') && !username.includes('BUSEE') && !username.includes('BUZE')) {
                this.setState({
                    username: this.state.newName,
                    editUsername: false
                })
                this.firebaseRef.database().ref().child('users/' + this.props.user.uid + '/username').set(this.state.newName)
            } else {
                ToastAndroid.show('Seu nome contem uma palavra reservada!', ToastAndroid.SHORT);
            }
        }




    }


    render() {
        let renderList = null;
        let userImage = null;
        let displayName = null;
        let user = this.firebaseRef.auth().currentUser;



        if (this.state.loading) {
            renderList = (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size='large' color='white' />
                </View>
            )
        } else {
            if (this.props.configs.PolvoOfertas) {
                if (this.state.coupons.length != 0) {
                    renderList = (
                        <FlatList
                            style={{ width: '100%' }}
                            data={this.state.coupons}
                            renderItem={({ item, index }) => (
                                <Coupon
                                    code={item.code}
                                    value={'5-30%'}
                                    publisher={'Polvo Ofertas'}
                                    // deleteCoupon={() => { this.deleteCoupon(item.item.key) }}
                                    index={index}
                                />
                            )}
                        />
                    )
                } else {
                    let text = 'Você ainda não possui cupons.'
                    let text2 = ' Compartilhe sua localização quando estiver em um ônibus para ganhar!'
                    renderList = (
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-evenly' }}>
                            <Text style={{ ...styles.textStyle, fontSize: 18 }}>{text}</Text>
                            <Text style={{ ...styles.textStyle, fontSize: 18 }}>{text2}</Text>
                        </View>

                    )
                }
            } else {
                let text = 'Sua cidade ainda não possui um sistema de recompensas :('
                // let text2 = ' Compartilhe sua localização quando estiver em um ônibus para ganhar!'
                renderList = (
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-evenly' }}>
                        <Text style={{ ...styles.textStyle, fontSize: 18 }}>{text}</Text>
                        {/* <Text style={{ ...styles.textStyle, fontSize: 18 }}>{text2}</Text> */}
                    </View>

                )
            }
        }


        if (user.photoURL != null) {
            userImage = (
                <Image source={{ uri: this.props.user.photoURL }} style={styles.avatar} />
            )
        } else {
            userImage = (
                <Image source={Unknown} style={styles.avatar} />
            )
        }

        if (user.displayName != null) {
            displayName = (
                <Text style={styles.textStyle}>{this.props.user.displayName}</Text>

            )
        } else {
            displayName = (
                <Text style={styles.textStyle}>Usuário</Text>
            )
        }


        levelView = (
            <View style={{ width: window_width * 0.8, height: '100%', alignItems: 'center', justifyContent: 'space-evenly', borderColor: 'white', borderWidth: 2, borderRadius: 10 }}>
                <Progress.Circle progress={this.state.percent} size={100} showsText={true} color='white' />
                <Text style={styles.textStyle}>Próximo nível</Text>
            </View>
        )
        userReportsView1 = (
            <View style={{ width: window_width * 0.8, height: '100%', alignItems: 'center', borderColor: 'white', borderWidth: 2, borderRadius: 10 }}>
                <View style={{ width: '100%', height: '20%', justifyContent: 'center' }}>
                    <Text style={{ ...styles.gameText, fontSize: 20 }}>Meus reports</Text>
                </View>
                <View style={{ width: '100%', height: '80%' }}>
                    <View style={{ width: '100%', height: '50%', flexDirection: 'row' }}>
                        <View style={styles.userReport}>
                            <Text style={styles.gameText}>Nota média</Text>
                            <Text style={{ ...styles.gameText, fontSize: 18 }}>{this.state.userReports.rating ? this.state.userReports.rating : '0'}</Text>
                        </View>
                        <View style={styles.userReport}>
                            <Text style={styles.gameText}>Muito cheio</Text>
                            <Text style={{ ...styles.gameText, fontSize: 18 }}>{this.state.userReports.crowded ? this.state.userReports.crowded : '0'}</Text>
                        </View>
                    </View>

                    <View style={{ width: '100%', height: '50%', flexDirection: 'row' }}>
                        <View style={styles.userReport}>
                            <Text style={styles.gameText}>Sujo</Text>
                            <Text style={{ ...styles.gameText, fontSize: 18 }}>{this.state.userReports.dirty ? this.state.userReports.dirty : '0'}</Text>
                        </View>
                        <View style={styles.userReport}>
                            <Text style={styles.gameText}>Não passou</Text>
                            <Text style={{ ...styles.gameText, fontSize: 18 }}>{this.state.userReports.noBus ? this.state.userReports.noBus : '0'}</Text>
                        </View>
                    </View>
                </View>

            </View>
        )

        userReportsView2 = (
            <View style={{ width: window_width * 0.8, height: '100%', alignItems: 'center', borderColor: 'white', borderWidth: 2, borderRadius: 10 }}>
                <View style={{ width: '100%', height: '20%', justifyContent: 'center' }}>
                    <Text style={{ ...styles.gameText, fontSize: 20 }}>Meus reports</Text>
                </View>
                <View style={{ width: '100%', height: '80%' }}>
                    <View style={{ width: '100%', height: '50%', flexDirection: 'row' }}>
                        <View style={styles.userReport}>
                            <Text style={styles.gameText}>Motorista imprudente</Text>
                            <Text style={{ ...styles.gameText, fontSize: 18 }}>{this.state.userReports.crazy ? this.state.userReports.crazy : '0'}</Text>
                        </View>
                        <View style={styles.userReport}>
                            <Text style={styles.gameText}>Acidente</Text>
                            <Text style={{ ...styles.gameText, fontSize: 18 }}>{this.state.userReports.crash ? this.state.userReports.crash : '0'}</Text>
                        </View>
                    </View>

                    <View style={{ width: '100%', height: '50%', flexDirection: 'row' }}>
                        <View style={styles.userReport}>
                            <Text style={styles.gameText}>Pneu furado</Text>
                            <Text style={{ ...styles.gameText, fontSize: 18 }}>{this.state.userReports.flattire ? this.state.userReports.flattire : '0'}</Text>
                        </View>
                        <View style={styles.userReport}>
                            <Text style={styles.gameText}>Problema mecânico</Text>
                            <Text style={{ ...styles.gameText, fontSize: 18 }}>{this.state.userReports.engine ? this.state.userReports.engine : '0'}</Text>
                        </View>
                    </View>
                </View>

            </View >
        )

        if (this.state.editUsername) {
            username = (
                <View style={{ flexDirection: 'row' }}>
                    <TextInput
                        underlineColorAndroid='white'
                        placeholder='Novo nome'
                        style={{ color: 'white', fontFamily: 'Decker' }}
                        value={this.state.newName}
                        onChangeText={(val) => val.length < 15 ? this.setState({ newName: val }) : null}
                        placeholderTextColor='white'
                        multiline={false}

                    />
                    <TouchableWithoutFeedback onPress={() => { this.updateUsername() }}>
                        <Icon size={25} name='check' color='white' />
                    </TouchableWithoutFeedback>
                </View>

            )
        } else {
            username = (
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ ...styles.textStyle, fontSize: 18, paddingRight: 5 }}>{this.state.username}</Text>
                    <TouchableWithoutFeedback onPress={() => { 
                        ToastAndroid.show('Este é o seu nome que aparecerá no Spotted Buzee', ToastAndroid.LONG);
                        this.setState({ editUsername: true }) 
                        }}>
                        <Icon size={25} name='edit' color='white' />
                    </TouchableWithoutFeedback>
                </View>

            )
        }





        return (

            <View style={styles.screen}>

                <View style={styles.userContainer}>
                    <View style={{ position: 'absolute', left: 10, top: 10, padding: 10 }}>
                        <TouchableWithoutFeedback onPress={this.close}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </TouchableWithoutFeedback>
                    </View>
                    <View style={{ position: 'absolute', top: 10, right: 10, padding: 10 }}>
                        <TouchableWithoutFeedback onPress={this.showTutorial} >
                            <Icon size={30} name='question-circle' color='white' />
                        </TouchableWithoutFeedback>
                    </View>
                    {userImage}
                    <View style={{ alignItems: 'center' }}>
                        {displayName}
                        {username}
                        <Text style={{ ...styles.textStyle, fontSize: 18 }}>{'Nível ' + this.state.level}</Text>
                    </View>

                </View>
                <View style={styles.levelContainer}>

                    <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={[levelView, userReportsView1, userReportsView2]}
                        renderItem={({ item, index }) => (
                            item

                        )}
                        containerCustomStyle={{ flexGrow: 0 }}
                        sliderWidth={window_width}
                        itemWidth={window_width * 0.8}

                    />
                </View>
                <View style={styles.couponContainer}>
                    {renderList}
                </View>

            </View >

        )


    }

}


const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    userContainer: {
        height: '40%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    levelContainer: {
        height: '30%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    couponContainer: {
        height: '30%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 150,
        height: 150,
        borderRadius: 100,
        borderWidth: 3,
        borderColor: "white"
    },
    textStyle: {
        color: 'white',
        // alignItems: 'center',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    gameText: {
        color: 'white',
        // alignItems: 'center',
        fontSize: 16,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    userReport: {
        width: '50%',
        height: '100%',
        justifyContent: 'center'
    }

});

const mapStateToProps = state => {
    return {
        user: state.user.user,
        firebaseRef: state.firebaseRef

    }
}
export default connect(mapStateToProps, null)(ProfileScreen);