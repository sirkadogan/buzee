import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    ScrollView,
    FlatList,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';

import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Divider } from 'react-native-elements';


class ProfileTutorial extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);
        this.state = {
            percent: 0,
            level: 1,
            coupons: [],
            revealed: false,
            selectedCoupon: null,
            tutorial: null
        };
    }


    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    render() {

        let content = 'Você acumula pontos para cada quilômetro em que compartilha sua posição. Quando juntar pontos suficientes, você será premiado com um voucher. \n'
        let content2 = 'Estes vouchers oferecem desconto em qualquer oferta existente no Polvo Ofertas. Basta adicionar a oferta ao seu carrinho e aplicar o voucher!\n';
        let content3 = 'E não é um simples desconto! É um desconto em cima do valor da oferta do Polvo Ofertas! \n'
        let content4 = 'O seu desconto é de 5% sobre o valor integral ou 30% sobre o valor parcial.\n'
        let content5 = 'O desconto integral é quando o valor inteiro é pago através da plataforma do Polvo Ofertas. Desconto parcial é quando você paga uma parte pelo site e outra no estabelecimento.\n'

        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Ajuda</Text>
                </View>



                <View style={styles.middleContainer}>

                    <ScrollView >
                        <Text style={{...styles.titleStyle, fontFamily: 'Barlow-BoldItalic', marginBottom: 20}}>Como funcionam os cupons?</Text>
                        <Text style={styles.textStyle}>{content}</Text>
                        <Text style={styles.textStyle}>{content2}</Text>
                        <Text style={styles.textStyle}>{content3}</Text>
                        <Text style={{...styles.titleStyle, fontFamily: 'Barlow-BoldItalic', marginBottom: 20}}>De quanto é o meu desconto?</Text>
                        <Text style={styles.textStyle}>{content4}</Text>
                        <Text style={styles.textStyle}>{content5}</Text>
                    </ScrollView>

                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

export default (ProfileTutorial);