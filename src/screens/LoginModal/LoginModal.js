import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    ActivityIndicator,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Text,
    Image,
    Keyboard,
    AsyncStorage
} from 'react-native';


import DefaultInput from '../../components/UI/DefaultInput/DefaultInput';
import EnabledLoginButton from '../../assets/EnabledLoginButton.png';
import DisabledLoginButton from '../../assets/DisabledLoginButton.png';
import validate from '../../utility/validation';
// import * as firebase from 'firebase';
import firebase from "react-native-firebase";
import startMainTabs from '../../screens/MainTabs/startMainTabs/';

import { connect } from 'react-redux';
import { tryAuth, authAutoSignIn, loginFacebook, storeUser, storeStops } from '../../store/actions/index'
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

class LoginModal extends Component {

    state = {
        authMode: 'sign up',
        loginWithEmail: false,
        emailMissing: false,
        passwordMissing: false,
        confirmPasswordMissing: false,
        controls: {
            email: {
                value: '',
                valid: false,
                validationRules: {
                    isEmail: true
                },
                touched: false

            },
            password: {
                value: '',
                valid: false,
                validationRules: {
                    minLength: 6
                },
                touched: false
            },
            confirmPassword: {
                value: '',
                valid: false,
                validationRules: {
                    equalTo: 'password'
                },
                touched: false
            }

        }
    }

    getStops = () => {
        return new Promise((resolve, reject) => {
            console.log('Pegando as paradas na tela de autenticação')
            firebase.database().ref().child('UniFxV' + '/' + 'Bairro ► Centro' + '/P').once('value').then(snapshot => {
                let stopMarkers = [];

                snapshot.forEach(childSnapshot => {
                    let childKey = childSnapshot.key;
                    let childData = childSnapshot.val();

                    stopMarkers.push({ identifier: childKey, latitude: childData.la, longitude: childData.lo, notifyOnEntry: true, notifyOnExit: true, notifyOnDwell: false });
                });
                console.log('Pegou paradas')
                this.props.onStoreStops(stopMarkers);
                return stopMarkers
            }).then((stopMarkers) => {
                AsyncStorage.setItem('buzee:UniFxV/Bairro ► Centro/P', JSON.stringify(stopMarkers)).then(() => {
                    console.log('salvou paradas na memória na tela de autenticação')
                    firebase.database().ref().child('UniFxV' + '/' + 'Bairro ► Centro' + '/TS').once('value').then(snapshot => {
                        let timeStamp = snapshot.val();
                        console.log('Pegou o TS do servidor: ', timeStamp);
                        return timeStamp
                    }).then((timeStamp) => {
                        AsyncStorage.setItem('buzee:UniFxV/Bairro ► Centro/TS', timeStamp.toString()).then(() => {
                            console.log('Setou TS na memoria')
                            resolve();
                        })
                    })
                })

            })
        })
    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return {
                authMode: prevState.authMode === 'login' ? 'sign up' : 'login'
            };
        })
    };

    authHandler = () => {
        const authData = {
            email: this.state.controls.email.value,
            password: this.state.controls.password.value
        };
        // this.props.onTryAuth(authData, this.state.authMode);
        if (this.state.authMode === 'sign up') {
            this.props.firebaseRef.firebaseRef.auth().createUserWithEmailAndPassword(authData.email, authData.password).catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert('Algo deu errado')
                // ...
            });
        } else {
            this.props.firebaseRef.firebaseRef.auth().signInWithEmailAndPassword(authData.email, authData.password).catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert('Algo deu errado')
                // ...
            });
        }
        this.close();

    }

    updateInputState = (key, value) => {
        let connectedValue = {};
        if (this.state.controls[key].validationRules.equalTo) {
            const equalControl = this.state.controls[key].validationRules.equalTo
            const equalValue = this.state.controls[equalControl].value;
            connectedValue = {
                ...connectedValue,
                equalTo: equalValue
            };
        }
        if (key === 'password') {
            connectedValue = {
                ...connectedValue,
                equalTo: value
            };
        }
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    confirmPassword: {
                        ...prevState.controls.confirmPassword,
                        valid: key === 'password'
                            ? validate(
                                prevState.controls.confirmPassword.value,
                                prevState.controls.confirmPassword.validationRules,
                                connectedValue
                            )
                            : prevState.controls.confirmPassword.valid
                    },
                    [key]: {
                        ...prevState.controls[key],
                        value: value,
                        valid: validate(value, prevState.controls[key].validationRules, connectedValue),
                        touched: true
                    },

                }
            };
        })
    }

    forgotPassword = (email) => {
        if (!this.state.controls.email.valid) {
            this.setState((prevState) => {
                return {
                    emailMissing: true,
                }

            })
        } else {
            firebase.auth().sendPasswordResetEmail(email).then(function () {
                Navigation.showSnackbar({
                    text: 'Um email de redefinição de senha foi enviado',
                    duration: 'long'
                })
                Navigation.dismissModal(this.props.componentId)
                Keyboard.dismiss();
            }).catch(function (error) {
                // An error happened.
            });
        }
    }

    showEmptyFields = () => {
        if (!this.state.controls.email.valid) {
            this.setState((prevState) => {
                return {
                    emailMissing: true,
                }

            })
        }
        if (!this.state.controls.password.valid) {
            this.setState((prevState) => {
                return {
                    passwordMissing: true,
                }
            })
        }
        if (!this.state.controls.confirmPassword.valid) {
            this.setState((prevState) => {
                return {
                    confirmPasswordMissing: true,
                }
            })
        }
    }

    close = () => {

        Navigation.dismissModal(this.props.componentId)
    }

    render() {
        let sideButton = null;
        let emailButton = null;
        let confirmPasswordControl = null;
        let submitButton = null;

        if (!this.state.controls.confirmPassword.valid && this.state.authMode === 'sign up'
            || !this.state.controls.email.valid
            || !this.state.controls.password.valid) {
            submitButton = (

                <TouchableWithoutFeedback onPress={this.showEmptyFields}>
                    <View style={{ elevation: 2, backgroundColor: '#0854A2', borderRadius: 4, ...styles.buttonGroup, borderColor: 'white', borderWidth: 1, justifyContent: 'center' }}>
                        <Text style={styles.textStyle}>Confirmar</Text>
                    </View>
                </TouchableWithoutFeedback>
            );
        } else {
            submitButton = (

                <TouchableWithoutFeedback onPress={this.authHandler}>
                    <View style={{ elevation: 2, backgroundColor: '#0854A2', borderRadius: 4, ...styles.buttonGroup, borderColor: 'white', borderWidth: 1, justifyContent: 'center' }}>
                        <Text style={styles.textStyle}>Confirmar</Text>
                    </View>
                </TouchableWithoutFeedback>
            );
        }


        if (this.state.authMode === 'sign up') {
            confirmPasswordControl = (

                <DefaultInput
                    placeholder='Confirme sua senha'
                    style={this.state.confirmPasswordMissing ? styles.invalid : styles.input}
                    value={this.state.controls.confirmPassword.value}
                    onChangeText={(val) => this.updateInputState('confirmPassword', val)}
                    valid={this.state.controls.confirmPassword.valid}
                    touched={this.state.controls.confirmPassword.touched}
                    secureTextEntry
                    fontFamily='Decker' />

            );
            sideButton = (
                <TouchableWithoutFeedback onPress={this.switchAuthModeHandler}>
                    <View>
                        <Text style={{ ...styles.textStyle, fontSize: 18 }}>
                            Já possuo conta
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            );
            topText = (
                <Text style={styles.textStyle}>
                    Crie sua conta
                </Text>
            );

        } else {
            sideButton = (
                <TouchableWithoutFeedback onPress={() => this.forgotPassword(this.state.controls.email.value)}>
                    <View>
                        <Text style={{ ...styles.textStyle, fontSize: 18 }}>
                            Esqueci minha senha
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            );
            topText = (
                <Text style={styles.textStyle}>
                    Login
                </Text>
            );
        }

        if (this.props.isLoading) {
            submitButton = <ActivityIndicator />
        }


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                        <TouchableWithoutFeedback onPress={this.close}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </TouchableWithoutFeedback>
                    </View>




                </View>
                <View style={styles.middleContainer}>

                    {topText}
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <DefaultInput
                            placeholder='Email'
                            style={this.state.emailMissing ? styles.invalid : styles.input}
                            value={this.state.controls.email.value}
                            onChangeText={(val) => this.updateInputState('email', val)}
                            valid={this.state.controls.email.valid}
                            touched={this.state.controls.email.touched}
                            autoCapitalize='none'
                            autoCorrect={false}
                            keyboardType='email-address'
                            fontFamily='Decker'
                        />
                        <DefaultInput
                            placeholder='Senha'
                            style={this.state.passwordMissing ? styles.invalid : styles.input}
                            value={this.state.controls.password.value}
                            onChangeText={(val) => this.updateInputState('password', val)}
                            valid={this.state.controls.password.valid}
                            touched={this.state.controls.password.touched}
                            secureTextEntry
                            fontFamily='Decker'
                        />
                        {confirmPasswordControl}
                    </View>


                    <View style={{ alignSelf: 'flex-end', margin: 10 }}>
                        {sideButton}
                    </View>
                    {submitButton}
                </View>


                <View style={styles.bottomContainer}>

                </View>
            </View >
            // <View style={{ flex: 1 }}>
            //     <TouchableWithoutFeedback onPress={() => {
            //         this.close()
            //     }}>
            //         <View style={{ flex: 1 }}>

            //         </View>
            //     </TouchableWithoutFeedback>
            //     <View style={styles.screen}>
            //         {/* <TouchableWithoutFeedback onPress={() => {alert('toque')}}> */}
            //         <View style={styles.container}>


            //     <View style={styles.inputContainer}>
            //         {topText}
            //         <DefaultInput
            //             placeholder='Email'
            //             style={this.state.emailMissing ? styles.invalid : styles.input}

            //             value={this.state.controls.email.value}
            //             onChangeText={(val) => this.updateInputState('email', val)}
            //             valid={this.state.controls.email.valid}
            //             touched={this.state.controls.email.touched}
            //             autoCapitalize='none'
            //             autoCorrect={false}
            //             keyboardType='email-address' />
            //         <DefaultInput
            //             placeholder='Senha'
            //             style={this.state.passwordMissing ? styles.invalid : styles.input}
            //             value={this.state.controls.password.value}
            //             onChangeText={(val) => this.updateInputState('password', val)}
            //             valid={this.state.controls.password.valid}
            //             touched={this.state.controls.password.touched}
            //             secureTextEntry />
            //         {confirmPasswordControl}
            //     </View>
            // <View style={styles.sideButton}>
            //     {sideButton}
            // </View>
            // <View style={{ alignItems: 'center' }}>
            //     {submitButton}
            // </View>

            // </View>
            //         {/* </TouchableWithoutFeedback> */}
            //     </View>
            // </View>



        )
    }
}
const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    buttonGroup: {
        width: '80%',
        height: '10%',
        elevation: 2

    },
    portraitPasswordContainer: {
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    portraitPasswordWrapper: {
        width: '100%',
    },
    sideButton: {
        alignItems: 'flex-end',
        margin: 5
    },
    input: {
        width: '80%',
        height: 60,
        borderRadius: 5,
        backgroundColor: 'white',
    },
    invalid: {
        width: '80%',
        height: 60,
        borderRadius: 5,
        backgroundColor: '#f9c0c0',
        borderColor: 'red'
    }
});


const mapStateToProps = state => {
    return {
        isLoading: state.ui.isLoading,
        firebaseRef: state.firebaseRef
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onTryAuth: (authData, authMode) => dispatch(tryAuth(authData, authMode)),
        onAutoSignIn: () => dispatch(authAutoSignIn()),
        onLoginFacebook: () => dispatch(loginFacebook()),
        onStoreUser: (user) => dispatch(storeUser(user)),
        onStoreStops: (stopMarkers) => dispatch(storeStops(stopMarkers)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);

