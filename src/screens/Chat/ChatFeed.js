import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    FlatList,
    TouchableWithoutFeedback,
    ActivityIndicator
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import ChatPost from '../../components/ChatPost/ChatPost'
import { connect } from 'react-redux';

//Tela base do spotted buzee, com o feed de componentes chat post

class SideContato extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }

    constructor(props) {
        super(props);

    }

    state = {
        initialLoading: true,
        loading: false,
        posts: [],
        loadedPosts: 0,
        postsToLoad: 5,
        hasScrolled: false,
        isListEnd: false,
        isFetching: false,
        username: 'buzeer',
        feedRef: 'postFeed'
    }

    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef //Referencia ao firebse

        this.referenceToOldestKey = null; //Referencia global ao post mais antigo
        this.referenceToMyOldestPost = null; //Referencia global ao MEU post mais antigo
        this.getPosts(); //Busca os posts no firebase
    }

    onScroll = () => {
        this.setState({ hasScrolled: true })
    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    getPosts = () => {
        let referenceToOldestKey = this.referenceToOldestKey;
        if (!this.state.loading) {
            this.setState({
                loading: true
            })
            if (!referenceToOldestKey) { // if initial fetch

                this.firebaseRef.database()
                    .ref('posts/postFeed')
                    .orderByKey()
                    .limitToLast(10)
                    .once('value')
                    .then((snapshot) => {
                        // changing to reverse chronological order (latest first)
                        if (snapshot.exists()) {
                            let arrayOfKeys = Object.keys(snapshot.val())
                                .sort()
                                .reverse();
                            // transforming to array
                            let results = arrayOfKeys
                                .map((key) => {
                                    return [key, snapshot.val()[key]]
                                });

                            if (results.length > 0) {
                                // storing reference
                                this.referenceToOldestKey = arrayOfKeys[arrayOfKeys.length - 1]
                                this.setState({
                                    posts: results,
                                    initialLoading: false,
                                    loading: false,
                                    hasScrolled: false,
                                    loadedPosts: results.length,
                                    isFetching: false
                                })
                            } else {
                                this.setState({
                                    isListEnd: true,
                                    initialLoading: false,
                                    loading: false,
                                    isFetching: false
                                })
                            }

                        } else {
                            this.setState({
                                isListEnd: true,
                                initialLoading: false,
                                loading: false,
                                isFetching: false
                            })
                        }

                    })

            } else {

                this.firebaseRef.database()
                    .ref('posts/postFeed')
                    .orderByKey()
                    .endAt(referenceToOldestKey)
                    .limitToLast(11)
                    .once('value')
                    .then((snapshot) => {
                        // changing to reverse chronological order (latest first)
                        // & removing duplicate
                        if (snapshot.exists()) {
                            let arrayOfKeys = Object.keys(snapshot.val())
                                .sort()
                                .reverse()
                                .slice(1);
                            // transforming to array
                            let results = arrayOfKeys
                                .map((key) => {
                                    return [key, snapshot.val()[key]]
                                });
                            // updating reference
                            if (results.length > 0) {
                                this.referenceToOldestKey = arrayOfKeys[arrayOfKeys.length - 1]
                                let posts = this.state.posts;
                                results = posts.concat(results)

                                this.setState({
                                    posts: results,
                                    loading: false,
                                    hasScrolled: false,
                                    loadedPosts: results.length,
                                    isFetching: false,
                                    initialLoading: false
                                })
                            } else {
                                this.setState({
                                    isListEnd: true,
                                    loading: false,
                                    isFetching: false
                                })
                            }

                        } else {
                            this.setState({
                                isListEnd: true,
                                loading: false,
                                isFetching: false
                            })
                        }
                    })
            }
        }
    }

    //função para buscar posts feitos pelo próprio usuário
    getMyPosts = () => {
        let referenceToMyOldestPost = this.referenceToMyOldestPost;
        if (!this.state.loading) {
            this.setState({
                loading: true
            })
            if (!referenceToMyOldestPost) { // if initial fetch
                this.firebaseRef.database()
                    .ref('posts/myPosts/' + this.props.user.uid)
                    .orderByKey()
                    .limitToLast(10)
                    .once('value')
                    .then((snapshot) => {
                        // changing to reverse chronological order (latest first)
                        if (snapshot.exists()) {
                            let arrayOfKeys = Object.keys(snapshot.val())
                                .sort()
                                .reverse();
                            // transforming to array
                            let results = arrayOfKeys
                                .map((key) => {
                                    return [key, snapshot.val()[key]]
                                });
                            // storing reference
                            if (results.length > 0) {
                                this.referenceToMyOldestPost = arrayOfKeys[arrayOfKeys.length - 1]
                                let posts = []
                                new Promise((resolve) => {
                                    results.forEach((myPostRef) => {
                                        let postKey = myPostRef[0];
                                        this.firebaseRef.database()
                                            .ref('posts/postFeed/' + postKey)
                                            .once('value')
                                            .then((snapshot) => {
                                                let post = snapshot.val();

                                                posts.push([snapshot.key, post])


                                            }).then(() => {
                                                if (posts.length >= results.length) {
                                                    resolve()
                                                }
                                            })
                                    })
                                }).then(() => {
                                    this.setState({
                                        posts: posts,
                                        initialLoading: false,
                                        loading: false,
                                        hasScrolled: false,
                                        loadedPosts: results.length,
                                        isFetching: false
                                    })
                                })

                            } else {
                                this.setState({
                                    isListEnd: true,
                                    initialLoading: false,
                                    loading: false,
                                    isFetching: false
                                })
                            }

                        } else {
                            this.setState({
                                isListEnd: true,
                                initialLoading: false,
                                loading: false,
                                isFetching: false
                            })
                        }

                    })

            } else {
                this.firebaseRef.database()
                    .ref('posts/myPosts/' + this.props.user.uid)
                    .orderByKey()
                    .endAt(referenceToMyOldestPost)
                    .limitToLast(11)
                    .once('value')
                    .then((snapshot) => {
                        // changing to reverse chronological order (latest first)
                        // & removing duplicate
                        if (snapshot.exists()) {
                            let arrayOfKeys = Object.keys(snapshot.val())
                                .sort()
                                .reverse()
                                .slice(1);
                            // transforming to array
                            let results = arrayOfKeys
                                .map((key) => {
                                    return [key, snapshot.val()[key]]
                                });
                            // updating reference
                            if (results.length > 0) {
                                this.referenceToMyOldestPost = arrayOfKeys[arrayOfKeys.length - 1]
                                let posts = this.state.posts;
                                new Promise((resolve) => {
                                    results.forEach((myPostRef) => {
                                        let postKey = myPostRef[0];
                                        this.firebaseRef.database()
                                            .ref('posts/postFeed/' + postKey)
                                            .once('value')
                                            .then((snapshot) => {
                                                let post = snapshot.val();
                                                posts.push([snapshot.key, post])
                                            }).then(() => {
                                                if (posts.length >= results.length) {
                                                    resolve()
                                                }
                                            })
                                    })
                                }).then(() => {
                                    this.setState({
                                        posts: posts,
                                        initialLoading: false,
                                        loading: false,
                                        hasScrolled: false,
                                        loadedPosts: results.length,
                                        isFetching: false
                                    })
                                })
                            } else {
                                this.setState({
                                    isListEnd: true,
                                    loading: false,
                                    isFetching: false
                                })
                            }

                        } else {
                            this.setState({
                                isListEnd: true,
                                loading: false,
                                isFetching: false
                            })
                        }
                    })
            }
        }
    }

    renderFooter = () => {
        if (!this.state.loading) return null;
        return (
            <View style={styles.loading}>
                <ActivityIndicator size='large' color='white' />
            </View>
        );
    };

    //Atualiza lista ao puxar para baixo
    onRefresh = () => {
        this.referenceToOldestKey = null;
        this.referenceToMyOldestPost = null;
        if (this.state.feedRef == 'postFeed') {
            this.setState({
                isFetching: true,
                posts: [],
                initialLoading: true
            }, () => {
                this.getPosts();
            })
        } else {
            this.setState({
                isFetching: true,
                posts: [],
                initialLoading: true
            }, () => {
                this.getMyPosts();
            })
        }
    }


    openCreatePostScreen = () => {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'buzee.CreatePostScreen',
                        passProps: {
                            refreshPosts: this.onRefresh
                        }

                    }
                }]
            }
        });
    }

    // Toggle entre meus posts e feed geral
    showFeed = () => {
        if (this.state.feedRef !== 'postFeed') {
            this.referenceToOldestKey = null;
            this.setState({
                isFetching: true,
                posts: [],
                initialLoading: true,
                feedRef: 'postFeed'
            }, () => {
                this.getPosts();
            })
        }
    }

    showMyPosts = () => {

        if (this.state.feedRef !== 'myPosts') {
            this.referenceToMyOldestPost = null;
            this.setState({
                isFetching: true,
                posts: [],
                initialLoading: true,
                feedRef: 'myPosts'
            }, () => {
                this.getMyPosts();
            })
        }
    }

    
    render() {
        if (this.state.initialLoading) {
            middleContent = (
                <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='large' color='white' />
                </View>
            )
        } else {
            if (this.state.posts.length < 1) {
                middleContent = (
                    <Text style={styles.textStyle}>{'Ninguém postou nada ainda :( \n Que tal ser o primeiro a falar algo?'}</Text>
                )
            } else {
                middleContent = (
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isFetching}
                        ListFooterComponent={this.renderFooter}
                        onEndReachedThreshold={0.1}
                        extraData={this.state.posts}
                        onEndReached={this.state.isListEnd ? null : this.getPosts}
                        onScroll={this.onScroll}
                        data={this.state.posts}
                        style={{ width: '100%' }}
                        renderItem={({ item, index }) => (
                            <View style={{ width: '100%', flex: 1 }}>
                                <ChatPost
                                    keyRef={item[0]}
                                    text={item[1].text}
                                    score={item[1].score}
                                    author={item[1].author}
                                    timestamp={item[1].timestamp}
                                    comments={item[1].comments}
                                    uid={item[1].user}
                                    refresh={this.onRefresh}
                                    oficial={item[1].oficial}
                                />
                            </View>


                        )}
                    />
                )
            }

        }

        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '50%', width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='white' />
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={this.openCreatePostScreen}>
                            <View style={{ position: 'absolute', right: 0, padding: 10 }}>
                                <Icon size={30} name='plus' color='white' />
                            </View>
                        </TouchableWithoutFeedback>
                        <Text style={styles.titleStyle}>Spotted Buzee</Text>

                    </View>
                    <View style={{ height: '50%', width: '100%', alignItems: 'center', flexDirection: 'row' }}>
                        <TouchableWithoutFeedback onPress={this.showFeed}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.textStyle}>Feed</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={this.showMyPosts}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.textStyle}>Meus Posts</Text>
                            </View>
                        </TouchableWithoutFeedback>

                    </View>

                </View>



                <View style={styles.middleContainer}>
                    {middleContent}
                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    loading: {
        alignSelf: 'center',
        marginVertical: 20,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 18,
        color: 'white',
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    topContainer: {
        height: '20%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '75%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef,
        user: state.user.user
    }
}


export default connect(mapStateToProps)(SideContato);
