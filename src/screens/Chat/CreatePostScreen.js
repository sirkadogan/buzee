import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Picker,
    Text,
    Image,
    Button,
    FlatList,
    TextInput,
    ScrollView,
    ToastAndroid,


} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import BackgroundGeolocation from "react-native-background-geolocation";
import firebase from "react-native-firebase";
import { ButtonGroup, Divider, SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';

class CreatePostScreen extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        remainingChars: 140,
        story: []
    }

    componentWillMount = () => {
        this.firebaseRef = this.props.firebaseRef.firebaseRef
        this.firebaseRef.database().ref('users/' + this.props.user.uid + '/username').once('value').then((snapshot) => {
            this.setState({ username: snapshot.val() })
        })
    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    updateStory = (text) => {
        let maxLength = 140
        let remainingChars = maxLength - text.length;
        if (remainingChars > -1) {
            this.setState(prevState => {
                return {
                    story: text,
                    remainingChars: remainingChars
                }
            })
        }
    }

    sendStory = () => {
        if (this.state.story.length > 0) {
            let content = this.state.story
            this.firebaseRef.database().ref().child('posts/postFeed').push({
                author: this.state.username,
                timestamp: Date.now(),
                text: content,
                score: 0,
                comments: 0,
                user: this.props.user.uid
            }).then((snapshot) => {
                let pushedKey = snapshot.key;
                this.firebaseRef.database().ref().child('posts/myPosts/' + this.props.user.uid + '/' + pushedKey).set(true);
                this.props.refreshPosts();
            })
            this.close();
        } else {
            ToastAndroid.show('Texto vazio!', ToastAndroid.SHORT);
        }

    }



    render() {


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <View style={{ alignItems: 'center', height: '100%', width: '100%', flexDirection: 'row', justifyContent: 'space-evenly' }}>

                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='#EFE4CF' />
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ width: '80%' }}>
                            <Text style={styles.titleStyle}>Criar Post</Text>
                        </View>

                    </View>


                </View>


                <View style={styles.middleContainer}>
                    <View style={{ alignSelf: 'flex-end' }}>
                        <Text style={styles.textStyle}>{this.state.remainingChars + '/140'}</Text>
                    </View>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.textInput}
                        placeholder='Qual sua história hoje?'
                        value={this.state.story}
                        onChangeText={(val) => this.updateStory(val)}
                        placeholderTextColor='white'
                        multiline={true}

                    />





                </View>

                <View style={styles.bottomContainer}>
                    <TouchableWithoutFeedback onPress={this.sendStory}>
                        <View style={styles.buttonGroup}>
                            <Text style={styles.textStyle}>Enviar</Text>
                        </View>
                    </TouchableWithoutFeedback>

                </View>

            </View >

        )
    }





}

const window_height = Dimensions.get('window').height;
const window_width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '80%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        padding: 10
    },
    bottomContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    screen: {
        flex: 1,
        backgroundColor: '#7B3E63'
    },
    buttonGroup: {
        elevation: 4,
        backgroundColor: '#5E9BD4',
        borderRadius: 4,
        borderColor: 'white',
        borderWidth: 1,
        justifyContent: 'center',
        height: '80%',
        width: '80%'
    },
    textInput: {
        width: '90%',
        fontFamily: 'BarlowCondensed-Light',
        color: 'white',
        fontSize: 20,
    }
});

const mapStateToProps = state => {
    return {
        user: state.user.user,
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(CreatePostScreen);

