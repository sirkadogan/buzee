import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    FlatList,
    TouchableWithoutFeedback,
    ActivityIndicator,
    TextInput,
    Keyboard
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import ChatComment from '../../components/ChatPost/ChatComment'
import ChatPost from '../../components/ChatPost/ChatPost'
import { connect } from 'react-redux';

class PostDetailScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }

    constructor(props) {
        super(props);

    }

    state = {
        initialLoading: true,
        loading: false,
        comments: [],
        loadedcomments: 0,
        commentsToLoad: 5,
        hasScrolled: false,
        isListEnd: false,
        isFetching: false,
        username: 'buzeer',
        newComment: '',
        onDetailScreen: true
    }

    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef
        this.referenceToOldestKey = null;
        this.firebaseRef.database().ref('users/' + this.props.user.uid + '/username').once('value').then((snapshot) => {
            this.setState({ username: snapshot.val() })
        })
        this.getComments();
    }

    onScroll = () => {
        this.setState({ hasScrolled: true })
    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    getComments = () => {
        let referenceToOldestKey = this.referenceToOldestKey;
        if (!this.state.loading) {
            this.setState({
                loading: true
            })
            if (!referenceToOldestKey) { // if initial fetch

                this.firebaseRef.database().ref('posts/postComments/' + this.props.post.keyRef)
                    .orderByKey()
                    .limitToLast(10)
                    .once('value')
                    .then((snapshot) => {
                        // changing to reverse chronological order (latest first)
                        if (snapshot.exists()) {
                            let arrayOfKeys = Object.keys(snapshot.val())
                                .sort()
                                .reverse();
                            // transforming to array
                            let results = arrayOfKeys
                                .map((key) => {
                                    console.log([key, snapshot.val()[key]])
                                    return [key, snapshot.val()[key]]
                                });
                            // let data1 = results ? Object.values(results) : []
                            // storing reference
                            if (results.length > 0) {
                                this.referenceToOldestKey = arrayOfKeys[arrayOfKeys.length - 1]
                                this.setState({
                                    comments: results,
                                    initialLoading: false,
                                    loading: false,
                                    hasScrolled: false,
                                    loadedComments: results.length,
                                    isFetching: false
                                })
                            } else {
                                this.setState({
                                    isListEnd: true,
                                    initialLoading: false,
                                    loading: false,
                                    isFetching: false
                                })
                            }

                        } else {
                            this.setState({
                                isListEnd: true,
                                initialLoading: false,
                                loading: false,
                                isFetching: false
                            })
                        }

                    })

            } else {

                this.firebaseRef.database().ref('posts/postComments/' + this.props.post.keyRef)
                    .orderByKey()
                    .endAt(referenceToOldestKey)
                    .limitToLast(11)
                    .once('value')
                    .then((snapshot) => {
                        // changing to reverse chronological order (latest first)
                        // & removing duplicate

                        if (snapshot.exists()) {
                            let arrayOfKeys = Object.keys(snapshot.val())
                                .sort()
                                .reverse()
                                .slice(1);
                            // transforming to array
                            let results = arrayOfKeys
                                .map((key) => {
                                    return [key, snapshot.val()[key]]
                                });
                            // updating reference
                            if (results.length > 0) {
                                this.referenceToOldestKey = arrayOfKeys[arrayOfKeys.length - 1]
                                let comments = this.state.comments;
                                results = comments.concat(results)

                                this.setState({
                                    comments: results,
                                    loading: false,
                                    hasScrolled: false,
                                    loadedcomments: results.length,
                                    isFetching: false
                                })
                            } else {
                                this.setState({
                                    isListEnd: true,
                                    loading: false,
                                    isFetching: false
                                })
                            }

                        } else {
                            this.setState({
                                isListEnd: true,
                                loading: false,
                                isFetching: false
                            })
                        }


                    })

            }
        }


    }

    renderFooter = () => {
        if (!this.state.loading) return null;
        return (
            <View style={styles.loading}>
                <ActivityIndicator size='large' color='white' />
            </View>
        );
    };

    onRefresh = () => {
        this.referenceToOldestKey = null;
        this.setState({
            isFetching: true,
            comments: [],
            initialLoading: true
        }, () => {
            this.getComments();
        })
    }

    submitComment = () => {
        if (this.state.newComment.length >= 1) {
            Keyboard.dismiss();
            let comment = this.state.newComment
            let content = {
                author: this.state.username,
                timestamp: Date.now(),
                text: comment,
                score: 0,
                comments: 0,
                user: this.props.user.uid
            }
            this.firebaseRef.database().ref().child('posts/postComments/' + this.props.post.keyRef).push(content).then((snapshot) => {
                let key = snapshot.key;
                let comments = this.state.comments;
                comments.unshift([key, content])
                this.setState({
                    newComment: [],
                    comments: comments
                })
                this.addCommentCount();
                // this.onRefresh();
            })
        }

    }

    renderHeader = () => {
        return (
            <View style={{ flex: 1, marginBottom: 25 }}>
                <ChatPost
                    keyRef={this.props.post.keyRef}
                    text={this.props.post.text}
                    score={this.props.score}
                    author={this.props.post.author}
                    timestamp={this.props.post.timestamp}
                    comments={this.props.post.comments}
                    onDetailScreen={this.state.onDetailScreen}
                    oficial = {this.props.post.oficial}
                />
                <View style={{ height: 2, width: '100%', backgroundColor: 'white', margin: 3 }}></View >
                <Text style={styles.titleStyle}>{'Comentários'}</Text>


            </View>

        );
    };
    listEmpty = () => {
        return (
            <View style={{ flex: 1, width: '100%', margin: 2 }}>
                <Text style={styles.textStyle}>{'Seja o primeira a comentar!'}</Text>
            </View>

        );
    };

    updateComment = (val) => {
        this.setState({
            newComment: val
        })
    }

    addCommentCount = () => {
        return this.firebaseRef.database().ref().child('posts/postFeed/' + this.props.post.keyRef + '/comments')
            .transaction((current_value) => {
                if (current_value) {
                    return (current_value || 0) + 1;
                } else {
                    return 1;
                }

            });
    }

    render() {
        // let header;
        // if (this.state.comments.length <= 0) {
        //     header = (
        //         <View style={{ width: '100%', flex: 1 }}>
        //             <ChatPost keyRef={this.props.post.keyRef} text={this.props.post.text} score={this.props.score} author={this.props.post.author} timestamp={this.props.post.timestamp} comments={this.props.post.comments} onDetailScreen={this.state.onDetailScreen} />
        //             <View style={{ height: 2, width: '100%', backgroundColor: 'white', margin: 3 }}></View >
        // <Text style={styles.titleStyle}>{'Comentários'}</Text>
        //         </View>
        //     )
        // }
        if (this.state.initialLoading) {
            comments = (
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='large' color='white' />
                </View>
            )
        } else {

            comments = (
                <FlatList
                    showsVerticalScrollIndicator={false}
                    onRefresh={() => this.onRefresh()}
                    refreshing={this.state.isFetching}
                    ListFooterComponent={this.renderFooter}
                    ListHeaderComponent={this.renderHeader}
                    ListEmptyComponent={this.listEmpty}
                    onEndReachedThreshold={0.1}
                    extraData={this.state.comments}
                    onEndReached={this.state.isListEnd ? null : this.getComments}
                    onScroll={this.onScroll}
                    data={this.state.comments}
                    // contentContainerStyle={{ flex: 1 }}
                    style={{ width: '100%' }}
                    renderItem={({ item, index }) => (
                        <View style={{ width: '100%', flex: 1 }}>
                            <ChatComment
                                postKey={this.props.post.keyRef}
                                commentKey={item[0]}
                                text={item[1].text}
                                score={item[1].score}
                                author={item[1].author}
                                timestamp={item[1].timestamp}
                                uid={item[1].user}
                                refresh={this.onRefresh}
                                oficial ={item[1].oficial}
                            />
                        </View>

                    )}
                />
            )


        }

        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>
                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Spotted Buzee</Text>
                </View>

                <View style={styles.middleContainer}>
                    {/* {header} */}
                    <View style={{ flex: 1, margin: 5, width: '100%' }}>
                        {comments}
                    </View>
                    <View style={{ flexDirection: 'row', padding: 5 }}>
                        <TextInput
                            placeholder='Digite aqui'
                            style={{ color: '#454546', fontFamily: 'Decker', backgroundColor: 'white', flex: 1, borderRadius: 10, height: 50 }}
                            value={this.state.newComment}
                            onChangeText={(val) => val.length < 140 && val.length >= 0 ? this.updateComment(val) : null}
                            placeholderTextColor='grey'
                            multiline={true}
                            onSubmitEditing={this.submitComment}

                        />
                        <TouchableWithoutFeedback onPress={this.submitComment}>
                            <View style={{ backgroundColor: 'white', marginLeft: 5, borderRadius: 50, height: 50, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                                <Icon size={30} name='chevron-right' color='#619bd3' />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>

                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    loading: {
        alignSelf: 'center',
        marginVertical: 20,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 18,
        color: 'white',
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        // justifyContent: 'flex-start',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef,
        user: state.user.user
    }
}


export default connect(mapStateToProps)(PostDetailScreen);
