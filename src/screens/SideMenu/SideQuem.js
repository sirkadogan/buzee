import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    ScrollView,
    FlatList,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';

import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Divider } from 'react-native-elements';


class SideQuem extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);

    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    render() {

        let content = 'A Buzee Mobilidade é uma startup criada em 2018 e incubada na Pulsar Incubadora da Universidade Federal de Santa Maria, sediada na Agência de Inovação e Transferência de Tecnologia da UFSM (Agittec).\n'
        let content2 = 'Formada por três engenheiros de controle e automação, a empresa tem como missão melhorar a qualidade do transporte coletivo com a participação da comunidade.\n'
        let content3 = 'Percebemos que a comunidade tem um grande potencial de criar mudança, mas não temos voz quando se trata dos serviços que nos fornecem.\n '
        let content4 = 'Acreditamos que com a participação de todos, possamos criar uma comunidade que se sustente a ajude uns aos outros, seja mostrando a localização dos ônibus ou reclamando sobre a situação através do Buzee. \n'


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>

                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Quem Somos</Text>
                </View>



                <View style={styles.middleContainer}>

                    <ScrollView contentContainerStyle={{ alignItems: 'center', justifyContent: 'space-evenly' }} >
                        <Text style={styles.textStyle}>{content}</Text>
                        <Text style={styles.textStyle}>{content2}</Text>
                        <Text style={styles.textStyle}>{content3}</Text>
                        <Text style={styles.textStyle}>{content4}</Text>
                    </ScrollView>

                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

export default (SideQuem);