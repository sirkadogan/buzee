import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    ScrollView,
    FlatList,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';

import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Divider } from 'react-native-elements';


class SideAjuda extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);

    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    openTutorial = () => {
        this.close();
        Navigation.setRoot({
            root: {
                stack: {
                    children: [{
                        component: {
                            name: 'buzee.TutorialScreen'
                        }
                    }]
                }
            }
        });
    }

    render() {

        let content = 'Nós respondemos a várias questões referentes a cada seção do aplicativo nas suas respectivas telas. Por exemplo, ao entrar na área "Compartilhar", você verá lá no cantinho de cima à direita o ícone abaixo, onde respondemos as dúvidas mais frequentes.\n'

        let content2 = 'Ahh, mas se não está claro o que cada ícone significa, dê uma revisadinha no tutorial:\n'
        let content3 = 'Se o seu problema/dúvida ainda persistir, não deixe de entrar em contato conosco através da aba "Contato" para que possamos lhe ajudar.\n'


        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>

                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Ajuda</Text>
                </View>



                <View style={styles.middleContainer}>

                    <ScrollView contentContainerStyle={{ alignItems: 'center', justifyContent: 'space-evenly' }} >
                        <Text style={{ ...styles.titleStyle, fontFamily: 'Barlow-BoldItalic', marginBottom: 20 }}>{'Como compartilhar?\nGasta muito dado?\nPor que não vejo minha linha?'}</Text>
                        <Text style={styles.textStyle}>{content}</Text>
                        <Text style={styles.textStyle}>{content2}</Text>
                        <TouchableWithoutFeedback onPress={this.openTutorial}>

                            <View style={{ borderRadius: 10, borderWidth: 3, borderColor: 'white', padding: 10, alignItems: 'center', justifyContent: 'center', width: '80%', marginBottom: 20 }}>
                                <Text style={{ ...styles.textStyle, fontSize: 22 }}>Revisar tutorial</Text>
                            </View>

                        </TouchableWithoutFeedback>

                        <Text style={styles.textStyle}>{content3}</Text>
                    </ScrollView>

                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

export default (SideAjuda);