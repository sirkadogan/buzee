import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    ScrollView,
    TouchableWithoutFeedback,
    Linking
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import email from 'react-native-email'

class SideContato extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);

    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    handleEmail = () => {
        const to = ['contato@buzee.com.br'] // string or array of email addresses
        email(to).catch(console.error)
    }

    openMessenger = () => {
        Linking.canOpenURL('https://www.facebook.com/buzeemob/')
            .then((supported) => {
                if (!supported) {
                    console.log("Can't handle url: " + 'https://www.facebook.com/buzeemob/');
                } else {
                    return Linking.openURL('https://www.facebook.com/buzeemob/');
                }
            })
            .catch((err) => console.error('An error occurred', err));

    }

    openInstagram = () => {
        Linking.canOpenURL('https://www.instagram.com/buzeemob/')
            .then((supported) => {
                if (!supported) {
                    console.log("Can't handle url: " + 'https://www.instagram.com/buzeemob/');
                } else {
                    return Linking.openURL('https://www.instagram.com/buzeemob/');
                }
            })
            .catch((err) => console.error('An error occurred', err));
    }

    openTwitter = () => {
        Linking.canOpenURL('https://twitter.com/Buzeemob')
            .then((supported) => {
                if (!supported) {
                    console.log("Can't handle url: " + 'https://twitter.com/Buzeemob');
                } else {
                    return Linking.openURL('https://twitter.com/Buzeemob');
                }
            })
    }



    render() {

        let content = 'Entre em contato conosco, vamos adorar ouvir sua opinião.\n'
        let content2 = 'ou pelas redes sociais:\n'
        return (
            <View style={styles.screen}>

                <View style={styles.topContainer}>
                    <TouchableWithoutFeedback onPress={this.close}>
                        <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                            <Icon size={30} name='chevron-left' color='white' />
                        </View>

                    </TouchableWithoutFeedback>
                    <Text style={styles.titleStyle}>Contato</Text>
                </View>



                <View style={styles.middleContainer}>

                    <View style={{ alignItems: 'center', justifyContent: 'space-evenly', width:'100%', height:'100%' }} >
                        <Text style={{...styles.titleStyle, fontFamily: 'Barlow-BoldItalic'}}>{'Tem alguma ideia? \n Sugestão? \n Reclamação?\n'}</Text>
                        <Text style={styles.textStyle}>{content}</Text>
                        <TouchableWithoutFeedback onPress={this.handleEmail}>
                            <View style={{ borderRadius: 10, borderWidth: 3, borderColor: 'white', padding: 10, alignItems: 'center', justifyContent: 'center', width: '80%', marginBottom: 20 }}>
                                <Text style={{ ...styles.textStyle, fontSize: 22 }}>Enviar email</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <Text style={styles.textStyle}>{content2}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', width: '80%' }}>
                            <TouchableWithoutFeedback onPress={() => this.openMessenger()}>
                                <Icon name='facebook-f' size={40} color='white' />
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => this.openInstagram()}>
                                <Icon name='instagram' size={40} color='white' />
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => this.openTwitter()}>
                                <Icon name='twitter' size={40} color='white' />
                            </TouchableWithoutFeedback>


                        </View>
                    </View>

                </View>



                <View style={styles.bottomContainer}>
                </View>

            </View >


        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'Decker',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'BarlowCondensed-Light',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        backgroundColor: '#5E9BD4',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});

export default (SideContato);