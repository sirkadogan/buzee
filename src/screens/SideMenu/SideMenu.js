import React from 'react';
import {
    Dimensions,
    StyleSheet,
    ScrollView,
    View,
    Image,
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import Blue_Logo from '../../assets/Blue_Logo.png';
import Icon from 'react-native-vector-icons/FontAwesome';

const SideMenu = ({ onItemSelected, city }) => {
    return (
        <View style={styles.menu}>
            <View style={styles.logoContainer}>
                <Image
                    style={styles.avatar}
                    source={Blue_Logo}
                />
                <Text style={{ ...styles.item, color: '#0854A2', fontSize: 18 }}>{city}</Text>
            </View>
            <View style={{ height: '60%', width: '100%', justifyContent: 'space-evenly' }}>
                <TouchableWithoutFeedback onPress={() => onItemSelected('Chat')}>
                    <View style={styles.buttonStyle}>
                        <Text style={{ ...styles.item, color: 'white' }}>Spotted Buzee</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => onItemSelected('Ajuda')}>
                    <View style={styles.buttonStyle}>
                        <Text style={{ ...styles.item, color: 'white' }}>Ajuda</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => onItemSelected('Convidar')}>
                    <View style={styles.buttonStyle}>
                        <Text style={{ ...styles.item, color: 'white' }}>Convidar amigos</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => onItemSelected('Cidade')}>
                    <View style={styles.buttonStyle}>
                        <Text style={{ ...styles.item, color: 'white' }}>Alterar cidade</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => onItemSelected('Quem')}>
                    <View style={styles.buttonStyle}>
                        <Text style={{ ...styles.item, color: 'white' }}>Quem somos</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => onItemSelected('Contato')}>
                    <View style={styles.buttonStyle}>
                        <Text style={{ ...styles.item, color: 'white' }}>Contato</Text>
                    </View>
                </TouchableWithoutFeedback>


            </View>


            <View style={{ height: '20%', width: '100%', alignItems: 'center' }}>
                <Text style={{ ...styles.item, fontSize: 16, margin: 5 }}>Nos siga nas redes sociais: </Text>
                <Text style={{ ...styles.item, fontSize: 16, margin: 5 }}>@buzeemob</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', width: '80%', margin: 5 }}>
                    <TouchableWithoutFeedback onPress={() => onItemSelected('Facebook')}>
                        <Icon name='facebook-f' size={40} color='#0854A2' />
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => onItemSelected('Instagram')}>
                        <Icon name='instagram' size={40} color='#0854A2' />
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => onItemSelected('Twitter')}>
                        <Icon name='twitter' size={40} color='#0854A2' />
                    </TouchableWithoutFeedback>


                </View>

            </View>




        </View>
    );
}

const window = Dimensions.get('window');


const styles = StyleSheet.create({
    menu: {
        flex: 1,
        width: window.width * 0.68,
        height: window.height,
    },
    logoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '20%',
        paddingTop: 10
    },
    avatar: {
        // width: window.width * 0.5,
        height: '65%',

        resizeMode: 'contain'
    },
    itemsContainer: {

    },
    item: {
        fontFamily: 'Decker',
        fontSize: 22,
        fontWeight: '300',
        margin: 8,
        marginRight: 15,
        color: '#0854A2'
    },
    buttonStyle: {
        width: '90%',
        borderTopRightRadius: 40,
        borderBottomRightRadius: 40,
        backgroundColor: '#5E9BD4',
        marginBottom: 4,
        justifyContent: 'center',
    }
});


export default SideMenu