import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    AsyncStorage,
    ImageBackground
} from 'react-native';
import fundo from '../../assets/fundo.png';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import firebase from "react-native-firebase";

class SideCidade extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props) {
        super(props);

    }
    componentWillMount() {
        console.log(this.props.firebaseRef)
    }

    close = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    restartAppWithNewConfigs = (city) => {

        AsyncStorage.setItem('buzee:selectedCity', city).then(() => {
            firebase.messaging().subscribeToTopic(city);
            AsyncStorage.removeItem('buzee:filteredLines').then(() => {
                this.close()
                Navigation.setRoot({
                    root: {
                        stack: {
                            children: [{
                                component: {
                                    name: 'buzee.SplashScreen'
                                }
                            }]
                        }
                    }
                });
            })
        });
      
    }

    selectDB = (selectedCity) => {
        switch (selectedCity) {
            case 'SM':
                AsyncStorage.getItem('buzee:selectedCity').then((value) => {
                    if (value !== null) {
                        firebase.messaging().unsubscribeFromTopic(value);
                        this.restartAppWithNewConfigs('SM')
                    } else {
                        this.restartAppWithNewConfigs('SM')
                    }
                })
                break;
            case 'POA':
                AsyncStorage.getItem('buzee:selectedCity').then((value) => {
                    if (value !== null) {
                        firebase.messaging().unsubscribeFromTopic(value);
                        this.restartAppWithNewConfigs('POA')
                    } else {
                        this.restartAppWithNewConfigs('POA')
                    }
                })
                break;

        }
    }

    render() {

        let content = 'Selecione sua cidade:\n'
        return (
            <ImageBackground source={fundo} style={{ ...styles.screen, width: '100%' }}>
                <View style={styles.screen}>

                    {/* <View style={styles.topContainer}>
                        <TouchableWithoutFeedback onPress={this.close}>
                            <View style={{ position: 'absolute', left: 0, padding: 10 }}>
                                <Icon size={30} name='chevron-left' color='white' />
                            </View>

                        </TouchableWithoutFeedback>
                        <Text style={styles.titleStyle}>Alterar cidade</Text>
                    </View> */}



                    <View style={styles.middleContainer}>

                        <View style={{ alignItems: 'center', justifyContent: 'space-evenly', width: '100%', height: '100%' }} >
                            <Text style={styles.textStyle}>{content}</Text>
                            <TouchableWithoutFeedback onPress={() => { this.selectDB('SM') }}>
                                <View style={{ borderRadius: 10, borderWidth: 3, borderColor: 'white', padding: 10, alignItems: 'center', justifyContent: 'center', width: '80%', marginBottom: 20 }}>
                                    <Text style={{ ...styles.textStyle, fontSize: 20 }}>Santa Maria</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => { this.selectDB('POA') }}>
                                <View style={{ borderRadius: 10, borderWidth: 3, borderColor: 'white', padding: 10, alignItems: 'center', justifyContent: 'center', width: '80%', marginBottom: 20 }}>
                                    <Text style={{ ...styles.textStyle, fontSize: 20 }}>Porto Alegre</Text>
                                </View>
                            </TouchableWithoutFeedback>

                        </View>

                    </View>


                    {/* 
                    <View style={styles.bottomContainer}>
                    </View> */}

                </View >
            </ImageBackground>



        )


    }

}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    titleStyle: {
        fontSize: 24,
        color: 'white',
        fontFamily: 'FuturaBold',
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'center'
    },
    textStyle: {
        fontSize: 18,
        color: 'white',
        fontFamily: 'Futura',
        textAlign: 'center'
    },
    topContainer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'center'

    },
    middleContainer: {
        height: '85%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    bottomContainer: {
        height: '5%',
        width: '100%',
        backgroundColor: '#0854A2',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },

});


const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef
    }
}
export default connect(mapStateToProps)(SideCidade);
