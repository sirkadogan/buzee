import React from 'react';
import { TextInput, StyleSheet,Keyboard } from 'react-native'

const defaultInput = (props) => (
    <TextInput
        underlineColorAndroid='transparent'
        
        //this binds any other properties that are available to "textInput". THis way, when this component
        //is called, you can call any other properties you want and they will be assigned to it.
        {...props} //The property described above is called "distributor"
        style={[styles.input, props.style, !props.valid && props.touched ? styles.invalid : null]} //Passing the style as an array allows the styles to be overwritten
        
    />
);
const styles = StyleSheet.create({
    input: {
        width: '100%',
        borderWidth: 1,
        borderColor: '#eee',
        padding: 5,
        marginTop: 8,
        marginBottom: 8
    },
    invalid:{
        backgroundColor: '#f9c0c0',
        borderColor: 'red'
    }
});
export default defaultInput;