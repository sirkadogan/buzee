import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableWithoutFeedback, Alert, ToastAndroid } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import verified from '../../assets/verified.png';
//Este  componente é o que compõe cada uma dos comentários nos posts
// do Spotted Buzee. 
class ChatComment extends Component {
    state = {
        checked: null,
        postTime: 0,
        score: this.props.score,
        downvote: null,
        upvote: null,
    }

    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef //Referência ao firebase selecionado 
        this.calculateTime(); //Calcula há quanto tempo foi postado
        this.firebaseRef.database() //Busca se o usuário já deu upvote no post e seta o estado da seleção
            .ref()
            .child('posts/postVotes/' + this.props.commentKey + '/' + this.props.user.uid)
            .once('value')
            .then((snapshot) => {
                if (snapshot.exists()) {
                    let data = snapshot.val();
                    if (data === 1) {
                        this.setState({
                            upvote: true,
                        })
                    } else {
                        this.setState({
                            downvote: true,
                        })
                    }
                }
            })
    }

    calculateTime = () => { //Calcula há quanto tempo o comentário foi postado
        let now = Date.now() / 1000;
        let postTime = parseInt(this.props.timestamp) / 1000 //momento do post em segundos
        let timeDiff = Math.floor(now - postTime); //Diferença entre atual e horário do comentário
        let time;
        //Separa o modal do horário por mês, dias, horas, minutos e segundos
        if (timeDiff > 2648000) {
            time = Math.floor(timeDiff / 2648000)
            this.setState({
                postTime: time + 'm'
            })
        } else if (timeDiff > 86400) {
            time = Math.floor(timeDiff / 86400)
            this.setState({
                postTime: time + 'd'
            })
        } else if (timeDiff > 3600) {
            time = Math.floor(timeDiff / 3600)
            this.setState({
                postTime: time + 'h'
            })
        } else if (timeDiff > 60) {
            time = Math.floor(timeDiff / 60)
            this.setState({
                postTime: time + 'min'
            })
        } else if (timeDiff < 60) {
            time = Math.floor(timeDiff)
            this.setState({
                postTime: time + 's'
            })
        }
    }


    /*
    Esta função auxiliar está presente em diversos arquivos e 
    serve para incrementar algum valor no banco de dados. 
    Neste caso, incrementa ou decrementa a contagem de upvotes 
    do comentário
    */
    runTransaction = (val) => {
        return this.firebaseRef.database()
            .ref()
            .child('posts/postComments/' + this.props.postKey + '/' + this.props.commentKey + '/score')
            .transaction((current_value) => {
                if (current_value) {
                    return (current_value || 0) + val;
                } else {
                    return val;
                }

            });
    }


    //Função de upvote no comentário
    upvote = () => {

        if (this.state.upvote) { //Se usuário já tiver dado upvote, remove o upvote dele
            //Retira user da lista de upvotes do comentário
            this.firebaseRef.database()
                .ref()
                .child('posts/postVotes/' + this.props.commentKey + '/' + this.props.user.uid)
                .remove()
            //Muda estado local
            this.setState({
                upvote: false,
                score: this.state.score - 1,
            })
            //Decrementa estado online
            this.runTransaction(-1);
        } else { //Se usuário não tiver dado upvote
            let sum = this.state.downvote ? 2 : 1 //Se usuário tiver dado downvote e decidiu alterar para upvote, altera o valor a ser incrementado
            //Inclui usuário na lista de upvotes
            this.firebaseRef.database()
                .ref()
                .child('posts/postVotes/' + this.props.commentKey + '/' + this.props.user.uid)
                .set(1)
            //Altera o estado local do score
            this.setState({
                upvote: true,
                downvote: false,
                score: this.state.score + sum,
            })
            //Incrementa estado online
            this.runTransaction(sum);
        }
    }

    //Função de downvote no comentário
    downvote = () => {
        if (this.state.downvote) { //Se usuário já deu downvote no post, um novo downvote o remove
            //Remove usuário da lista de downvotes
            this.firebaseRef.database()
                .ref()
                .child('posts/postVotes/' + this.props.commentKey + '/' + this.props.user.uid)
                .remove();
            //Altera estado local e incrementa valor em 1, já que removeu downvote
            this.setState({
                downvote: false,
                score: this.state.score + 1,
            })
            //Altera estado no firebase
            this.runTransaction(1);
        } else { //Se não tiver downvote
            let sum = this.state.upvote ? -2 : -1 //Se usuário tiver dado upvote e decidiu alterar para downvote, altera o valor a ser incrementado
            //Adiciona usuário na lista de downvotes
            this.firebaseRef.database()
                .ref()
                .child('posts/postVotes/' + this.props.commentKey + '/' + this.props.user.uid)
                .set(-1)
            //Altera estado local
            this.setState({
                upvote: false,
                downvote: true,
                score: this.state.score + sum,
            })
            //Altera estado no firebase
            this.runTransaction(sum);
        }
    }

    //Long press no comentário abre Alerta para apagar 
    //o comentário se a uid do author for igual a do usuário
    longPress = () => {
        if (this.props.user.uid == this.props.uid) { //Condicional para comparar author ao usuário atual
            Alert.alert(
                'Apagar comentário',
                'Deseja apagar este comentário?',
                [
                    {
                        text: 'Não',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sim', onPress: () => {
                            ToastAndroid.show('Apagando Post!', ToastAndroid.SHORT);
                            return this.firebaseRef.database()
                                .ref()
                                .child('posts/postComments/' + this.props.postKey + '/' + this.props.commentKey)
                                .remove()
                                .then(() => {
                                    this.props.refresh()
                                });

                        }
                    },
                ],
                { cancelable: true },
            );

        }
    }

    render() {
        if (this.props.oficial) {
            //RETORNA UM COMENTÁRIO OFICIAL ESTILIZADO PARA A BUZEE
            return (
                <View style={{ ...styles.postContainer, backgroundColor: '#78CEDB' }}>
                    {/* O Objeto é dividido em duas partes, corpo (author, horário, texto) e pontuação (upvote, score, downvote)  */}
                    {/* CORPO */}
                    <TouchableWithoutFeedback onLongPress={this.longPress}>
                        <View style={{ height: '100%', flex: 1 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontSize: 18, fontFamily: 'BarlowCondensed-SemiBold', color: '#FFFFFF', marginRight: 5 }}>{this.props.author}</Text>
                                <Image style={{ width: 15, height: 15 }} source={verified} />
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={{ fontSize: 16, fontFamily: 'BarlowCondensed-Light', marginRight: 5, color: '#FFFFFF' }}>{this.props.text}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 10, fontFamily: 'barlowsemicondensed-light', color: '#FFFFFF' }}>{'há ' + this.state.postTime}</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>

                    {/* PONTUAÇÃO */}
                    <View style={{ height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableWithoutFeedback onPress={this.upvote}>
                            <View style={{ padding: 2 }}>
                                <Icon size={30} name='chevron-up' color={this.state.upvote ? '#33565C' : '#B1E1E9'} />
                            </View>
                        </TouchableWithoutFeedback>
                        <Text style={{ fontSize: 14, fontFamily: 'BarlowCondensed-SemiBold', color: '#FFFFFF' }}>{this.state.score}</Text>

                        <TouchableWithoutFeedback onPress={this.downvote}>
                            <View style={{ padding: 2 }}>
                                <Icon size={30} name='chevron-down' color={this.state.downvote ? '#33565C' : '#B1E1E9'} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            )

        } else { //Estilização padrão do comentário
            return (

                <View style={{ ...styles.postContainer, backgroundColor: 'white' }}>
                    {/* CORPO */}
                    <TouchableWithoutFeedback onLongPress={this.longPress}>
                        <View style={{ height: '100%', flex: 1 }}>
                            <View>
                                <Text style={{ fontSize: 16, fontFamily: 'BarlowCondensed-SemiBold', color: '#145EAC' }}>{this.props.author}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={{ fontSize: 16, fontFamily: 'BarlowCondensed-Light', marginRight: 5, color: '#454546' }}>{this.props.text}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 10, fontFamily: 'barlowsemicondensed-light', color: '#454546' }}>{'há ' + this.state.postTime}</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>

                    {/* PONTUAÇÃO */}
                    <View style={{ height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableWithoutFeedback onPress={this.upvote}>
                            <View style={{ padding: 2 }}>
                                <Icon size={30} name='chevron-up' color={this.state.upvote ? '#1866B1' : '#97C5EA'} />
                            </View>
                        </TouchableWithoutFeedback>
                        <Text style={{ fontSize: 14, fontFamily: 'BarlowCondensed-SemiBold', color: '#454546' }}>{this.state.score}</Text>

                        <TouchableWithoutFeedback onPress={this.downvote}>
                            <View style={{ padding: 2 }}>
                                <Icon size={30} name='chevron-down' color={this.state.downvote ? '#1866B1' : '#97C5EA'} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            );
        }
    }
};

const styles = StyleSheet.create({
    postContainer: {
        width: '100%',
        borderRadius: 15,
        padding: 5,
        marginBottom: 10,
        flexDirection: 'row',
        borderWidth: 2,
        borderColor: '#F1F2F2'
    },
    postBottomContainer: {
        width: '100%',
        flexDirection: 'row'
    },
    postTopContainer: {
        flexDirection: 'row',
        width: '100%',
    },
    authorContainer: {
        width: '85%',
        padding: 5,
        justifyContent: 'center'
    },
    timeContainer: {
        width: '15%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textContainer: {
        width: '85%',
        padding: 5,
        justifyContent: 'center',
    },
    voteContainer: {
        width: '15%',
        alignItems: 'center',
        justifyContent: 'center'
    },


});


const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef,
        user: state.user.user
    }
}


export default connect(mapStateToProps)(ChatComment);
