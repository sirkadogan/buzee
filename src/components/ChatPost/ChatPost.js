import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Image, Alert, ToastAndroid } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import verified from '../../assets/verified.png';

//Este componente é semelhante ao componetne de comentários
//É o componente geral do post, úica diferença é possuir o " 10 comentários em seu corpo"
//Olhar documentação de chatComment para referência 
class ChatPost extends Component {
    state = {
        checked: null,
        postTime: 0,
        score: this.props.score,
        downvote: null,
        upvote: null,
    }

    componentWillMount() {
        this.firebaseRef = this.props.firebaseRef.firebaseRef
        this.calculateTime();
        this.firebaseRef.database()
            .ref()
            .child('posts/postVotes/' + this.props.keyRef + '/' + this.props.user.uid)
            .once('value')
            .then((snapshot) => {
                if (snapshot.exists()) {
                    let data = snapshot.val();
                    if (data === 1) {
                        this.setState({
                            upvote: true,
                        })
                    } else {
                        this.setState({
                            downvote: true,
                        })
                    }
                }
            })
    }

    calculateTime = () => {
        let now = Date.now() / 1000;
        let postTime = parseInt(this.props.timestamp) / 1000
        let timeDiff = Math.floor(now - postTime);
        let time;
        if (timeDiff > 2648000) {
            time = Math.floor(timeDiff / 2648000)
            this.setState({
                postTime: time + 'm'
            })
        } else if (timeDiff > 86400) {
            time = Math.floor(timeDiff / 86400)
            this.setState({
                postTime: time + 'd'
            })
        } else if (timeDiff > 3600) {
            time = Math.floor(timeDiff / 3600)
            this.setState({
                postTime: time + 'h'
            })
        } else if (timeDiff > 60) {
            time = Math.floor(timeDiff / 60)
            this.setState({
                postTime: time + 'min'
            })
        } else if (timeDiff < 60) {
            time = Math.floor(timeDiff)
            this.setState({
                postTime: time + 's'
            })
        }
    }

    runTransaction = (val) => {
        return this.firebaseRef.database()
            .ref()
            .child('posts/postFeed/' + this.props.keyRef + '/score')
            .transaction((current_value) => {
                if (current_value) {
                    return (current_value || 0) + val;
                } else {
                    return val;
                }

            });
    }

    upvote = () => {
        if (this.state.upvote) {
            this.firebaseRef.database()
                .ref()
                .child('posts/postVotes/' + this.props.keyRef + '/' + this.props.user.uid)
                .remove()
            this.setState({
                upvote: false,
                score: this.state.score - 1,
            })

            this.runTransaction(-1);
        } else {
            let sum = this.state.downvote ? 2 : 1
            this.firebaseRef.database()
                .ref()
                .child('posts/postVotes/' + this.props.keyRef + '/' + this.props.user.uid)
                .set(1)
            this.setState({
                upvote: true,
                downvote: false,
                score: this.state.score + sum,
            })
            this.runTransaction(sum);
        }
    }

    downvote = () => {
        if (this.state.downvote) {
            this.firebaseRef.database()
                .ref()
                .child('posts/postVotes/' + this.props.keyRef + '/' + this.props.user.uid)
                .remove();
            this.setState({
                downvote: false,
                score: this.state.score + 1,
            })
            this.runTransaction(1);
        } else {
            let sum = this.state.upvote ? -2 : -1
            this.firebaseRef.database()
                .ref()
                .child('posts/postVotes/' + this.props.keyRef + '/' + this.props.user.uid)
                .set(-1)
            this.setState({
                upvote: false,
                downvote: true,
                score: this.state.score + sum,
            })
            this.runTransaction(sum);

        }
    }


    openPost = () => {
        if (!this.props.onDetailScreen) {
            Navigation.showModal({
                stack: {
                    children: [{
                        component: {
                            name: 'buzee.PostDetailScreen',
                            passProps: {
                                post: this.props,
                                postTime: this.state.postTime,
                                score: this.state.score
                            }
                        }
                    }]
                }
            });
        }

    }

    longPress = () => {
        if (!this.props.onDetailScreen) {
            if (this.props.user.uid == this.props.uid) {
                Alert.alert(
                    'Apagar post',
                    'Deseja apagar este post?',
                    [
                        {
                            text: 'Não',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                        {
                            text: 'Sim', onPress: () => {
                                ToastAndroid.show('Apagando Post!', ToastAndroid.SHORT);
                                return this.firebaseRef.database()
                                    .ref()
                                    .child('posts/postFeed/' + this.props.keyRef)
                                    .remove()
                                    .then(() => {
                                        this.firebaseRef.database()
                                            .ref()
                                            .child('posts/myPosts/' + this.props.user.uid + '/' + this.props.keyRef)
                                            .remove();
                                        this.props.refresh()
                                    });

                            }
                        },
                    ],
                    { cancelable: true },
                );

            }
        }

    }

    render() {
        if (this.props.oficial) {
            //RETORNA UM POST COM ESTILIZAÇÃO ESPECIAL PARA DEMONSTRAR UM POST DO BUZEE
            return (
                <View style={{ ...styles.postContainer, backgroundColor: '#78CEDB' }}>
                    <TouchableWithoutFeedback onPress={this.openPost} onLongPress={this.longPress}>
                        <View style={{ height: '100%', flex: 1 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontSize: 18, fontFamily: 'BarlowCondensed-SemiBold', color: '#FFFFFF', marginRight: 5 }}>{this.props.author}</Text>
                                <Image style={{ width: 15, height: 15 }} source={verified} />
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', }}>
                                <Text style={{ fontSize: 16, fontFamily: 'BarlowCondensed-Light', marginRight: 5, color: '#FFFFFF' }}>{this.props.text}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 10, fontFamily: 'barlowsemicondensed-light', marginRight: 5, color: '#FFFFFF' }}>{this.props.comments > 1 ? this.props.comments + ' comentários' : this.props.comments == 0 ? this.props.comments + ' comentários' : this.props.comments + ' comentário'}</Text>
                                <Text style={{ fontSize: 10, fontFamily: 'barlowsemicondensed-light', marginRight: 5, color: '#FFFFFF' }}>{'•'}</Text>
                                <Text style={{ fontSize: 10, fontFamily: 'barlowsemicondensed-light', color: '#FFFFFF' }}>{'há ' + this.state.postTime}</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{ height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableWithoutFeedback onPress={this.upvote}>
                            <View style={{ padding: 2 }}>
                                <Icon size={30} name='chevron-up' color={this.state.upvote ? '#33565C' : '#B1E1E9'} />
                            </View>
                        </TouchableWithoutFeedback>
                        <Text style={{ fontSize: 14, fontFamily: 'BarlowCondensed-SemiBold', color: '#FFFFFF' }}>{this.state.score}</Text>

                        <TouchableWithoutFeedback onPress={this.downvote}>
                            <View style={{ padding: 2 }}>
                                <Icon size={30} name='chevron-down' color={this.state.downvote ? '#33565C' : '#B1E1E9'} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            )
        } else { 
            // RETORNA ESTILIZAÇÃO DE POST PADRÃO
            return (
                <View style={{ ...styles.postContainer, backgroundColor: this.props.oficial ? 'gold' : 'white' }}>
                    <TouchableWithoutFeedback onPress={this.openPost} onLongPress={this.longPress}>
                        <View style={{ height: '100%', flex: 1 }}>
                            <View >
                                <Text style={{ fontSize: 16, fontFamily: 'BarlowCondensed-SemiBold', color: '#145EAC' }}>{this.props.author}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', }}>
                                <Text style={{ fontSize: 16, fontFamily: 'BarlowCondensed-Light', marginRight: 5, color: '#454546' }}>{this.props.text}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 10, fontFamily: 'barlowsemicondensed-light', marginRight: 5, color: '#454546' }}>{this.props.comments > 1 ? this.props.comments + ' comentários' : this.props.comments == 0 ? this.props.comments + ' comentários' : this.props.comments + ' comentário'}</Text>
                                <Text style={{ fontSize: 10, fontFamily: 'barlowsemicondensed-light', marginRight: 5, color: '#454546' }}>{'•'}</Text>
                                <Text style={{ fontSize: 10, fontFamily: 'barlowsemicondensed-light', color: '#454546' }}>{'há ' + this.state.postTime}</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{ height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableWithoutFeedback onPress={this.upvote}>
                            <View style={{ padding: 2 }}>
                                <Icon size={30} name='chevron-up' color={this.state.upvote ? '#1866B1' : '#97C5EA'} />
                            </View>
                        </TouchableWithoutFeedback>
                        <Text style={{ fontSize: 14, fontFamily: 'BarlowCondensed-SemiBold', color: '#454546' }}>{this.state.score}</Text>

                        <TouchableWithoutFeedback onPress={this.downvote}>
                            <View style={{ padding: 2 }}>
                                <Icon size={30} name='chevron-down' color={this.state.downvote ? '#1866B1' : '#97C5EA'} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>

            );
        }

    }
};

const styles = StyleSheet.create({
    postContainer: {
        width: '100%',
        borderRadius: 15,
        padding: 5,
        marginBottom: 10,
        flexDirection: 'row',
        backgroundColor: 'white',
        borderWidth: 2,
        borderColor: '#F1F2F2'
    },
    postBottomContainer: {
        width: '100%',
        flexDirection: 'row'
    },
    postTopContainer: {
        flexDirection: 'row',
        width: '100%',
    },
    authorContainer: {
        width: '85%',
        padding: 5,
        justifyContent: 'center'
    },
    timeContainer: {
        width: '15%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textContainer: {
        width: '85%',
        padding: 5,
        justifyContent: 'center',
    },
    voteContainer: {
        width: '15%',
        alignItems: 'center',
        justifyContent: 'center'
    },


});


const mapStateToProps = state => {
    return {
        firebaseRef: state.firebaseRef,
        user: state.user.user
    }
}


export default connect(mapStateToProps)(ChatPost);
