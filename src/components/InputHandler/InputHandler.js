import React from 'react';
import DefaultInput from '../../components/UI/DefaultInput/DefaultInput';
//Este componente é utilizado na tela de login por email 
//Como campo de escrita.
const inputHandler = props => (

    <DefaultInput
        value={props.placeData.value}
        valid = {props.placeData.valid}
        touched = {props.placeData.touched}
        onChangeText={props.onChangeText}
        placeholder={props.placeholder}
        
    />
);

export default inputHandler;