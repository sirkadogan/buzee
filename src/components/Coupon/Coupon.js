
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
//Este componente retorna a estilização do cupom
// do Polvo Louco presente na tela do Perfil
// com 3 colunas (Código do cupom, valor do desconto e qual fonte do cupom)

const coupon = props => {
    return (
        <View style={styles.container}>
            <View style={{ ...styles.coupon, backgroundColor: props.index % 2 ? '#5E9BD4' : '#0854A2' }} >
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text style={styles.textStyle}>{props.code}</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text style={styles.textStyle}>{props.value}</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text style={styles.textStyle}>{props.publisher}</Text>
                </View>
            </View>
        </View >
    )


};

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    coupon: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 20,
        paddingBottom: 20,
        alignItems: 'center'

    },
    textStyle: {
        color: 'white',
        fontSize: 18,
        fontFamily: 'Decker'

    }
});

export default coupon;