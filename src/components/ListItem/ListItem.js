import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Image } from 'react-native'
import { CheckBox } from 'react-native-elements';
//Este componente é referente a tela de selecionar linhas
//Cada uma das linhas é um componente deste, com nome da linha, 
//código, e checkbox para seleção
class listItem extends Component {
    state = {
        checked: null,
        filteredLines: this.props.filteredLines
    }
    componentWillMount() {
        // Procura quais linhas tinham sido selecionadas previamente na memória
        if (this.state.filteredLines.includes(this.props.lineKey)) {
            this.setState({
                checked: true
            })
        }
    }
    //Adiciona item selecionado na lista. 
    //Função vinda dos props apenas adiciona o item na lista
     
    addToList = (lineKey) => {
        this.props.addToList(lineKey)
    }
    //Remove item selecionado da lista. 
    removeFromList = (lineKey) => {
        this.props.removeFromList(lineKey)
    }

    //Toggle do  check no checkbox
    check = () => {
        this.state.checked ? this.removeFromList(this.props.lineKey) : this.addToList(this.props.lineKey)
        this.setState({ checked: !this.state.checked })
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => this.check()}>
                <View style={{ ...styles.listItem, backgroundColor: this.props.index % 2 ? '#5E9BD4' : '#0854A2' }} >
                    <View style={{ width: '20%', padding: 10 }}>
                        <Text style={styles.text}>{this.props.lineKey}</Text>
                    </View>
                    <View style={{ width: '60%', padding: 10 }}>
                        <Text style={{ ...styles.text, fontSize: 18 }}>{this.props.lineName}</Text>
                    </View>
                    <View style={{ width: '20%' }}>
                        <CheckBox
                            center
                            checked={this.state.checked}
                            onPress={() => this.check()}
                            checkedColor='white'
                            uncheckedColor='white'
                        />
                    </View>
                </View>
            </TouchableWithoutFeedback>

        );
    }
};

const styles = StyleSheet.create({
    listItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    text: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'Decker'
    },

});

export default listItem;