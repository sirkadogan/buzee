
import React from 'react';
import { Button, Text, View, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Divider } from 'react-native-elements';
//Este componente é o objeto que descreve cada item dos horários de 
//ônibus ao tocar em uma parada.
const BusTime = props => {
    return (
        <View style={styles.container}>
            <View style={styles.time}>
                <Text style={styles.textStyle}>{props.time}</Text> 
            </View>
            <View style={styles.description}>
                <Text style={styles.textStyle}>{props.line}</Text>
                <Text style={{ ...styles.textStyle, fontSize: 18 }}>{props.direction}</Text>
            </View>

        </View>
    )

};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        padding: 5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    time: {
        width: '20%'
    },
    textStyle: {
        color: 'white',
        fontSize: 22,
        fontFamily: 'Decker',
        textAlign: 'center'
    },
    description: {
        width: '80%'
    }
});

export default BusTime;