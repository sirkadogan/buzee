
export const UI_START_LOADING = 'UI_START_LOADING';
export const UI_STOP_LOADING = 'UI_STOP_LOADING';

export const AUTH_SET_TOKEN = 'AUTH_SET_TOKEN';
export const AUTH_REMOVE_TOKEN = 'AUTH_REMOVE_TOKEN';

export const STORE_USER = 'STORE_USER';

export const STORE_STOPS = 'STORE_STOPS';
export const STORE_LOCATION = 'STORE_LOCATION';

export const STORE_FIREBASE = 'STORE_FIREBASE';
export const STORE_FIREBASE_AUTH = 'STORE_FIREBASE_AUTH';


