import { STORE_STOPS, STORE_LOCATION } from './actionTypes'

//THESE ARE ACTION CREATORS. THEY ARE FUNCTIONS THAT HELP CREATE THE ACTIONS
//WITHOUT NEED OF COPYING CODE ALL OVER. BASICALLY THEY TAKE THE TYPE OF THE ACTION
//AND ADD THE PAYLOAD AS THE INPUT OF THE FUNCTION

export const storeStops = (stopMarkers) => {
    return {
        type: STORE_STOPS,
        stopMarkers: stopMarkers
    };
}
export const storeLocation = (location) => {
    return {
        type: STORE_LOCATION,
        location: location
    };
}