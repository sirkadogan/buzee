import { STORE_FIREBASE, STORE_FIREBASE_AUTH } from './actionTypes'

//THESE ARE ACTION CREATORS. THEY ARE FUNCTIONS THAT HELP CREATE THE ACTIONS
//WITHOUT NEED OF COPYING CODE ALL OVER. BASICALLY THEY TAKE THE TYPE OF THE ACTION
//AND ADD THE PAYLOAD AS THE INPUT OF THE FUNCTION

export const storeFirebase = (firebaseRef) => {
    return {
        type: STORE_FIREBASE,
        firebaseRef: firebaseRef
    };
}

export const storeFirebaseAuth = (authSubscription) => {
    return {
        type: STORE_FIREBASE_AUTH,
        authSubscription: authSubscription
    };
}

