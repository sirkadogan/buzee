import { STORE_USER } from './actionTypes'
import firebase from 'react-native-firebase'
import BackgroundGeolocation from "react-native-background-geolocation";
//THESE ARE ACTION CREATORS. THEY ARE FUNCTIONS THAT HELP CREATE THE ACTIONS
//WITHOUT NEED OF COPYING CODE ALL OVER. BASICALLY THEY TAKE THE TYPE OF THE ACTION
//AND ADD THE PAYLOAD AS THE INPUT OF THE FUNCTION

export const storeUser = (user) => {
    return {
        type: STORE_USER,
        user: user
    };
}

export const updateLevel = (user) => {
    return dispatch => {
        let uid = user.uid;
        BackgroundGeolocation.getOdometer().then((result) => {
            // alert('Total de ' + result + ' km percorridos');
            firebase.database().ref().child('users/' + uid + '/odometer').once('value', function (snapshot) {
                let newOdometer = result + snapshot.val();
                return firebase.database().ref().child('users/' + uid + '/odometer').set(newOdometer);
            });
        });
    }
}