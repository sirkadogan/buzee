import { AsyncStorage } from 'react-native';
import { uiStartLoading, uiStopLoading } from './index';
// import * as firebase from 'firebase';
import firebase from 'react-native-firebase';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';

// // Initialize Firebase
// const firebaseConfig = {
//     apiKey: "AIzaSyCK36qUnfS0UetJwTNlKRqEVpsmjvivHgc",
//     databaseURL: "https://buzee2-9c40b.firebaseio.com/",
//     authDomain: "buzee2-9c40b.firebaseapp.com",
// };
// const firebaseApp = firebase.initializeApp(firebaseConfig);


const API_KEY = 'AIzaSyCK36qUnfS0UetJwTNlKRqEVpsmjvivHgc';

export const googleLogin = () => {
    return dispatch => {
        GoogleSignin.hasPlayServices().then(
            GoogleSignin.signIn().then(data => {
                // this.setState({ data });
                const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
                dispatch(uiStartLoading());
                // startMainTabs();
                return state.firebaseRef.firebaseRef.auth().signInAndRetrieveDataWithCredential(credential);

            }).catch((error) => {
                if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                    // user cancelled the login flow
                } else if (error.code === statusCodes.IN_PROGRESS) {
                    // operation (f.e. sign in) is in progress already
                } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                    // play services not available or outdated
                } else {
                    // some other error happened
                }
            })
        )
    };
};
