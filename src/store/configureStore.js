import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import uiReducer from './reducers/ui';
// import authReducer from './reducers/auth';
import userReducer from './reducers/user';
import geolocationReducer from './reducers/geolocation';
import firebaseRefReducer from './reducers/firebaseRef';

const rootReducer = combineReducers({
    ui: uiReducer,
    // auth: authReducer,
    user: userReducer,
    geolocation: geolocationReducer,
    firebaseRef: firebaseRefReducer
});

let composeEnhancers = compose;

if (__DEV__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = () => {
    return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

};

export default configureStore;