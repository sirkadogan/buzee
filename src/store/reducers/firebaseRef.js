import { STORE_FIREBASE, STORE_FIREBASE_AUTH } from '../actions/actionTypes';

const initialState = {
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case STORE_FIREBASE:
            return {
                ...state,
                firebaseRef: action.firebaseRef
            };
        case STORE_FIREBASE_AUTH:
            return {
                ...state,
                authSubscription: action.authSubscription
            };
        default:
            return state;
    }
};

export default reducer;