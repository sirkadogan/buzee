import { STORE_STOPS, STORE_LOCATION } from '../actions/actionTypes';

const initialState = {
   stopMarkers: [],
   location: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case STORE_STOPS:
            return {
                ...state,
               stopMarkers: action.stopMarkers
            };
        case STORE_LOCATION:
            return{
                ...state,
                location: action.location
            }
        default:
            return state;
    }
};

export default reducer;