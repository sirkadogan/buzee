import { STORE_USER } from '../actions/actionTypes';

const initialState = {
   user: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case STORE_USER:
            return {
                ...state,
               user: action.user
            };
        default:
            return state;
    }
};

export default reducer;