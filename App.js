import React from 'react';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';

import AuthScreen from './src/screens/Auth/Auth';
import MainScreen from './src/screens/MainScreen/MainScreen';
import ShareLocation from './src/screens/ShareLocation/ShareLocation';
import BusTimeModal from './src/screens/BusTimeModal/BusTimeModal';
import LoginModal from './src/screens/LoginModal/LoginModal';
import ProfileScreen from './src/screens/Profile/ProfileScreen';
import LinesScreen from './src/screens/LinesScreen/LinesScreen';
import SplashScreen from './src/screens/SplashScreen/SplashScreen';
import TutorialScreen from './src/screens/TutorialScreen/TutorialScreen';
import BusDetail from './src/screens/BusDetail/BusDetail';
import ProfileTutorial from './src/screens/Profile/ProfileTutorial';
import ShareTutorial from './src/screens/ShareLocation/ShareTutorial';
import LinesTutorial from './src/screens/LinesScreen/LinesTutorial';
import ReportScreen from './src/screens/ReportScreen/ReportScreen';
import NoBusScreen from './src/screens/ReportScreen/NoBusScreen';
import NoBusScreen2 from './src/screens/ReportScreen/NoBusScreen2';
import AssaultScreen from './src/screens/ReportScreen/AssaultScreen';
import FinishAssaultScreen from './src/screens/ReportScreen/FinishAssaultScreen';
import TechnicalScreen from './src/screens/ReportScreen/TechnicalScreen';
import TechDetailScreen from './src/screens/ReportScreen/TechDetailScreen';
import AssaultExtra1Screen from './src/screens/ReportScreen/AssaultExtra1Screen';
import AssaultExtra2Screen from './src/screens/ReportScreen/AssaultExtra2Screen';
import SideAjuda from './src/screens/SideMenu/SideAjuda';
import SideQuem from './src/screens/SideMenu/SideQuem';
import SideContato from './src/screens/SideMenu/SideContato';
import SideCidade from './src/screens/SideMenu/SideCidade';
import ReportTutorial from './src/screens/ReportScreen/ReportTutorial';
import ChatFeed from './src/screens/Chat/ChatFeed';
import CreatePostScreen from './src/screens/Chat/CreatePostScreen';
import PostDetailScreen from './src/screens/Chat/PostDetailScreen';
import InviteFriends from './src/screens/InviteFriends/InviteFriends';


//Configuring the Store for REDUX
import configureStore from './src/store/configureStore';
const store = configureStore();

//Register Screens
Navigation.registerComponent('buzee.AuthScreen', () => (props) => (
  <Provider store={store}>
    <AuthScreen {...props} />
  </Provider>
), () => AuthScreen);
Navigation.registerComponent('buzee.MainScreen', () => (props) => (
  <Provider store={store}>
    <MainScreen {...props} />
  </Provider>
), () => MainScreen, store, Provider);
Navigation.registerComponent('buzee.ShareLocation', () => (props) => (
  <Provider store={store}>
    <ShareLocation {...props} />
  </Provider>
), () => ShareLocation, store, Provider);
Navigation.registerComponent('buzee.BusTimeModal', () => (props) => (
  <Provider store={store}>
    <BusTimeModal {...props} />
  </Provider>
), () => BusTimeModal, store, Provider);
Navigation.registerComponent('buzee.LoginModal', () => (props) => (
  <Provider store={store}>
    <LoginModal {...props} />
  </Provider>
), () => LoginModal, store, Provider);
Navigation.registerComponent('buzee.ProfileScreen', () => (props) => (
  <Provider store={store}>
    <ProfileScreen {...props} />
  </Provider>
), () => ProfileScreen, store, Provider);
Navigation.registerComponent('buzee.LinesScreen', () => (props) => (
  <Provider store={store}>
    <LinesScreen {...props} />
  </Provider>
), () => LinesScreen, store, Provider);
Navigation.registerComponent('buzee.SplashScreen', () => (props) => (
  <Provider store={store}>
    <SplashScreen {...props} />
  </Provider>
), () => SplashScreen, store, Provider);
Navigation.registerComponent('buzee.TutorialScreen', () => (props) => (
  <Provider store={store}>
    <TutorialScreen {...props} />
  </Provider>
), () => TutorialScreen, store, Provider);
Navigation.registerComponent('buzee.BusDetail', () => (props) => (
  <Provider store={store}>
    <BusDetail {...props} />
  </Provider>
), () => BusDetail, store, Provider);

Navigation.registerComponent('buzee.ProfileTutorial', () => (props) => (
  <Provider store={store}>
    <ProfileTutorial {...props} />
  </Provider>
), () => ProfileTutorial);
Navigation.registerComponent('buzee.ShareTutorial', () => (props) => (
  <Provider store={store}>
    <ShareTutorial {...props} />
  </Provider>
), () => ShareTutorial);
Navigation.registerComponent('buzee.LinesTutorial', () => (props) => (
  <Provider store={store}>
    <LinesTutorial {...props} />
  </Provider>
), () => LinesTutorial);
Navigation.registerComponent('buzee.ReportScreen', () => (props) => (
  <Provider store={store}>
    <ReportScreen {...props} />
  </Provider>
), () => ReportScreen);
Navigation.registerComponent('buzee.NoBusScreen', () => (props) => (
  <Provider store={store}>
    <NoBusScreen {...props} />
  </Provider>
), () => NoBusScreen);
Navigation.registerComponent('buzee.NoBusScreen2', () => (props) => (
  <Provider store={store}>
    <NoBusScreen2 {...props} />
  </Provider>
), () => NoBusScreen2);
Navigation.registerComponent('buzee.AssaultScreen', () => (props) => (
  <Provider store={store}>
    <AssaultScreen {...props} />
  </Provider>
), () => AssaultScreen);
Navigation.registerComponent('buzee.FinishAssaultScreen', () => (props) => (
  <Provider store={store}>
    <FinishAssaultScreen {...props} />
  </Provider>
), () => FinishAssaultScreen);
Navigation.registerComponent('buzee.TechnicalScreen', () => (props) => (
  <Provider store={store}>
    <TechnicalScreen {...props} />
  </Provider>
), () => TechnicalScreen);

Navigation.registerComponent('buzee.TechDetailScreen', () => (props) => (
  <Provider store={store}>
    <TechDetailScreen {...props} />
  </Provider>
), () => TechDetailScreen);
Navigation.registerComponent('buzee.AssaultExtra1Screen', () => (props) => (
  <Provider store={store}>
    <AssaultExtra1Screen {...props} />
  </Provider>
), () => AssaultExtra1Screen);

Navigation.registerComponent('buzee.AssaultExtra2Screen', () => (props) => (
  <Provider store={store}>
    <AssaultExtra2Screen {...props} />
  </Provider>
), () => AssaultExtra2Screen);
Navigation.registerComponent('buzee.SideAjuda', () => (props) => (
  <Provider store={store}>
    <SideAjuda {...props} />
  </Provider>
), () => SideAjuda);
Navigation.registerComponent('buzee.SideQuem', () => (props) => (
  <Provider store={store}>
    <SideQuem {...props} />
  </Provider>
), () => SideQuem);
Navigation.registerComponent('buzee.SideContato', () => (props) => (
  <Provider store={store}>
    <SideContato {...props} />
  </Provider>
), () => SideContato);
Navigation.registerComponent('buzee.SideCidade', () => (props) => (
  <Provider store={store}>
    <SideCidade {...props} />
  </Provider>
), () => SideCidade);
Navigation.registerComponent('buzee.ReportTutorial', () => (props) => (
  <Provider store={store}>
    <ReportTutorial {...props} />
  </Provider>
), () => ReportTutorial);
Navigation.registerComponent('buzee.ChatFeed', () => (props) => (
  <Provider store={store}>
    <ChatFeed {...props} />
  </Provider>
), () => ChatFeed);
Navigation.registerComponent('buzee.CreatePostScreen', () => (props) => (
  <Provider store={store}>
    <CreatePostScreen {...props} />
  </Provider>
), () => CreatePostScreen);
Navigation.registerComponent('buzee.InviteFriends', () => (props) => (
  <Provider store={store}>
    <InviteFriends {...props} />
  </Provider>
), () => InviteFriends);
Navigation.registerComponent('buzee.PostDetailScreen', () => (props) => (
  <Provider store={store}>
    <PostDetailScreen {...props} />
  </Provider>
), () => PostDetailScreen);

//Start an app
export default () => {
  Navigation.events().registerAppLaunchedListener(() => {
    // your app initialization code here
    // setRoot call

    Navigation.setDefaultOptions({
      topBar: {
        visible: false,
        drawBehind: true,
        animate: false,
      },
      layout: {
        orientation: ['portrait']
      }
    });

    Navigation.setRoot({
      root: {
        stack: {
          children: [{
            component: {
              name: 'buzee.SplashScreen'
            }
          }]
        }
      },
      layout: {
        orientation: ['portrait']
      }
    });
  });

}