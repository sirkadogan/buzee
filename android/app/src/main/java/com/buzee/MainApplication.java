package com.buzee;

import android.app.Application;

import com.facebook.react.ReactApplication;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage; 
import io.invertase.firebase.storage.RNFirebaseStoragePackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import io.invertase.firebase.database.RNFirebaseDatabasePackage;


import co.apptailor.googlesignin.RNGoogleSigninPackage;


import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

import com.oblador.vectoricons.VectorIconsPackage;
import com.airbnb.android.react.maps.MapsPackage;

import com.transistorsoft.rnbackgroundgeolocation.*;
import com.transistorsoft.rnbackgroundfetch.RNBackgroundFetchPackage;

import co.apptailor.googlesignin.RNGoogleSigninPackage;  
import com.heanoria.library.reactnative.locationenabler.RNAndroidLocationEnablerPackage;    


import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.react.NavigationReactNativeHost;
import com.reactnativenavigation.react.ReactGateway;

import android.support.multidex.MultiDex;
import android.content.Context;

public class MainApplication extends NavigationApplication {
    @Override
    protected ReactGateway createReactGateway() {
        ReactNativeHost host = new NavigationReactNativeHost(this, isDebug(), createAdditionalReactPackages()) {
            @Override
            protected String getJSMainModuleName() {
                return "index";
            }
        };
        return new ReactGateway(this, isDebug(), host);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public boolean isDebug() {
         // Make sure you are using BuildConfig from your own application
        return BuildConfig.DEBUG;
    }

     protected List<ReactPackage> getPackages() {
         // Add additional packages you require here
         // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
            new VectorIconsPackage(),
            new MapsPackage(),
            new RNBackgroundGeolocation(),
            new RNBackgroundFetchPackage(),
            new RNGoogleSigninPackage(),
            new RNAndroidLocationEnablerPackage(),
            new RNFirebasePackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage(),
            new RNFirebaseStoragePackage(),
            new RNFirebaseAuthPackage(),
            new RNFirebaseDatabasePackage()
        );
     }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }

//     @Override
//         public String getJSMainModuleName() {
//     return "index";
//  }
}

